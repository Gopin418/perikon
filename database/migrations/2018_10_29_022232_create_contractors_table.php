<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractors', function (Blueprint $table) {
            $table->increments('id');
            $table->char('uri_id');
            $table->string('nama_kontraktor');
            $table->string('email');
            $table->string('nama_pekerjaan');
            $table->date('tanggal_mulai');
            $table->string('tgl_mulai');
            $table->date('tanggal_selesai');
            $table->string('tgl_selesai');
            $table->enum('verified', ['true', 'false'])->default('false');
            $table->enum('status', ['Approved', 'Pending'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractors');
    }
}
