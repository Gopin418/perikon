<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IkoStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iko_status', function(Blueprint $table) {
            $table->integer('id')
            ->unsigned()
            ->references('id')
            ->on('pengantar')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->enum('status', ['Pending', 'Approved'])->default('Pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iko_status');
    }
}
