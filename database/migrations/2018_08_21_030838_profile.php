<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Profile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function(Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->nullable()->unsigned();
          $table->string('image')->default('avatar5');
          $table->string('mime')->nullable();
          $table->string('original_filename')->nullable();
          $table->string('bg')->default('photo1');
          $table->string('bg_mime')->nullable();
          $table->string('bg_original_filename')->nullable();
          $table->string('company')->default('Nutrifood');
          $table->enum('sub', ['Ciawi', 'Cibitung'])->default('Ciawi');
          $table->string('departement')->default('SSA')->references('departement')->on('departements');
          $table->string('fname')->references('firstname')->on('users');
          $table->string('lname')->references('lastname')->on('users');
          $table->string('phone')->nullable();
          $table->string('email')->references('email')->on('users');
          $table->string('bio')->nullable();

          // foreign key
          $table->foreign('user_id', 'foreign_user_id')
          ->references('id')
          ->on('users')
          ->onUpdate('cascade')
          ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
