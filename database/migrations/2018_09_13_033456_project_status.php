<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_status', function(Blueprint $table) {
            $table->increments('id');
            $table->char('no_iko')
            ->references('no_iko')
            ->on('pengantar')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->enum('iko_status', ['Pending', 'Approved'])
            ->references('status')
            ->on('iko_status')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('no_jsa')
            ->references('no_jsa')
            ->on('jsa')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->enum('jsa_status', ['Pending', 'Approved'])
            ->references('status')
            ->on('jsa_status')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('no_adl')
            ->references('no_adl')
            ->on('adl')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->enum('adl_status', ['Pending', 'Approved'])
            ->references('status')
            ->on('adl_status')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('no_ipb')
            ->references('no_ipb')
            ->on('ipbs')
            ->onDelete('cascade')
            ->onUpdate('cascade')
            ->nullable();
            $table->enum('ipb_status',['Pending', 'Approved'])
            ->references('status')
            ->on('ipb_status')
            ->onDelete('cascade')
            ->onUpdate('cascade')
            ->nullable();
            $table->date('tanggal_mulai')
            ->references('tanggal_mulai')
            ->on('pengantar')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->date('tanggal_selesai')
            ->references('tanggal_selesai')
            ->on('pengantar')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->enum('status', ['Pending', 'Upcoming', 'Ongoing', 'Finished'])
            ->default('Pending');
            $table->enum('project_status', ['Pending', 'Approved'])
            ->default('Pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_status');
    }
}
