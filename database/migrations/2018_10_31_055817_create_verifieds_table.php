<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifiedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verifieds', function (Blueprint $table) {
            $table->increments('id');
            $table->char('contractor_uri_id')->references('uri_id')->on('contractors')->onUpdate('cascade')->onDelete('cascade');
            $table->char('code_uri_id')->references('uri_id')->on('codes')->onUpdate('cascade')->onDelete('cascade');
            $table->enum('status', ['Verified', 'Pending', 'Outdated']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifieds');
    }
}
