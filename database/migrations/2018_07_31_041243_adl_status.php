<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdlStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adl_status', function(Blueprint $table) {
            $table->integer('id')
            ->unsigned()
            ->references('id')
            ->on('adl')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->enum('status', ['Pending', 'Approved'])->default('Pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adl_status');
    }
}
