<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JsaStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jsa_status', function (Blueprint $table) {
            $table->integer('id')
            ->unsigned()
            ->references('id')
            ->on('jsa')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->enum('status', ['Pending', 'Approved'])->default('Pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jsa_status');
    }
}
