<?php

use Illuminate\Database\Seeder;
use App\profile;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $profile = new profile;
      $profile->image = 'avatar2.png';
      $profile->bg = 'photo1.png';
      $profile->user_id = 1;
      $profile->fname = 'Rizky';
      $profile->lname = 'Ayu';
      $profile->phone = '08983201224';
      $profile->email = 'rizky@hse.com';
      $profile->bio = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante';
      $profile->save();

      $profile2 = new profile;
      $profile2->image = 'avatar04.png';
      $profile2->bg = 'photo2.png';
      $profile2->user_id = 2;
      $profile2->fname = 'Ridho';
      $profile2->lname = '';
      $profile2->phone = '08983201224';
      $profile2->email = 'ridho@hse.com';
      $profile2->bio = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante';
      $profile2->save();

      $profile3 = new profile;
      $profile3->image = 'avatar5.png';
      $profile3->bg = 'photo4.jpg';
      $profile3->user_id = 3;
      $profile3->fname = 'Alvin';
      $profile3->lname = 'Ardiansyah';
      $profile3->phone = '08983201224';
      $profile3->email = 'alvinardiansyah.18.4@gmail.com';
      $profile3->bio = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante';
      $profile3->save();
    }
}
