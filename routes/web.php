<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/contractor/{id}/verif/{code_id}', 'indexController@verif')->name('verif');
Route::post('/contractor/{id}/verif/{code_id}', 'indexController@verify')->name('verify');

Route::get('/', 'indexController@index')->name('index');
Route::post('/', 'indexController@start')->name('start');


Route::get('/export/jsa', 'JSAController@export');

Route::get('/perikon', 'MailController@send')->name('finish');
Route::get('/hold', 'MailController@hold')->name('hold');

Route::get('/notify', 'PusherController@sendNotification');
Route::get('/notify-ipb', 'PusherController@sendIPB');

    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // // Registration Routes...
    // Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    // Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//
// Route::get('/notify', 'PusherController@sendNotification');
// Route::get('/home', function(){
//   return view('test');
// });

//IKO
Route::get('/IKO', [
  'as' => 'iko',
  'uses' => 'HSEController@IKO'
]);
Route::post('/IKO', [
  'as' => 'store-iko',
  'uses' => 'HSEController@store'
]);

//JSA
Route::get('/JSA', [
  'as' => 'jsa',
  'uses' => 'HSEController@JSA'
]);
Route::post('/JSA', [
  'as' => 'store-jsa',
  'uses' => 'HSEController@storeJSA'
]);

//ADL
Route::get('/ADL', [
  'as' => 'adl',
  'uses' => 'HSEController@ADL'
]);
Route::post('/ADL', [
  'as' => 'store-adl',
  'uses' => 'HSEController@storeADL'
]);

//IPB
Route::prefix('IPB')->group(function() {
    Route::get('/', function() {
      return redirect()->route('guest-404');
    });

    Route::get('/sd7ashb29ndjf92j123', [
      'as' => 'ipb',
      'uses' => 'HSEController@IPB'
    ]);

    Route::post('/', [
      'as' => 'store-ipb',
      'uses' => 'HSEController@storeLampiran'
    ]);

    Route::get('/ruang-terbatas', [
      'as' => 'ruang-terbatas',
      'uses' => 'HSEController@ruangTerbatas'
    ]);
    Route::post('/ruang-terbatas', [
      'as' => 'store-ruang-terbatas',
      'uses' => 'HSEController@storeRuangTerbatas'
    ]);

    Route::get('/kebakaran', [
      'as' => 'kebakaran',
      'uses' => 'HSEController@kebakaran'
    ]);
    Route::post('/kebakaran', [
      'as' => 'store-kebakaran',
      'uses' => 'HSEController@storeKebakaran'
    ]);

    Route::get('/ketinggian', [
      'as' => 'ketinggian',
      'uses' => 'HSEController@ketinggian'
    ]);
    Route::post('/ketinggian', [
      'as' => 'store-ketinggian',
      'uses' => 'HSEController@storeKetinggian'
    ]);
});


Route::prefix('admin')->group(function () {

    //Dashboard
    Route::get('/', [
      'middleware' => 'auth',
      'as' => 'dashboard',
      'uses' => 'AdminController@index'
    ]);

    //Contractor request
    Route::prefix('request')->group(function() {

      // Get main page
      Route::get('/', [
        'middleware' => 'auth',
        'as' => 'request',
        'uses' => 'AdminController@request'
      ]);

      // Approve
      Route::put('/{id}', [
        'middleware' => 'auth',
        'as' => 'requestApprove',
        'uses' => 'AdminController@requestApprove'
      ]);

      // Decline
      Route::post('/{id}', [
        'middleware' => 'auth',
        'as' => 'requestDecline',
        'uses' => 'AdminController@requestDecline'
      ]);

    });

    Route::prefix('dashboard')->group(function() {

      // Get upcoming data table dashboard
      Route::get('/upcoming', [
        'middleware' => 'auth',
        'as' => 'upcomingData',
        'uses' => 'AdminController@upcoming'
      ]);

      Route::get('/ongoing', [
        'middleware' => 'auth',
        'as' => 'ongoingData',
        'uses' => 'AdminController@ongoing'
      ]);
    });

    Route::prefix('profile')->group(function() {
      Route::get('/', [
        'as' => 'profile',
        'uses' => 'profileController@index'
      ]);

      Route::put('/pf', [
        'as' => 'profile-picture',
        'uses' => 'profileController@update'
      ]);
      Route::put('/bg', [
        'as' => 'profile-background',
        'uses' => 'profileController@updateBg'
      ]);

    });

    Route::prefix('utility')->group(function() {

      // Halaman Approval Utility
      Route::get('/', [
        'as' => 'pj-utility',
        'uses' => 'UtilityController@index'
      ]);

      //post utility
      Route::put('/air/{id}', [
        'as' => 'pj-air',
        'uses' => 'UtilityController@updateAir'
      ]);
      Route::put('/angin/{id}', [
        'as' => 'pj-angin',
        'uses' => 'UtilityController@updateAngin'
      ]);
      Route::put('/listrik/{id}', [
        'as' => 'pj-listrik',
        'uses' => 'UtilityController@updateListrik'
      ]);
      Route::put('/steam/{id}', [
        'as' => 'pj-steam',
        'uses' => 'UtilityController@updateSteam'
      ]);
    });

    //Admin list
    Route::get('/list', [
      'as' => 'user-list',
      'uses' => 'Admin@index'
    ]);
    Route::put('/list/{id}', [
      'as' => 'user-edit',
      'uses' => 'Admin@update'
    ]);
    Route::delete('/list/{id}', [
      'as' => 'user-delete',
      'uses' => 'Admin@destroy'
    ]);

    //Halaman Role
    Route::get('/role', [
      'as' => 'role',
      'uses' => 'Admin@role'
    ]);

    //Halaman Add User
    Route::get('/add', [
      'as' => 'add-user',
      'uses' => 'Admin@add'
    ]);
    Route::post('/add', [
      'as' => 'store-user',
      'uses' => 'Admin@store'
    ]);

    Route::prefix('IKO')->group(function () {

        Route::get('/pending', [
          'as' => 'iko-pending',
          'uses' => 'IKO@pending'
        ]);
        Route::put('/pending/{id}', [
          'as' => 'iko-approve',
          'uses' => 'IKO@update'
        ]);
        Route::get('/approved', [
          'as' => 'iko-approved',
          'uses' => 'IKO@approved'
        ]);

        Route::get('export/{type}', [
          'middleware' => 'auth',
          'as' => 'export',
          'uses' => 'IKO@export'
        ]);
    });


    Route::prefix('JSA')->group(function () {

        Route::get('/pending', [
          'as' => 'jsa-pending',
          'uses' => 'JSAController@pending'
        ]);
        Route::put('/pending/{id}', [
          'as' => 'jsa-approve',
          'uses' => 'JSAController@update'
        ]);

        Route::get('/approved', [
          'as' => 'jsa-approved',
          'uses' => 'JSAController@approved'
        ]);

    });


    Route::prefix('ADL')->group(function () {

        Route::get('/pending', [
          'as' => 'adl-pending',
          'uses' => 'ADLController@pending'
        ]);
        Route::put('/pending/{id}', [
          'as' => 'adl-approve',
          'uses' => 'ADLController@update'
        ]);

        Route::get('/approved', [
          'as' => 'adl-approved',
          'uses' => 'ADLController@approved'
        ]);

    });

    Route::prefix('IPB')->group(function() {

      Route::get('/pending', [
        'as' => 'ipb-pending',
        'uses' => 'IPBController@pending'
      ]);
      Route::put('/pending/{id}', [
        'as' => 'ipb-approve',
        'uses' => 'IPBController@update'
      ]);

      Route::get('/approved', [
        'as' => 'ipb-approved',
        'uses' => 'IPBController@approved'
      ]);

    });

    Route::get('404', [
      'as' => '404',
      'uses' => 'ErrorController@notFound'
    ]);

    Route::get('500', [
      'as' => '500',
      'uses' => 'ErrorController@fatal'
    ]);
});

Route::get('/chat', 'ChatController@getIndex');


Route::get('404', [
  'as' => 'guest-404',
  'uses' => 'ErrorController@guestNotFound'
]);

Route::get('500', [
  'as' => 'guest-500',
  'uses' => 'ErrorController@guestFatal'
]);
