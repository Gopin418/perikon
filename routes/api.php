<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('iko')->group(function(){
  Route::get('/', [
    'uses' => 'DataController@iko'
  ]);
  Route::get('/pending', [
    'uses' => 'DataController@ikoPending'
  ]);
});

Route::prefix('jsa')->group(function(){
  Route::get('/', [
    'uses' => 'DataController@jsa'
  ]);
  Route::get('/pending', [
    'uses' => 'DataController@jsaPending'
  ]);
});

Route::prefix('adl')->group(function(){
  Route::get('/',[
    'uses' => 'DataController@adl'
  ]);
  Route::get('/pending',[
    'uses' => 'DataController@adlPending'
  ]);
});

Route::prefix('ipb')->group(function(){
  Route::get('/',[
    'uses' => 'DataController@ipb'
  ]);
  Route::get('/pending',[
    'uses' => 'DataController@ipbPending'
  ]);
});

Route::get('/maintenance', [
  'uses' => 'DataController@maintenance'
]);

Route::get('/project',[
  'uses' => 'DataController@project'
]);

Route::get('/key', [
  'uses' => 'DataController@key'
]);
