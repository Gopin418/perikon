<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    {{-- Bootstrap CSS --}}
    {{ Asset::add('bootstrap-css', '/css/bootstrap.css') }}
    {{ Asset::add('Material Design', '/css/mdb.min.css') }}
    {{ Asset::add('fontawesom', '/css/font-awesome.css') }}
    {{ Asset::add('master-css', '/css/master.css') }}
    {{ Asset::add('iko', '/css/iko.css') }}
    {{ Asset::add('datepicker-css', '/css/datepicker.css') }}
    {{ Asset::add('smartwizard-css', '/css/smart_wizard.css')}}

    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Izin Kerja Kontraktor</title>


</head>

<body>
    <div class="loading-page yellow darken-3 text-white" id="loading-page">
        <div class="fa-animation flex-center">
            <i class="fa fa-cog fa-lg fa-spin"></i>
        </div>
    </div>


    <!-- Content Start Here -->
    <div class="col-sm-12 p-0">
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-12 col-sm-10 col-md-8 col-lg-6 pt-lg-5">

                    <div class="shadow mt-5">
                        <div class="kuning"></div>
                        <div class="card border border-white rounded-0">
                            <div class="card-body border border-white p-0">
                                <form action="{{route('store-iko')}}" method="post" id="iko-form">
                                  {{ csrf_field() }}

                                    <div id="smartwizard">
                                        <ul hidden>
                                            <li><a href="#step-1"></a></li>
                                            <li><a href="#step-2"></a></li>
                                            <li><a href="#step-3"></a></li>
                                            <li><a href="#step-4"></a></li>
                                            <li><a href="#step-5"></a></li>
                                            <li><a href="#step-6"></a></li>
                                            <li><a href="#step-7"></a></li>
                                            <li><a href="#step-8"></a></li>
                                        </ul>

                                        <div class="form">

                                            {{-- Step 1 --}}
                                            {{-- Izin Kerja Kontraktor --}}
                                            <div id="step-1" class="">

                                                <div class="form">
                                                    {{-- Header --}}
                                                    <h2 class="card-title">Izin Kerja Kontraktor</h2>
                                                    <small class="card-text text-muted">
                                                        Silahkan isi form IKO sebelum melanjutkan
                                                    </small>

                                                    {{-- Nama Project --}}
                                                    <div class="md-form kun mt-4">
                                                        <input type="text" name="nama_pekerjaan" id="nama_pekerjaan" aria-describeby="inputPekerjaanHelpBlok" class="form-control">
                                                        <label for="nama_pekerjaan">Nama Pekerjaan</label>
                                                        <small id="inputPekerjaanHelpBlock" class="form-text text-muted">
                                                            Nama pekerjaan yang akan dilakaukan. Misalnya, Perbaikan jalan
                                                        </small>
                                                    </div>
                                                    {{-- /Nama Project --}}

                                                    {{-- Jenis Project --}}
                                                    <div class="mt-4 kun">
                                                        <small class="card-text text-muted">Jenis Pekerjaan</small>
                                                        <br>
                                                        <input type="radio" name="jenis" id="project" value="Project">
                                                        <label for="project">Project</label>
                                                        <br>
                                                        <input type="radio" name="jenis" id="maintenance" value="Maintenance">
                                                        <label for="maintenance">Maintenance</label>
                                                    </div>

                                                    {{-- /Jenis Project --}}

                                                    {{-- Periode Pekerjaan --}}
                                                    <div class="mt-4">
                                                        <div class="md-form kun">
                                                            <input type="hidden" name="tanggal_mulai" id="tgl_mulai">
                                                            <input type="text" name="tgl_mulai" id="tanggal_mulai" class="form-control" style="cursor: pointer;background-color: #fff; color: transparent; text-shadow: 0 0 0 #495057; &:focus{outline:none;}"
                                                              readonly>
                                                            <label for="tanggal_mulai">Tanggal Mulai</label>
                                                        </div>

                                                        <div class="md-form kun">
                                                            <input type="hidden" name="tanggal_selesai" id="tgl_selesai">
                                                            <input type="text" name="tgl_selesai" id="tanggal_selesai" class="form-control" style="cursor: pointer;background-color: #fff; color: transparent; text-shadow: 0 0 0 #495057; &:focus{outline:none;}"
                                                              readonly>
                                                            <label for="tanggal_selesai">Tanggal Selesai</label>
                                                        </div>
                                                    </div>
                                                    {{-- /Periode Pekerjaan --}}

                                                    {{-- Footer Message --}}
                                                    <div class="mt-5 mb-4">
                                                        <div class="card border border-warning text-center">
                                                            <div class="card-body p-2 pl-1 pr-1">
                                                                <small class="card-text"><i class="fa fa-exclamation-triangle d-none d-xl-inline"></i>&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum melanjutkan&nbsp;&nbsp;<i class="fa fa-exclamation-triangle d-none d-xl-inline"></i></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer Message --}}

                                                    {{-- Footer IKO --}}
                                                    <div class="row no-gutters text-muted">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                        <div class="col-2 text-center">
                                                            <small>IKO</small>
                                                        </div>
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer IKO --}}

                                                </div>






                                            </div>
                                            {{-- /Izin Kerja Kontraktor --}}
                                            {{-- /Step 1 --}}

                                            {{-- Step 2 --}}
                                            <div id="step-2" class="">

                                                <div class="form">

                                                    {{-- Header --}}
                                                    <h2>Izin Kerja Kontraktor</h2>
                                                    {{-- /Header --}}

                                                    {{-- Hint Form --}}
                                                    <div class="mt-3">
                                                        <small class="card-text">Silahkan isi form IKO sebelum melanjutkan</small>
                                                    </div>
                                                    {{-- /Hint Form --}}


                                                    <!-- Nama Kontraktor -->
                                                    <div class="md-form mt-4 kun">
                                                        <input type="text" name="nama_kontraktor" id="nama_kontraktor" aria-describedby="inputNamaKontraktorHelper" class="form-control input-md">
                                                        <label for="nama_kontraktor">Nama Kontraktor</label>
                                                        <small id="inputNamaKontraktorHelper" class="form-text text-muted">
                                                            Nama kontraktor yang akan bekerja. Misalnya, PT. Roda Jaya
                                                        </small>
                                                    </div>

                                                    <!-- Penanggung Jawab Kontraktor -->
                                                    <div class="md-form kun">
                                                        <input type="text" name="pj_kontraktor" id="pj_kontraktor" class="form-control input-md" aria-describedby="inputPJKontraktorHelper">
                                                        <label for="pj_kontraktor">Penanggung Jawab Kontraktor</label>
                                                        <small id="inputPJKontraktor" class="form-text text-muted">
                                                            Nama penanggung jawab dari Kontraktor. Misalnya, Alvin Ardiansyah
                                                        </small>
                                                    </div>

                                                    <!-- No. Telp. Penanggung Jawab Kontraktor -->
                                                    <div class="md-form kun">
                                                        <input type="tel" name="telp_pj_kontraktor" id="telp_pj_kontraktor" class="form-control input-md" aria-describedby="inputTelpKontraktor">
                                                        <label for="telp_pj_kontraktor">No. Telp.</label>
                                                        <small id="inputTelpKontraktor" class="form-text text-muted">
                                                            Nomor telp penanggung jawab kontraktor yang dapat dihubungi
                                                        </small>
                                                    </div>

                                                    <!-- Penanggung Jawab Lapangan -->
                                                    <div class="md-form kun">
                                                        <input type="text" name="pj_lapangan" id="pj_lapangan" class="form-control input-md" aria-describedby="inputPJLapangan">
                                                        <label for="pj_lapangan">Penanggung Jawab Lapangan</label>
                                                        <small id="inputPJLapangan" class="form-text text-muted">
                                                            Nama penanggung jawab di lapangan. Misalnya, Alvin Ardiansyah
                                                        </small>
                                                    </div>

                                                    <!-- No. Telp. Penanggung Jawab Lapangan -->
                                                    <div class="md-form kun">
                                                        <input type="tel" name="telp_pj_lapangan" id="telp_pj_lapangan" class="form-control input-md" aria-describedby="inputTelpLapangan">
                                                        <label for="telp_pj_lapangan">No. Telp.</label>
                                                        <small id="inputTelpLapangan" class="form-text text-muted">
                                                            Nomor telp. penanggung jawab lapangan yang dapat dihubungi
                                                        </small>
                                                    </div>

                                                    {{-- Footer Message --}}
                                                    <div class="mt-5 mb-4">
                                                        <div class="card border border-warning text-center">
                                                            <div class="card-body p-2 pl-1 pr-1">
                                                                <small class="card-text"><i class="fa fa-exclamation-triangle d-none d-xl-inline"></i>&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum melanjutkan&nbsp;&nbsp;<i class="fa fa-exclamation-triangle d-none d-xl-inline"></i></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer Message --}}
                                                    {{-- Footer IKO --}}
                                                    <div class="row no-gutters text-muted">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                        <div class="col-2 text-center">
                                                            <small>IKO</small>
                                                        </div>
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer IKO --}}

                                                </div>



                                            </div>
                                            {{-- /Step 2 --}}

                                            {{-- Step 3 --}}
                                            <div id="step-3" class="">

                                                <div class="form">

                                                    {{-- Header --}}
                                                    <h2>Izin Kerja Kontraktor</h2>
                                                    {{-- /Header --}}

                                                    {{-- Hint --}}
                                                    <div class="mt-3">
                                                        <small class="card-text">
                                                            Silahkan isi form IKO sebelum melanjutkan
                                                        </small>
                                                    </div>
                                                    {{-- /Hint --}}

                                                    {{-- Nama Penanggung Jawab Project --}}
                                                    <div class="md-form kun mt-4">
                                                        <input type="text" name="pj_project" id="pj_project" class="form-control" aria-describedby="inputPJProject">
                                                        <label for="pj_project">Penanggung Jawab Project</label>
                                                        <small id="inputPJProject" class="form-text text-muted">
                                                            Nama penanggung jawab/pemilik project/maintenance dari Nutrifood. Misalnya, Alvin Ardiansyah
                                                        </small>
                                                    </div>
                                                    {{-- /Nama Penanggung Jawab Project --}}

                                                    {{-- No Telp Penanggung Jawab --}}
                                                    <div class="md-form kun">
                                                        <input type="tel" name="telp_pj_project" id="telp_pj_project" class="form-control" aria-describedby="inputTelpProject">
                                                        <label for="telp_pj_project">No. Telp.</label>
                                                        <small id="inputTelpProject" class="form-text text-muted">
                                                            Nomor telp. penanggung jawab/pemilik project/maintenance yang dapat dihubungi
                                                        </small>
                                                    </div>
                                                    {{-- /No Telp Penanggung Jawab --}}

                                                    {{-- Footer Message --}}
                                                    <div class="mt-5 mb-4">
                                                        <div class="card border border-warning text-center">
                                                            <div class="card-body p-2 pl-1 pr-1">
                                                                <small class="card-text"><i class="fa fa-exclamation-triangle d-none d-xl-inline"></i>&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum melanjutkan&nbsp;&nbsp;<i class="fa fa-exclamation-triangle d-none d-xl-inline"></i></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer Message --}}

                                                    {{-- Footer IKO --}}
                                                    <div class="row no-gutters text-muted">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                        <div class="col-2 text-center">
                                                            <small>IKO</small>
                                                        </div>
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer IKO --}}

                                                </div>

                                            </div>
                                            {{-- /Step 3 --}}

                                            {{-- Step 4 --}}
                                            <div id="step-4" class="">

                                                <div class="form">

                                                    {{-- Header --}}
                                                    <h2>Izin Kerja Kontraktor</h2>
                                                    {{-- /Header --}}

                                                    {{-- Hint --}}
                                                    <div class="mt-3 mb-4"><small class="card-text">Silahkan isi form IKO sebelum melanjutkan</small></div>
                                                    {{-- /Hint --}}

                                                    {{-- Radio Lokasi --}}
                                                    <small class="card-text">Lokasi Pekerjaan</small>
                                                    {{-- Ciawi --}}
                                                    <div class="kun mb-1 mt-1">
                                                        <input type="radio" name="lokasi" id="ciawi" value="Ciawi">
                                                        <label for="ciawi">Ciawi</label>
                                                    </div>
                                                    {{-- /Ciawi --}}

                                                    <div class="kun">
                                                        <input type="radio" name="lokasi" id="cibitung" value="Cibitung">
                                                        <label for="cibitung">Cibitung</label>
                                                    </div>
                                                    {{-- /Radio Lokasi --}}

                                                    {{-- Footer Message --}}
                                                    <div class="mt-4 mb-4">
                                                        <div class="card border border-warning text-center">
                                                            <div class="card-body p-2 pl-1 pr-1">
                                                                <small class="card-text"><i class="fa fa-exclamation-triangle d-none d-xl-inline"></i>&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum melanjutkan&nbsp;&nbsp;<i class="fa fa-exclamation-triangle d-none d-xl-inline"></i></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer Message --}}

                                                    {{-- Footer --}}
                                                    <div class="row no-gutters text-muted">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                        <div class="col-2 text-center"><small>IKO</small></div>
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer --}}

                                                </div>

                                            </div>
                                            {{-- /Step 4 --}}

                                            {{-- Step 5 --}}
                                            <div id="step-5" class="">

                                                <div class="form">

                                                    {{-- Header --}}
                                                    <h2>Izin Kerja Kontraktor</h2>
                                                    {{-- /Header --}}

                                                    {{-- Hint --}}
                                                    <div class="mt-3">
                                                        <small class="card-text">
                                                            Silahkan isi form IKO sebelum melanjutkan
                                                        </small>
                                                    </div>
                                                    {{-- /Hint --}}

                                                    {{-- Deskripsi --}}
                                                    <div class="md-form mt-3 kun">
                                                        <textarea name="deskripsi" id="desc_project" class="form-control md-textarea" rows="5" aria-describedby="inputDeskripsi"></textarea>
                                                        <label for="desc_project">Deskripsi Project/Maintenance</label>
                                                        <small id="inputDeskripsi" class="form-text text-muted">
                                                            Deskripsikan project/maintenance yang akan dikerjakan
                                                        </small>
                                                    </div>
                                                    {{-- /Deskripsi --}}

                                                    {{-- Lokasi --}}
                                                    <div class="md-form kun">
                                                        <input type="text" name="area" id="lokasi_project" class="form-control inpput-md" aria-describedby="inputArea">
                                                        <label for="lokasi_project">Area Pekerjaan</label>
                                                        <small id="inputArea" class="form-text text-muted">Area pekerjaan yang akan dilakukan,. Misalnya, Jalan Utama</small>
                                                    </div>
                                                    {{-- /Lokasi --}}


                                                    {{-- Footer Message --}}
                                                    <div class="mt-5 mb-4">
                                                        <div class="card border border-warning text-center">
                                                            <div class="card-body p-2 pl-1 pr-1">
                                                                <small class="card-text"><i class="fa fa-exclamation-triangle d-none d-xl-inline"></i>&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum melanjutkan&nbsp;&nbsp;<i class="fa fa-exclamation-triangle d-none d-xl-inline"></i></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer Message --}}


                                                    {{-- Footer --}}
                                                    <div class="row no-gutters text-muted">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                        <div class="col-2 text-center">
                                                            <small>IKO</small>
                                                        </div>
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer --}}

                                                </div>

                                            </div>
                                            {{-- /Step 5 --}}

                                            {{-- Step 6 --}}
                                            <div id="step-6" class="">

                                                <div class="form">

                                                    {{-- Header --}}
                                                    <h2>Izin Kerja Kontraktor</h2>
                                                    {{-- /Header --}}

                                                    {{-- Hint --}}
                                                    <div class="mt-3">
                                                        <small class="card-text">
                                                            Silahkan isi form IKO sebelum melanjutkan
                                                        </small>
                                                    </div>
                                                    {{-- /Hint --}}

                                                    {{-- Area GMP --}}
                                                    <div class="md-form kun mt-3">
                                                        <input type="checkbox" name="area_gmp" id="area_gmp" value="Ya" aria-describedby="inputGMP">
                                                        <label for="area_gmp">Bekerja Pada Area GMP</label>
                                                        <small id="inputGMP" class="form-text text-muted">Jangan dicheck/Abaikan jika tidak bekerja pada area GMP (Good Manufacturing Practice)</small>
                                                    </div>
                                                    {{-- /Area GMP --}}

                                                    {{-- Barang dan Peralatan yang disimpan --}}
                                                    <div class="md-form pt-2" id="gmp">

                                                        <div class="md-form input-group mb-2 kun" id="gmp_template">
                                                            <input type="text" name="brg_yang_disimpan[]" id="barang" class="form-control">
                                                            <label for="barang">Alat dan Barang yang disimpan</label>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-danger waves-effect m-0 p-2 pl-3 pr-3 shadow-none" id="gmp_remove_current"><span class="fa fa-minus"></span></button>
                                                                <button class="btn btn-success waves-effect m-0 p-2 pl-3 pr-3 shadow-none" id="gmp_add"><span class="fa fa-plus"></span></button>
                                                            </div>
                                                        </div>


                                                        <div id="gmp_noforms_template"></div>

                                                    </div>

                                                    {{-- /Barang dan peralatan yang disimpan --}}

                                                    {{-- Lokasi Gudang --}}
                                                    <div class="md-form kun">
                                                        <input type="text" name="lokasi_gudang" id="lokasi_gudang" class="form-control" aria-describedby="inputGudang">
                                                        <label for="lokasi_gudang">Lokasi Gudang Penyimpanan</label>
                                                        <small id="inputGudang" class="form-text text-muted">
                                                            lokasi gudang untuk penyimpanan alat dan barang
                                                        </small>
                                                    </div>
                                                    {{-- /Lokasi Gudang --}}

                                                    {{-- Footer Message --}}
                                                    <div class="mt-5 mb-4">
                                                        <div class="card border border-warning text-center">
                                                            <div class="card-body p-2 pl-1 pr-">
                                                                <small class="card-text"><i class="fa fa-exclamation-triangle d-none d-xl-inline"></i>&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum melanjutkan&nbsp;&nbsp;<i class="fa fa-exclamation-triangle d-none d-xl-inline"></i></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer Message --}}

                                                    <div class="row no-gutters text-muted">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                        <div class="col-2 text-center"><small>IKO</small></div>
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            {{-- /Step 6 --}}

                                            {{-- Step 7 --}}
                                            <div id="step-7" class="">

                                                <div class="form">

                                                    {{-- Header --}}
                                                    <h2>Izin Kerja Kontraktor</h2>
                                                    {{-- /Header --}}

                                                    {{-- Hint --}}
                                                    <div class="mt-3"><small class="card-text">Silahkan isi form IKO sebelum melanjutkan</small></div>
                                                    {{-- /Hint --}}

                                                    {{-- Jenis Pekerjaan Berbahaya --}}
                                                    <div class="md-form">
                                                        <div class="mt-4">
                                                            <small class="text-muted">Pekerjaan Berbahaya</small>
                                                        </div>
                                                        <div id="check-ipb">
                                                            {{-- Api --}}
                                                            <div class="mb-1 kun">
                                                                <input type="checkbox" name="pekerjaan_berbahaya[]" id="api" value="Api" aria-describedby="inputApi">
                                                                <label for="api">Kebakaran</label>
                                                                <small id="inputApi" class="form-text text-muted">(mudah terbakar / dapat meledak)</small>
                                                            </div>
                                                            {{-- /Api --}}
                                                            {{-- Ketinggian --}}
                                                            <div class="mb-1 kun">
                                                                <input type="checkbox" name="pekerjaan_berbahaya[]" id="ketinggian" value="Ketinggian" aria-describedby="inputKetinggian">
                                                                <label for="ketinggian">Ketinggian</label>
                                                                <small id="inputKetinggian" class="form-text text-muted">(Ketinggian > 2m)</small>
                                                            </div>
                                                            {{-- /Ketinggian --}}
                                                            {{-- Ruang Terbatas --}}
                                                            <div class="mb-1 kun">
                                                                <input type="checkbox" name="pekerjaan_berbahaya[]" id="ruang_terbatas" value="Ruang Terbatas" aria-describedby="inputRuangTerbatas">
                                                                <label for="ruang_terbatas">Ruang Terbatas</label>
                                                                <small id="inputRuangTerbatas" class="form-text text-muted">(Berada dalam ruangan)</small>
                                                            </div>
                                                            {{-- /Ruang Terbatas --}}
                                                        </div>
                                                        <div id="check-kimia">
                                                            {{-- Bahan Kimia Berbahaya --}}
                                                            <div class="kun">
                                                                <input type="checkbox" name="pekerjaan_berbahaya[]" id="kimia" value="Bahan Kimia Berbahaya" aria-activedescendant="inputKimia">
                                                                <label for="kimia">Bahan kimia Berbahaya</label>
                                                                <small id="inputKimia" class="form-text text-muted">(Bahan Beracun / Bahan Korosif / Bahan Oksidator)</small>
                                                            </div>
                                                            {{-- /Bahan Kimia Berbahaya --}}
                                                        </div>
                                                    </div>
                                                    {{-- /Jenis Pekerjaan Berbahaya --}}

                                                    {{-- Dokumen Lampiran --}}
                                                    <div class="md-form">
                                                        <div class="mt-5">
                                                            <small class="card-text">Dokumen Lampiran yang dibutuhkan</small>
                                                        </div>
                                                        {{-- IPB --}}
                                                        <div class="kun">
                                                            <input type="checkbox" name="doc_lampiran[]" id="ipb" value="IPB">
                                                            <label style="cursor:default">IPB</label>
                                                        </div>
                                                        {{-- /IPB --}}
                                                        {{-- MSDS --}}
                                                        <div class="kun">
                                                            <input type="checkbox" name="doc_lampiran[]" id="msds" value="MSDS">
                                                            <label style="cursor:default">MSDS</label>
                                                        </div>
                                                        {{-- /MSDS --}}
                                                        {{-- SO --}}
                                                        <div class="kun">
                                                            <input type="checkbox" name="doc_lampiran[]" id="so" value="SO" checked>
                                                            <label style="cursor:default">Struktur Organisasi</label>
                                                        </div>
                                                        {{-- /SO --}}
                                                    </div>
                                                    {{-- /Dokumen Lampiran --}}

                                                    {{-- Kebutuhan Utility --}}
                                                    <div class="mt-5">
                                                        <small class="card-text">Kebutuhan Utility</small>
                                                    </div>
                                                    <div class="row kun">
                                                        <div class="col-2">
                                                            <input type="checkbox" name="utility[]" id="air" value="Air" aria-describedby="inputAir">
                                                            <label for="air">Air</label>
                                                        </div>
                                                    </div>
                                                    <div class="row kun">
                                                        <div class="col-2">
                                                            <input type="checkbox" name="utility[]" id="angin" value="Angin">
                                                            <label for="angin">Angin</label>
                                                        </div>
                                                    </div>
                                                    <div class="row kun">
                                                        <div class="col-2">
                                                            <input type="checkbox" name="utility[]" id="listrik" value="Listrik">
                                                            <label for="listrik">Listrik</label>
                                                        </div>
                                                    </div>
                                                    <div class="row kun">
                                                        <div class="col-2">
                                                            <input type="checkbox" name="utility[]" id="steam" value="Steam">
                                                            <label for="steam">Steam</label>
                                                        </div>
                                                    </div>
                                                    {{-- /Kebutuhan Utility --}}

                                                    {{-- Sistem yang akan terganggu --}}
                                                    <div class="md-form">
                                                        <div class="mt-5">
                                                            <small class="card-text">Sistem yang akan terganggu</small>
                                                        </div>
                                                        <div class="kun">
                                                            <input type="checkbox" name="sistem_terganggu[]" id="fire" value="Fire Alarm">
                                                            <label for="fire">Fire Alarm</label>
                                                        </div>
                                                        <div class="kun">
                                                            <input type="checkbox" name="sistem_terganggu[]" id="pest" value="Pest Control">
                                                            <label for="pest">Pest Control</label>
                                                        </div>
                                                        <div class="kun">
                                                            <input type="checkbox" name="sistem_terganggu[]" id="lalin" value="Lalu Lintas">
                                                            <label for="lalin">Lalu Lintas</label>
                                                        </div>
                                                        <div class="kun">
                                                            <input type="checkbox" name="sistem_terganggu[]" id="lainnya" value="">
                                                            <label for="lainnya">Lainnya</label>
                                                        </div>
                                                        <div class="md-form" id="lain" style="visibility : hidden;">

                                                            {{-- template --}}
                                                            <div class="md-form input-group mb-2 kun" id="lain_template">
                                                                <input type="text" name="sistem_terganggu[]" id="lain_text" class="form-control" placeholder="Sistem Lainnya">
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-danger m-0 p-2 pl-3 pr-3 shadow-none" id="lain_remove_current"><span class="fa fa-minus"></span></button>
                                                                    <button class="btn btn-success m-0 p-2 pl-3 pr-3 shadow-none" id="lain_add"><span class="fa fa-plus"></span></button>
                                                                </div>
                                                            </div>
                                                            {{-- /template --}}

                                                            {{-- no template --}}
                                                            <div id="lain_noforms_template"></div>
                                                            {{-- /no template --}}

                                                        </div>
                                                    </div>
                                                    {{-- /Sistem yang akan terganggu --}}

                                                    {{-- Footer Message --}}
                                                    <div class="mt-5 mb-4">
                                                        <div class="card border border-warning text-center">
                                                            <div class="card-body p-2 pr-1 pl-1">
                                                                <small class="card-text"><i class="fa fa-exclamation-triangle d-none d-xl-inline"></i>&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum melanjutkan&nbsp;&nbsp;<i class="fa fa-exclamation-triangle d-none d-xl-inline"></i></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer Message --}}


                                                    {{-- Footer --}}
                                                    <div class="row no-gutters text-muted">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                        <div class="col-2 text-center">
                                                            <small>IKO</small>
                                                        </div>
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer --}}

                                                </div>

                                            </div>
                                            {{-- /Step 7 --}}

                                            {{-- Step 8 --}}
                                            <div id="step-8" class="">

                                                <div class="form">

                                                    <h2>Syarat dan Ketentuan</h2>

                                                    <div class="mt-3">
                                                        <ul class="card-text p-3">
                                                            <li class="mb-2">Seragam Kontraktor atau identitas wajib digunakan selama project/maintenance berlangsung</li>
                                                            <li class="mb-2">APD wajib digunakan</li>
                                                            <li class="mb-2">Sampah bekas project harus dibersihkan dan dibawa keluar area Nutrifood</li>
                                                            <li class="mb-2">Area atau lokasi project harus dibatasi</li>
                                                            <li>Jika terjadi pelanggaran, akan diterbitkan Laporan Ketidaksesuaian K3 atau Lingkungan (LKK atau LKL) dan mengurangi penilaian kontraktor</li>
                                                        </ul>
                                                    </div>

                                                    {{-- Footer --}}
                                                    <div class="row no-gutters text-muted">
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                        <div class="col-2 text-center">
                                                            <small>IKO</small>
                                                        </div>
                                                        <div class="col">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    {{-- /Footer --}}

                                                </div>

                                            </div>
                                            {{-- /Step 8 --}}

                                        </div>

                                    </div>

                            </div>
                            <input type="hidden" name="status" value="Pending">
                            <button type="submit" class="btn btn-indigo sw-btn-finish waves-effect float-right pl-4 pr-4">submit</button>
                            </form>
                        </div>
                    </div>
                    <footer>
                        <div class="mb-5 mt-5 text-center">
                            <small>
                                &copy;2018 Alvin Ardiansyah Maulana
                            </small>
                        </div>
                    </footer>
                </div>
                <div class="col">

                </div>
            </div>

        </div>
    </div>

    <!-- Content End Here -->



    {{-- jQuery Script here --}}
    <script src="{{ asset('/js/jquery.js') }}" charset="utf-8"></script>
    {{-- Material Design --}}
    <script src="{{ asset('/js/mdb.min.js') }}"></script>
    {{-- Daterange --}}
    <script src="{{ asset('/js/jquery-ui.js') }}" charset="utf-8"></script>
    {{-- Bootstrap JS --}}
    <script src="{{ asset('/js/bootstrap.bundle.js') }}" charset="utf-8"></script>

    <script src="{{ asset('/js/jquery.sheepItPlugin.js') }}" charset="utf-8"></script>
    {{-- Smart Wizard --}}
    <script src="{{ asset('/js/jquery.smartWizard.js') }}" charset="utf-8"></script>
    <script>
        $(window).on('load', function() {
            loadingPage();
            $(document).scrollTop(0);
        });

        function loadingPage() {
            setTimeout(function() {
                $('.loading-page').fadeOut('slow');
            }, 500);
        }

        $(document).ready(function() {

            $(this).scrollTop(0);

            // popovers Initialization
            $('[data-toggle="popover"]').popover();

            $('#tanggal_mulai').datepicker({
                todayHighlight: true,
                showAnim: "fadeIn",
                dateFormat: "DD, d MM yy",
                altField: "#tgl_mulai",
                altFormat: "yy-mm-dd",
                dayNamesMin: [
                    "Min",
                    "Sen",
                    "Sel",
                    "Rab",
                    "Kam",
                    "Jum",
                    "Sab"
                ],
                dayNames: [
                    "Minggu",
                    "Senin",
                    "Selasa",
                    "Rabu",
                    "Kamis",
                    "Jum'at",
                    "Sabtu"
                ],
                monthNames: [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                monthNamesShort: [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "Mei",
                    "Jun",
                    "Jul",
                    "Agu",
                    "Sep",
                    "Okt",
                    "Nov",
                    "Des"
                ],
                "firstDay": 1
            });

            $('#tanggal_selesai').datepicker({
                todayHighlight: true,
                showAnim: "fadeIn",
                dateFormat: "DD, d MM yy",
                altField: "#tgl_selesai",
                altFormat: "yy-mm-dd",
                dayNamesMin: [
                    "Min",
                    "Sen",
                    "Sel",
                    "Rab",
                    "Kam",
                    "Jum",
                    "Sab"
                ],
                dayNames: [
                    "Minggu",
                    "Senin",
                    "Selasa",
                    "Rabu",
                    "Kamis",
                    "Jum'at",
                    "Sabtu"
                ],
                monthNames: [
                    "Januari",
                    "Februari",
                    "Maret",
                    "April",
                    "Mei",
                    "Juni",
                    "Juli",
                    "Agustus",
                    "September",
                    "Oktober",
                    "November",
                    "Desember"
                ],
                monthNamesShort: [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "Mei",
                    "Jun",
                    "Jul",
                    "Agu",
                    "Sep",
                    "Okt",
                    "Nov",
                    "Des"
                ],
                "firstDay": 1
            });

            $('#smartwizard').smartWizard({
                selected: 0,
                keyNavigation: false,
                enableAllSteps: false,
                autoAdjustHeight: false,
                cycleSteps: false,
                backButtonSupport: false,
                toolbarSettings: {
                    showPreviousButton: true
                },
                useURLHash: true,
                transitionEffect: 'fade',
                transitionSpeed: '400',
                labelFinish: 'Submit',
                enableFinishButton: true
            });

            $('#smartwizard').on('leaveStep', function(e, anchorObject, stepNumber, stepDirection){
              
            })



            $('#gmp').sheepIt({
                separator: '',
                allowRemoveCurrent: true,
                allowAdd: true,
                minFormsCount: 1,
                iniFormsCount: 1
            });

            $('#lain').sheepIt({
                separator: '',
                allowRemoveCurrent: true,
                allowAdd: true,
                minFormsCount: 1,
                iniFormsCount: 1
            });

            $('#air').click(function() {
                if ($('#air').is(':checked')) {
                    $('#pj_air').val('{{ $air->name }}');
                    $('#dept_pj_air').val('{{ $air->departement }}');
                    $('#email_pj_air').val('{{ $air->email }}');
                    $('#telp_pj_air').val('{{ $air->telp }}');
                } else {
                    $('#pj_air').val('Penanggung Jawab Utility');
                    $('#dept_pj_air').val('dept');
                    $('#email_pj_air').val('Email Penanggung Jawab');
                    $('#telp_pj_air').val('Telp');
                }
            });

            $('#angin').click(function() {
                if ($('#angin').is(':checked')) {
                    $('#pj_angin').val('{{ $angin->name }}');
                    $('#dept_pj_angin').val('{{ $angin->departement }}');
                    $('#email_pj_angin').val('{{ $angin->email }}');
                    $('#telp_pj_angin').val('{{ $angin->telp }}');
                } else {
                    $('#pj_angin').val('Penanggung Jawab Utility');
                    $('#dept_pj_angin').val('dept');
                    $('#email_pj_angin').val('Email Penanggung Jawab');
                    $('#telp_pj_angin').val('Telp');
                }
            });

            $('#listrik').click(function() {
                if ($('#listrik').is(':checked')) {
                    $('#pj_listrik').val('{{ $listrik->name }}');
                    $('#dept_pj_listrik').val('{{ $listrik->departement }}');
                    $('#email_pj_listrik').val('{{ $listrik->email }}');
                    $('#telp_pj_listrik').val('{{ $listrik->telp }}');
                } else {
                    $('#pj_listrik').val('Penanggung Jawab Utility');
                    $('#dept_pj_listrik').val('dept');
                    $('#email_pj_listrik').val('Email Penanggung Jawab');
                    $('#telp_pj_listrik').val('Telp');
                }
            });

            $('#steam').click(function() {
                if ($('#steam').is(':checked')) {
                    $('#pj_steam').val('{{ $steam->name }}');
                    $('#dept_pj_steam').val('{{ $steam->departement }}');
                    $('#email_pj_steam').val('{{ $steam->email }}');
                    $('#telp_pj_steam').val('{{ $steam->telp }}');
                } else {
                    $('#pj_steam').val('Penanggung Jawab Utility');
                    $('#dept_pj_steam').val('dept');
                    $('#email_pj_steam').val('Email Penanggung Jawab');
                    $('#telp_pj_steam').val('Telp');
                }
            });

            $('#lainnya').click(function() {
                if ($('#lainnya').is(':checked')) {
                    $('#lain').css('visibility', 'visible');
                    $('#lain_text').val('');
                    $('#lain_text').focus();
                } else {
                    $('#lain').css('visibility', 'hidden');

                }
            });

            $('#check-ipb').change(function() {
                if ($('#api').is(':checked') || $('#ketinggian').is(':checked') || $('#ruang_terbatas').is(':checked')) {
                    $('#ipb').prop('checked', true);
                } else {
                    $('#ipb').prop('checked', false);
                }
            });

            $('#check-kimia').change(function() {
                if ($('#kimia').is(':checked')) {
                    $('#msds').prop('checked', true);
                } else {
                    $('#msds').prop('checked', false);
                }
            })

        });
    </script>

</body>

</html>
