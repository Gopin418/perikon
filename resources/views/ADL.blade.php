<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <meta http-equiv="refresh" content="2" > -->
    {{ Asset::add('bootstrap', '/css/bootstrap.css') }}
    {{ Asset::add('Material Design', '/css/mdb.min.css') }}
    {{ Asset::add('fontawesome', '/css/font-awesome.css') }}
    {{ Asset::add('master-css', '/css/master.css')}}
    {{ Asset::add('adl', '/css/adl.css') }}
    <title>HSE - Analisa Dampak Lingkungan</title>


</head>

<body>
    <div class="loading-page bg-success text-white" id="loading-page">
        <div class="fa-animation flex-center">
            <i class="fa fa-envira fa-lg fa-shake"></i>
        </div>
    </div>
<form action="/ADL" method="post">

    <div class="d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                            <div class="jsabox shadow">
                                    <div class="hijau"></div>
                                    <div class="card border border-white">
                                        <div class="card-header border border-white text-center bg-white">
                                            <h2 class="card-title">Analisa Dampak Lingkungan</h2>
                                        </div>
                                    <div class="card-body border border-white">
                                        <div id="ADL">
                                            <!-- Form utama ADL -->
                                            <div id="ADL_template">
                                                <table class="table table-borderless text-center col-sm-12 table-sm font-w">
                                                    <thead class="thead-light">
                                                        <tr>
                                                            <th class="font-weight-bold" width="74.99999997%">Aktivitas</th>
                                                            <th class="font-weight-bold" width="24.99999999%">Penanggung Jawab</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <div class="row">

                                                    <!-- Nama Aktivitas -->
                                                    <div class="col-sm-9 jau">
                                                        <input type="text" name="aktivitas[]" id="ADL_#index#_nama_aktivitas" class="form-control" placeholder="Perbaikan Jalan">
                                                    </div>
                                                    <!-- /Nama Aktivitas -->

                                                    <!-- Penanggung Jawab -->
                                                    <div class="col-sm-3 jau">
                                                        <input type="text" name="penanggung_jawab[]" id="ADL_#index#_penanggung_jawab" class="form-control" placeholder="John Doe">
                                                    </div>
                                                    <!-- /Penanggung Jawab -->
                                                </div>
                                                <table class="table table-borderless text-center col-sm-12 mt-3">
                                                    <thead class="thead-light">
                                                        <tr>
                                                            <th class="font-weight-bold" width="24.99999999%">Alat dan Bahan</th>
                                                            <th class="font-weight-bold" width="24.99999999%">Jenis Pencemaran</th>
                                                            <th class="font-weight-bold" width="24.99999999%">Potensi Pencemaran</th>
                                                            <th class="font-weight-bold" width="24.99999999%">Pengendalian</th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <div class="row mt-2">

                                                    <!-- Alat dan Bahan -->
                                                    <div class="col-sm-3">
                                                        <div id="ADL_#index#_ADB">

                                                            <!-- Template Alat dan Bahan (Nesting) -->
                                                            <div id="ADL_#index#_ADB_template">
                                                                <div class="input-group mb-2 jau">
                                                                    <input type="text" name="adb[#index#][]" id="ADL_#index#_ADB_#index_ADB#" class="form-control" placeholder="Kabel">
                                                                    <div class="input-group-append">
                                                                        <button class="btn btn-danger m-0 p-2 pl-3 pr-3 shadow-none" id="ADL_#index#_ADB_remove_current" type="button"><span class="fa fa-minus"></span></button>
                                                                        <button class="btn btn-success m-0 p-2 pl-3 pr-3 shadow-none" id="ADL_#index#_ADB_add" type="button"><span class="fa fa-plus"></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /Template Alat dan Bahan (Nesting) -->

                                                            <!-- No Form template -->
                                                            <div id="ADL_#index#_ADB_noforms_template">Tidak Ada Alat dan Bahan</div>
                                                            <!-- /No form template -->

                                                        </div>
                                                    </div>
                                                    <!-- /Alat dan Bahan -->

                                                    <div class="col-sm-3">
                                                        <div id="ADL_#index#_jenis_pencemaran">

                                                            <div id="ADL_#index#_jenis_pencemaran_template">
                                                                <div class="form-group row no-gutters ml-2 text-muted">
                                                                    <div class="jau col-lg-5 col-xl-4">
                                                                        <input type="checkbox" name="jenis_pencemaran[#index#][]" id="ADL_#index#_udara" value="Udara">
                                                                        <label for="ADL_#index#_udara">Udara</label>
                                                                    </div>
                                                                    <div class="jau col-lg-5 col-xl-4">
                                                                        <input type="checkbox" name="jenis_pencemaran[#index#][]" id="ADL_#index#_tanah" value="Tanah">
                                                                        <label for="ADL_#index#_tanah">Tanah</label>
                                                                    </div>
                                                                    <div class="jau col-lg-2 col-xl-4">
                                                                        <input type="checkbox" name="jenis_pencemaran[#index#][]" id="ADL_#index#_air" value="Air">
                                                                        <label for="ADL_#index#_air">Air</label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <!-- Potensi Pencemaran -->
                                                    <div class="col-sm-3">
                                                        <div id="ADL_#index#_potensi_pencemaran">

                                                            <!-- Template potensi pencemaran -->
                                                            <div id="ADL_#index#_potensi_pencemaran_template">
                                                                <div class="input-group mb-2 jau">
                                                                    <input type="text" name="potensi_pencemaran[#index#][]" id="ADL_#index#_potensi_pencemaran_#index_potensi_pencemaran#" class="form-control" placeholder="Serbuk Besi">
                                                                    <div class="input-group-append">
                                                                        <button class="btn btn-danger m-0 p-2 pl-3 pr-3 shadow-none" id="ADL_#index#_potensi_pencemaran_remove_current" type="button"><span class="fa fa-minus"></span></button>
                                                                        <button class="btn btn-success m-0 p-2 pl-3 pr-3 shadow-none" id="ADL_#index#_potensi_pencemaran_add" type="button"><span class="fa fa-plus"></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /Template potensi pencemaran -->

                                                            <!-- No form template -->
                                                            <div id="ADL_#index#_potensi_pencemaran_noforms_template">Tidak Ada Potensi Pencemaran</div>
                                                            <!-- /No form template -->

                                                        </div>
                                                    </div>
                                                    <!-- /Potensi Pencemaran -->

                                                    <!-- Pengendalian -->
                                                    <div class="col-sm-3">
                                                        <div id="ADL_#index#_pengendalian">

                                                            <!-- Template Pengendalian -->
                                                            <div id="ADL_#index#_pengendalian_template">
                                                                <div class="input-group mb-2 jau">
                                                                    <input type="text" name="pengendalian[#index#][]" id="ADL_#index#_pengendalian_#index_pengendalian#" class="form-control" placeholder="Membersihkan sisa Material">
                                                                    <div class="input-group-append">
                                                                        <button class="btn btn-danger m-0 p-2 pl-3 pr-3 shadow-none" id="ADL_#index#_pengendalian_remove_current" type="button"><span class="fa fa-minus"></span></button>
                                                                        <button class="btn btn-success m-0 p-2 pl-3 pr-3 shadow-none" id="ADL_#index#_pengendalian_add" type="button"><span class="fa fa-plus"></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /Template Pengendalian -->

                                                            <!-- pengendalian no template -->
                                                            <div id="ADL_#index#_pengendalian_noforms_template">Tidak Ada Pengendalian</div>
                                                            <!-- /pengendalian no template -->

                                                        </div>
                                                    </div>
                                                    <!-- /Pengendalian -->

                                                </div>


                                                <!-- Keterangan -->
                                                <div class="row mt-2">
                                                    <div class="col-sm-12 jau">
                                                        <input type="text" name="keterangan[]" id="ADL_#index#_keterangan" class="form-control" placeholder="Keterangan Aktivitas (Jika Ada)">
                                                    </div>
                                                </div>
                                                <!-- /Keterangan -->

                                                    <hr>



                                            </div>
                                            <!-- /Form Utama ADL -->

                                            <!-- No form template -->
                                            <div id="ADL_noforms_template">Tidak Ada Dampak Lingkungan</div>
                                            <!-- /No form template -->

                                            {{-- Footer Message --}}
                                            <div class="card border-success">
                                              <div class="card-body p-2 pl-1 pr-1 text-muted text-center">
                                                <small><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum melanjutkan&nbsp;&nbsp;&nbsp;<i class="fa fa-exclamation-triangle"></i></small>
                                              </div>
                                            </div>
                                            {{-- /Footer Message --}}

                                            {{-- Footer --}}

                                            <!-- /Controls -->

                                        </div>
                                    </div>
                                    <div class="card-footer bg-white border border-white">
                                            <div class="row">
                                                    <div class="col">
                                                        <hr>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <small>ADL</small>
                                                    </div>
                                                    <div class="col">
                                                        <hr>
                                                    </div>
                                                </div>
                                                {{-- /Footer --}}

                                                <!-- Controls -->
                                                <div class="row mt-2">
                                                    <div class="col-sm-1">
                                                        <button class="btn btn-success font-weight-bold" id="ADL_add">Add</button>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <button class="btn btn-danger font-weight-bold ml-3" id="ADL_remove_last">Remove</button>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    <button class="btn btn-indigo font-weight-bold float-right" type="submit">Submit</button>
                                                        {{ csrf_field() }}
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
            </div>

    </div>

<div class="d-block d-lg-none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                        <div class="bg-white shadow mt-5">
                                <div class="hijau"></div>
                                <div class="form">
                                    <h3>Analisa Dampak Lingkungan</h3>
                                    <small class="text-muted">*Silahkan isi Formulir ADL sebelum melanjutkan</small>
                                        <hr>
                                    <div id="ADL-mobile">
                                        <!-- Form utama ADL -->
                                        <div id="ADL-mobile_template">



                                                <!-- Nama Aktivitas -->
                                                <div class="form-group">
                                                    <input type="text" name="aktivitas[]" id="ADL-mobile_#index#_nama_aktivitas" class="form-control" placeholder="Nama Aktivitas">
                                                </div>
                                                <!-- /Nama Aktivitas -->





                                                <!-- Alat dan Bahan -->
                                                <div class="form-group">
                                                    <div id="ADL-mobile_#index#_ADB">

                                                        <!-- Template Alat dan Bahan (Nesting) -->
                                                        <div id="ADL-mobile_#index#_ADB_template">
                                                            <div class="input-group mb-2">
                                                                <input type="text" name="adb[#index#][]" id="AD-mobileL_#index#_ADB_#index_ADB#" class="form-control" placeholder="Alat dan Bahan">
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-danger m-0 p-2 pl-3 pr-3" id="ADL-mobile_#index#_ADB_remove_current" type="button"><span class="fa fa-minus"></span></button>
                                                                    <button class="btn btn-success m-0 p-2 pl-3 pr-3" id="ADL-mobile_#index#_ADB_add" type="button"><span class="fa fa-plus"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Template Alat dan Bahan (Nesting) -->

                                                        <!-- No Form template -->
                                                        <div id="ADL-mobile_#index#_ADB_noforms_template">Tidak Ada Alat dan Bahan</div>
                                                        <!-- /No form template -->

                                                    </div>
                                                </div>
                                                <!-- /Alat dan Bahan -->

                                                <div class="form-group text-muted">
                                                    <div id="ADL-mobile_#index#_jenis_pencemaran">
                                                        <label for="">Jenis Pencemaran</label>
                                                        <div id="ADL-mobile_#index#_jenis_pencemaran_template">
                                                                <div class="jau">
                                                                    <input type="checkbox" name="jenis_pencemaran[#index#][]" id="ADL-mobile_#index#_udara" value="Udara">
                                                                    <label for="ADL-mobile_#index#_udara">Udara</label>
                                                                </div>
                                                                <div class="jau">
                                                                    <input type="checkbox" name="jenis_pencemaran[#index#][]" id="ADL-mobile_#index#_tanah" value="Tanah">
                                                                    <label for="ADL-mobile_#index#_tanah">Tanah</label>
                                                                </div>
                                                                <div class="jau">
                                                                    <input type="checkbox" name="jenis_pencemaran[#index#][]" id="ADL-mobile_#index#_air" value="Air">
                                                                    <label for="ADL-mobile_#index#_air">Air</label>
                                                                </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <!-- Potensi Pencemaran -->
                                                <div class="form-group">
                                                    <div id="ADL-mobile_#index#_potensi_pencemaran">

                                                        <!-- Template potensi pencemaran -->
                                                        <div id="ADL-mobile_#index#_potensi_pencemaran_template">
                                                            <div class="input-group mb-2">
                                                                <input type="text" name="potensi_pencemaran[#index#][]" id="ADL-mobile_#index#_potensi_pencemaran_#index_potensi_pencemaran#" class="form-control" placeholder="Potensi Pencemaran">
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-danger m-0 p-2 pl-3 pr-3" id="ADL-mobile_#index#_potensi_pencemaran_remove_current" type="button"><span class="fa fa-minus"></span></button>
                                                                    <button class="btn btn-success m-0 p-2 pl-3 pr-3" id="ADL-mobile_#index#_potensi_pencemaran_add" type="button"><span class="fa fa-plus"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Template potensi pencemaran -->

                                                        <!-- No form template -->
                                                        <div id="ADL-mobile_#index#_potensi_pencemaran_noforms_template">Tidak Ada Potensi Pencemaran</div>
                                                        <!-- /No form template -->

                                                    </div>
                                                </div>
                                                <!-- /Potensi Pencemaran -->

                                                <!-- Pengendalian -->
                                                <div class="form-group">
                                                    <div id="ADL-mobile_#index#_pengendalian">

                                                        <!-- Template Pengendalian -->
                                                        <div id="ADL-mobile_#index#_pengendalian_template">
                                                            <div class="input-group mb-2">
                                                                <input type="text" name="pengendalian[#index#][]" id="ADL-mobile_#index#_pengendalian_#index_pengendalian#" class="form-control" placeholder="Pengendalian Pencemaran">
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-danger m-0 p-2 pl-3 pr-3" id="ADL-mobile_#index#_pengendalian_remove_current" type="button"><span class="fa fa-minus"></span></button>
                                                                    <button class="btn btn-success m-0 p-2 pl-3 pr-3" id="ADL-mobile_#index#_pengendalian_add" type="button"><span class="fa fa-plus"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Template Pengendalian -->

                                                        <!-- pengendalian no template -->
                                                        <div id="ADL-mobile_#index#_pengendalian_noforms_template">Tidak Ada Pengendalian</div>
                                                        <!-- /pengendalian no template -->

                                                    </div>
                                                </div>
                                                <!-- /Pengendalian -->



                                                <hr>


                                                <!-- Penanggung Jawab -->
                                                <div class="form-group">
                                                    <input type="text" name="penanggung_jawab[]" id="ADL-mobile_#index#_penanggung_jawab" class="form-control" placeholder="Penanggung Jawab">
                                                </div>
                                                <!-- /Penanggung Jawab -->

                                                <!-- Keterangan -->
                                                <div class="form-group">
                                                    <input type="text" name="keterangan[]" id="ADL-mobile_#index#_keterangan" class="form-control" placeholder="Keterangan Aktivitas (Jika Ada)">
                                                </div>
                                                <!-- /Keterangan -->

                                                <hr>



                                        </div>
                                        <!-- /Form Utama ADL -->

                                        <!-- No form template -->
                                        <div id="ADL-mobile_noforms_template">Tidak Ada Dampak Lingkungan</div>
                                        <!-- /No form template -->

                                        {{-- Footer Message --}}
                                        <div class="card border-success text-muted text-center mb-3">
                                          <div class="card-body p-2 pl-1 pl-1">
                                            <small><i class="fa fa-exclamation-triangle d-none d-md-inline"></i>&nbsp;&nbsp;&nbsp;Pastikan anda telah mengisi dengan benar sebelum menambah atau melanjutkan&nbsp;&nbsp;&nbsp;<i class="fa fa-exclamation-triangle d-none d-md-inline"></i></small>
                                          </div>
                                        </div>
                                        {{-- /Footer Message --}}

                                        {{-- Footer --}}
                                        <div class="row no-gutters text-muted">
                                            <div class="col">
                                                <hr>
                                            </div>
                                            <div class="col-2 text-center">
                                                <small>ADL</small>
                                            </div>
                                            <div class="col">
                                                <hr>
                                            </div>
                                        </div>
                                        {{-- /Footer --}}

                                        <!-- Controls -->
                                        <div class="row mt-2">
                                            <div class="col-5">
                                              <button class="btn btn-danger" id="ADL-mobile_remove_last"><span class="fa fa-minus"></span></button>
                                                <button class="btn btn-success pull-left" id="ADL-mobile_add"><span class="fa fa-plus"></span></button>
                                            </div>
                                            <div class="col">
                                            </div>
                                            <div class="col-4">
                                                <input type="hidden" name="status" value="Pending">
                                            <button class="btn btn-primary pull-right" type="submit"><span class="fa fa-check"></span></button>
                                                {{ csrf_field() }}
                                            </div>
                                        </div>
                                        <!-- /Controls -->

                                    </div>
                                        <hr>
                                </div>
                            </div>
                </div>
            </div>
        </div>

</div>

</form>

<script src="{{ asset('js/jquery.js') }}" charset="utf-8"></script>
<script src="{{ asset('js/mdb.min.js') }}"></script>
<script src="{{ asset('js/jquery.sheepItPlugin.js') }}" charset="utf-8"></script>
<script>
var id = 0;

var sheepItForm = {};

$(window).on('load', function() {
    pageLoading();
});

function pageLoading() {
    setTimeout(function() {
        $('.loading-page').fadeOut('slow');
    }, 500);
}

    $(document).ready(function () {

        var sheepItForm = $('#ADL').sheepIt({
            separator: '',
            allowRemoveCurrent: true,
            allowAdd: true,
            minFormsCount: 1,
            iniFormsCount: 1,

            nestedForms: [
                {
                    id: 'ADL_#index#_ADB',
                    options: {
                        separator: '',
                        indexFormat: '#index_ADB#',
                        allowRemoveCurrent: true,
                        allowAdd: true,
                        minFormsCount: 1,
                        iniFormsCount: 1
                    }
                },
                {
                    id: 'ADL_#index#_potensi_pencemaran',
                    options: {
                        separator: '',
                        indexFormat: '#index_potensi_pencemaran#',
                        allowRemoveCurrent: true,
                        allowAdd: true,
                        minFormsCount: 1,
                        iniFormsCount: 1
                    }
                },
                {
                    id: 'ADL_#index#_pengendalian',
                    options: {
                        separator: '',
                        indexFormat: '#index_pengendalian#',
                        allowRemoveCurrent: true,
                        allowAdd: true,
                        minFormsCount: 1,
                        iniFormsCount: 1
                    }
                }

            ]

        });

        var sheepItForm = $('#ADL-mobile').sheepIt({
            separator: '',
            allowRemoveCurrent: true,
            allowAdd: true,
            minFormsCount: 1,
            iniFormsCount: 1,

            nestedForms: [
                {
                    id: 'ADL-mobile_#index#_ADB',
                    options: {
                        separator: '',
                        indexFormat: '#index_ADB#',
                        allowRemoveCurrent: true,
                        allowAdd: true,
                        minFormsCount: 1,
                        iniFormsCount: 1
                    }
                },
                {
                    id: 'ADL-mobile_#index#_potensi_pencemaran',
                    options: {
                        separator: '',
                        indexFormat: '#index_potensi_pencemaran#',
                        allowRemoveCurrent: true,
                        allowAdd: true,
                        minFormsCount: 1,
                        iniFormsCount: 1
                    }
                },
                {
                    id: 'ADL-mobile_#index#_pengendalian',
                    options: {
                        separator: '',
                        indexFormat: '#index_pengendalian#',
                        allowRemoveCurrent: true,
                        allowAdd: true,
                        minFormsCount: 1,
                        iniFormsCount: 1
                    }
                }

            ]

        });

    });
</script>
</body>

</html>
