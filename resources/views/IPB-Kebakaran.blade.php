<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <meta http-equiv="refresh" content="2" > -->

    {{ Asset::add('bootstrap', '/css/bootstrap.css') }}
    {{ Asset::add('master-css', '/css/master.css') }}
    {{ Asset::add('ipb', '/css/ipb.css') }}
    {{ Asset::add('fontawesome', '/css/font-awesome.css') }}
    <title>HSE - Izin Pekerjaan Berbahaya</title>
    <style>
        .fa-lg{font-size:15em; top:8px; left:0px;}
</style>

</head>

<body>

    <div class="loading-page text-center bg-danger text-white" id="loading-page">
        <div class="fa-animation flex-center">
            <i class="fa fa-fire fa-lg fa-shake"></i>
        </div>
    </div>

    <div class="col-lg-12 ipb-back">
        <div class="container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div class="ipb-form shadow">
                        <div class="merah"></div>
                        <div class="card border border-white">
                            <div class="card-header bg-white text-center border border-white">
                                <h2 class="card-title">Izin Pekerjaan Berbahaya Kebakaran</h2>
                            </div>
                            <div class="card-body">
                                <form action="{{route('store-kebakaran')}}" method="post">

                                    <table class="table table-bordered table-striped table-sm mt-3">

                                        {{-- Header --}}
                                        <thead class="text-center align-middle">
                                            <tr>
                                                <th style="border-bottom:none" rowspan="2" width="50%">Persyaratan Pekerjaan</th>
                                                <th style="border:none" colspan="2" width="20%">Pemenuhan</th>
                                                <th style="border-bottom:none" rowspan="2" width="30%">Keterangan</th>
                                            </tr>
                                            <tr class="align-content-center">
                                                <th style="border-bottom:none">Ya</th>
                                                <th style="border-bottom:none">Tidak</th>
                                            </tr>
                                        </thead>
                                        {{-- /Header --}}

                                        {{-- Body --}}
                                        <tbody>
                                            <tr>
                                                <td colspan="4" class="font-weight-bold">
                                                    Umum
                                                </td>
                                            </tr>
                                            {{-- pemenuhan_1 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Tersedia alat pemadam api (APAR)?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_1" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_1" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_1" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_1 --}}
                                            {{-- Dua --}}
                                            <tr>
                                                <td style="vertical-align:middle">Tersedia alarm kebakaran?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_2" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_2" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_2" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /Dua --}}
                                            {{-- pemenuhan_3 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Peralatan yang dipakai dalam kondisi baik?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_3" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_3" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_3" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_3 --}}
                                            {{-- pemenuhan_4 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Rute evakuasi memadai bagi pekerja?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_4" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_4" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_4" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_4 --}}
                                            {{-- pemenuhan_5 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Perisa perlindungan memadai?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_5" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_5" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_5" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_5 --}}
                                            {{-- pemenuhan_6 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Memasang rambu?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_6" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_6" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_6" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_6 --}}
                                            {{-- pemenuhan_7 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Tidak berada pada area yang berdekatan dengan bahan mudah terbakar atau memicu ledakan?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_7" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_7" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_7" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_7 --}}
                                            {{-- pemenuhan_8 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Membersihkan area kerja sebelum pelaksanaan kegiatan (bersih dari debu, serat, minyak)?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_8" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_8" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_8" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_8 --}}
                                            {{-- pemenuhan_9 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Lantai mudah terbakar atau bereaksi terhadap api perlu dilapisi dengan bahan/terpal yang lebih tahan api?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_9" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_9" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_9" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_9 --}}
                                            {{-- pemenuhan_10 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Pastikan ruangan bersih dari uap yang mudah terbakar (bahan bakar/chemical)?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_10" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_10" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_10" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_10 --}}
                                            {{-- pemenuhan_11 --}}
                                            <tr>
                                                <td style="vertical-align:middle">Bejana tekan, perpipaan dan peralatan bebas dari perbaikan, isolasi dan angin?</td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_11" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_11" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_11" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_11 --}}
                                            <tr>
                                                <td colspan="4" class="font-weight-bold">
                                                    Pengawasan Kebakaran
                                                </td>
                                            </tr>
                                            {{-- pemenuhan_12 --}}
                                            <tr>
                                                <td style="vertical-align:middle"><i>Pelaksana atau pengawas kegiatan paham bagaimana cara menggunakan APAR?</i></td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_12" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_12" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_12" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_12 --}}
                                            {{-- pemenuhan_3belas --}}
                                            <tr>
                                                <td style="vertical-align:middle"><i>Melakukan pengecekan area kerja setelah pekerjaan selesai(mematikan alat yang dipakai dan sumber bahan bakar yang dipakai)?</i></td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_13" id="Ya" value="Ya" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle" class="text-center pt-2">
                                                    <div class="ipb-forms">
                                                        <label>
                                                            <input type="radio" name="pemenuhan_13" id="Tidak" value="Tidak" class="option-input radio">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <div class="mer">
                                                        <input type="text" name="keterangan_13" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- /pemenuhan_3belas --}}
                                        </tbody>
                                        {{-- /Body --}}

                                    </table>

                                    {{-- Footer Message --}}

                                    {{-- /Footer Message --}}

                                    {{-- Footer --}}

                                    {{-- /Footer --}}



                            </div>
                            <div class="card-footer bg-white border border-white">
                                <small>*Pastikan anda telah mengisi form dengan benar sebelum melanjutkan</small>
                                <div class="row">
                                    <div class="col">
                                        <hr>
                                    </div>
                                    <div class="col-lg-1"><small>IPB</small></div>
                                    <div class="col">
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input type="submit" value="Submit" class="btn btn-primary float-right">
                                        {{ csrf_field() }}
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </div>



            <script src="{{ asset('js/jquery.js') }}"></script>

            <script>
                $(window).on('load', function() {
                    pageLoading();
                });

                function pageLoading() {
                    setTimeout(function() {
                        $('.loading-page').fadeOut('slow');
                    }, 500);
                }
            </script>
</body>

</html>
