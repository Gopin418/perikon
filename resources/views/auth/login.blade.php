<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HSEAdmin | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/bootstrap.css">
  <link rel="stylesheet" href="/css/mdb.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
  html, body{
    height: 100%;
  }
  </style>
</head>
<body class="blue-gradient">
<div class="container pt-5 justify-content-center">
    <div class="row pt-5">
      <div class="col-4"></div>
      <div class="col-xl-4 pt-5">
          <div class="card">
              <div class="card-header bg-white text-center border border-0 mb-0 pb-0 pt-3">
                <h1 class="card-title h3 text-primary"><b>HSE</b>Admin</h1>
              </div>
              <!-- /.login-logo -->
              <div class="card-body pt-0">
            
                <form action="{{ route('login') }}" method="post">
                    {{ csrf_field() }}
                  <div class="md-form {{ $errors->has('email') ? ' has-error' : ' ' }}">
                    <input id="email" name="email" type="email" class="form-control" value="{{ old('email') }}" required autofocus>
                    <label for="email">Email</label>
                    @if ($errors->has('email'))
                        <span class="help-block text-danger">
                            <small>{{ $errors->first('email') }}</small>
                        </span>
                    @endif
                  </div>
                  <div class="md-form {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" name="password" type="password" class="form-control" required>
                    <label for="password">Password</label>
                    @if ($errors->has('password'))
                        <span class="help-block text-danger">
                            <small>{{ $errors->first('password') }}</small>
                        </span>
                    @endif
                  </div>
                      <div class="form-check pl-0 mb-4">
                          <input type="checkbox" class="form-check-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                          <label class="form-check-label card-text" for="remember">Remember Me</label>
                      </div>
                  <div class="row">
                    <!-- /.col -->
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-outline-primary btn-rounded waves-effect btn-block shadow-none">LogIn</button>
                      </div>
                  </div>
              </div>
              <!-- /.card-body -->
            </div>
      </div>
    </div>
</div>
<!-- /.card -->

<!-- jQuery 3 -->
<script src="{{ asset('/js/jquery.js') }}"></script>
<script src="{{ asset('/js/mdb.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
<script>
</script>
</body>
</html>
