<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <meta http-equiv="refresh" content="2" > -->

    {{ Asset::add('bootstrap', '/css/bootstrap.css') }}
    {{ Asset::add('master-css', '/css/master.css') }}
    {{ Asset::add('fontawesome', '/css/font-awesome.css') }}

    <title>HSE - Izin Pekerjaan Berbahaya</title>
<style>
.fa-lg{
    font-size: 15em;
    top: 8;
    left: 0;
}
</style>
</head>

<body class="ipb">
    <div class="loading-page bg-danger text-white" id="loading-page">
        <div class="fa-animation">
            <i class="fa fa-fire fa-lg fa-shake"></i>
        </div>
    </div>
    <div class="col-lg-12 ipb-back">
        <div class="container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div class="ipb-form shadow">
                        <div class="merah"></div>
                        <div class="card card-white border border-white">
                            <div class="card-header bg-white border border-white text-center">
                                <h2 class="card-title">Izin Pekerjaan Berbahaya Ruangan Terbatas</h2>
                            </div>

                            <form action="{{route('store-ruang-terbatas')}}" method="post">
                                <div class="card-body">
                                <table class="table-striped table-bordered table-sm mt-3">

                                    {{-- Header --}}
                                    <thead class="text-center align-middle">
                                        <tr>
                                            <th style="border-bottom:none" rowspan="2" width="50%">Persyaratan Pekerjaan</th>
                                            <th style="border:none" colspan="2" width="20%">Pemenuhan</th>
                                            <th style="border-bottom:none" rowspan="2" width="30%">Keterangan</th>
                                        </tr>
                                        <tr class="align-content-center">
                                            <th style="border-bottom:none">Ya</th>
                                            <th style="border-bottom:none">Tidak</th>
                                        </tr>
                                    </thead>
                                    {{-- /Header --}}

                                    {{-- Body --}}
                                    <tbody>
                                        {{-- pemenuhan_1 --}}
                                        <tr>
                                            <td>Apakah Anda Terlatih untuk menjalankan Pekerjaan ini?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_1" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_1" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_1" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_1 --}}
                                        {{-- Dua --}}
                                        <tr>
                                            <td>Ruangan telah diisolasi dari semua jaringan pipa?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_2" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_2" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_2" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /Dua --}}
                                        {{-- pemenuhan_3 --}}
                                        <tr>
                                            <td>Ruangan telah dibersihkan dengan uap/air/udara?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_3" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_3" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_3" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_3 --}}
                                        {{-- pemenuhan_4 --}}
                                        <tr>
                                            <td>Ruangan telah terkunci secara elektrik?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_4" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_4" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_4" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_4 --}}
                                        {{-- pemenuhan_5 --}}
                                        <tr>
                                            <td>Ruangan telah terkunci secara mekanik?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_5" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_5" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_5" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_5 --}}
                                        {{-- pemenuhan_6 --}}
                                        <tr>
                                            <td>Suhu ruangan berada dibawah 30°C pada pendinginan penuh?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_6" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_6" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_6" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_6 --}}
                                        {{-- pemenuhan_7 --}}
                                        <tr>
                                            <td>Apakah pintu masuk cukup besar untuk dilewati pada saat darurat?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_7" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_7" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_7" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_7 --}}
                                        {{-- pemenuhan_8 --}}
                                        <tr>
                                            <td>Apakah persediaan udara/ventilasi yang dibutuhkan terpenuhi?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_8" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_8" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_8" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_8 --}}
                                        {{-- pemenuhan_9 --}}
                                        <tr>
                                            <td>Apakah sarana akes masuk dan keluar dapat diterima?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_9" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_9" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_9" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_9 --}}
                                        {{-- pemenuhan_10 --}}
                                        <tr>
                                            <td>Apakah alat bantu bernafas dibawa dan dalam keadaan baik?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_10" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_10" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_10" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_10 --}}
                                        {{-- pemenuhan_11 --}}
                                        <tr>
                                            <td>Apakah tali/tripod/harness dan peralatan bantuan lainnya dibawa?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_11" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_11" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_11" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_11 --}}
                                        {{-- pemenuhan_12 --}}
                                        <tr>
                                            <td>Apakah ada pengaturan darurat yang memadai?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_12" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_12" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_12" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_12 --}}
                                        {{-- pemenuhan_3belas --}}
                                        <tr>
                                            <td>Apakah ada orang terlatih yang bersiaga di pintu masuk?</td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_13" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_13" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mer">
                                                    <input type="text" name="keterangan_13" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_3belas --}}
                                    </tbody>
                                    {{-- /Body --}}

                                </table>

                            </div>
                            <div class="card-footer bg-white border border-white">
                                    <small>*Pastikan anda telah mengisi form dengan benar sebelum melanjutkan</small>

                                {{-- Footer --}}
                                <div class="row">
                                    <div class="col"><hr></div>
                                    <div class="col-lg-1"><small>IPB</small></div>
                                    <div class="col"><hr></div>
                                </div>
                                {{-- /Footer --}}

                                <div class="row">
                                    <div class="col-lg-12">

                                        <input type="submit" value="Submit" class="btn btn-primary float-right">
                                        {{ csrf_field() }}
                                    </div>
                                </div>
                            </div>
                            </form>


                        </div>
                    </div>
                <div>
            </div>
        </div>
    </div>


<script src="{{ asset('js/jquery.js') }}"></script>
<script>
$(window).on('load', function(){
    pageLoading();
});

function pageLoading() {
    setTimeout(function() {
        $('.loading-page').fadeOut('slow');
    }, 500);
}
</script>

</body>

</html>
