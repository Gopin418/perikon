<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <meta http-equiv="refresh" content="2" > -->

    {{ Asset::add('bootstrap', '/css/bootstrap.css') }}
    {{ Asset::add('master-css', '/css/master.css') }}
    {{ Asset::add('fontawesome', '/css/font-awesome.css') }}

    <title>HSE - Izin Pekerjaan Berbahaya</title>
<style>
.fa-lg{
    font-size: 15em;
    top: 8;
    left: 0;
}
</style>
</head>

<body class="ipb">
    <div class="loading-page bg-danger text-white" id="loading-page">
        <div class="fa-animation">
            <i class="fa fa-fire fa-lg fa-shake"></i>
        </div>
    </div>
    <div class="col-lg-12 ipb-back">
        <div class="container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div class="ipb-form shadow">
                        <div class="merah"></div>
                        <div class="card border border-white">
                            <div class="card-header text-center bg-white border border-white">
                                <h2 class="card-title">Izin Pekerjaan Berbahaya Ketinggian</h2>
                            </div>
                            <form action="{{route('store-ketinggian')}}" method="post">
                            <div class="card-body">

                                <table class="table table-striped table-bordered table-sm mt-3">

                                    {{-- Header --}}
                                    <thead class="text-center align-middle">
                                        <tr>
                                            <th style="border-bottom:none" rowspan="2" width="50%">Persyaratan Pekerjaan</th>
                                            <th style="border:none" colspan="2" width="20%">Pemenuhan</th>
                                            <th style="border-bottom:none" rowspan="2" width="30%">Keterangan</th>
                                        </tr>
                                        <tr class="align-content-center">
                                            <th style="border-bottom:none">Ya</th>
                                            <th style="border-bottom:none">Tidak</th>
                                        </tr>
                                    </thead>
                                    {{-- /Header --}}

                                    {{-- Body --}}
                                    <tbody>
                                        {{-- pemenuhan_1 --}}
                                        <tr>
                                            <td style="vertical-align:middle">Apakah tersedia APD yang memadai (full body hardness, helm, sepatu)?</td>
                                            <td style="vertical-align:middle" class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_1" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align:middle" class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_1" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align:middle">
                                                <div class="mer">
                                                    <input type="text" name="keterangan_1" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_1 --}}
                                        {{-- Dua --}}
                                        <tr>
                                            <td style="vertical-align:middle">Apakah tersedia peralatan yang memadai (tangga, platform)?</td>
                                            <td style="vertical-align:middle" class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_2" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align:middle" class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_2" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align:middle">
                                                <div class="mer">
                                                    <input type="text" name="keterangan_2" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /Dua --}}
                                        {{-- pemenuhan_3 --}}
                                        <tr>
                                            <td style="vertical-align:middle">Apakah dilakukan pengecekan terhadap kondisi tangga?</td>
                                            <td style="vertical-align:middle" class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_3" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align:middle" class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_3" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align:middle">
                                                <div class="mer">
                                                    <input type="text" name="keterangan_3" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- /pemenuhan_3 --}}
                                        {{-- pemenuhan_4 --}}
                                        <tr>
                                            <td style="vertical-align:middle">Apakah scaffolding/perancah diperiksa kondisinya (pada saat awal, pada paling tidak 7 hari sekali, dan pada kondisi darurat?</td>
                                            <td style="vertical-align:middle" class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label>
                                                        <input type="radio" name="pemenuhan_4" id="Ya" value="Ya" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align:middle" class="text-center pt-2">
                                                <div class="ipb-forms">
                                                    <label >
                                                        <input type="radio" name="pemenuhan_4" id="Tidak" value="Tidak" class="option-input radio">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align:middle">
                                                <div class="mer">
                                                    <input type="text" name="keterangan_4" id="keterangan" class="form-control" placeholder="(Jika Ada)">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    {{-- /Body --}}

                                </table>

                                

                            </div>
                            <div class="card-footer bg-white border border-white">
                                {{-- Footer Message --}}
                                    <small>*Pastikan anda telah mengisi form dengan benar sebelum melanjutkan</small>
                                {{-- /Footer Message --}}

                                {{-- Footer --}}
                                <div class="row">
                                    <div class="col"><hr></div>
                                    <div class="col-lg-1"><small>IPB</small></div>
                                    <div class="col"><hr></div>
                                </div>
                                {{-- /Footer --}}

                                <div class="row">
                                    <div class="col-lg-12">

                                        <input type="submit" value="Submit" class="btn btn-primary float-right">
                                        {{ csrf_field() }}
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                <div>
            </div>
        </div>
    </div>

<script src="{{ asset('js/jquery.js') }}"></script>
<script>
$(window).on('load', function() {
    pageLoading();
});

function pageLoading() {
    setTimeout(function() {
        $('.loading-page').fadeOut('slow');
    }, 500);
}

</script>
</body>

</html>
