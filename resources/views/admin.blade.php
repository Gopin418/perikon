<!DOCTYPE html>
<html>
<head>
  {{ Asset::add('fontawesome', 'css/font-awesome.css') }}
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HSE Kontraktor | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  {{-- CSS --}}
  {{ Asset::add('bootstrap-4', 'css/bootstrap.css') }}
  {{ Asset::add('select2', 'css/select2.min.css') }}
  {{ Asset::add('theme', 'css/adminlte.min.css') }}
  {{ Asset::add('skin', 'css/_all-skins.min.css') }}
  {{-- {{ Asset::add('chat', 'css/chat.css') }} --}}
  {{ Asset::add('data', 'css/data.css') }}
  {{ Asset::add('datepicker', 'css/datepicker3.css') }}
  {{ Asset::add('daterangepicker', 'css/daterangepicker-bs3.css') }}
  {{ Asset::add('scrollbar', 'css/jquery.mCustomScrollbar.css') }}
  @yield('css')
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition sidebar-mini sidebar-collapse">

<div class="wrapper" style="overflow:hidden">
{{-- <div class="loading-page @yield('loading-skin') " id="loading-page">
  <div class="fa-animation">
    <i class="fa fa-lg  @yield('loading-style')"></i>
  </div>
</div> --}}

    @include('backend.header')

  <!-- Left side column. contains the logo and sidebar -->

    <!-- sidebar: syle can be found in sidebar.less -->
    @include('backend.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@yield('content-header')</h1>
          </div>
          <div class="col-sm-6" style="font-size : 0.9em;">
            <ol class="breadcrumb float-sm-right">
              <li><a href="{{ route('dashboard') }}" class="text-dark"><i class="fa fa-dashboard"></i></a></li>
              @yield('breadcrumb')
            </ol>
          </div>
        </div>
      </div>
    </div>

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        @yield('content')

        @include('backend.notif')
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<script src="{{ asset('js/jquery.js')}}" charset="utf-8"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('js/daterangepicker.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js')}}" charset="utf-8"></script>
<script src="{{ asset('js/pusher.min.js')}}" charset="utf-8"></script>
<script src="{{ asset('js/bootstrap.bundle.js')}}" charset="utf-8"></script>
<script src="{{ asset('js/fastclick.js')}}" charset="utf-8"></script>
<script src="{{ asset('js/jquery.dataTables.min.js')}}" charset="utf-8"></script>
<script src="{{ asset('js/select2.full.min.js')}}" charset="utf-8"></script>
<script src="{{ asset('js/jquery.mCustomScrollbar.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('js/Chart.min.js') }}"></script>
<script>

window.addEventListener('load', function() {
  new FastClick(document.body);
}, false);

$(window).on('load', function() {
  $('.scroll').mCustomScrollbar({
    mouseWheel: {
      scrollAmount: 200
    }
  });
  urlCheck();
});

function urlCheck() {
  if(window.location.href.indexOf('IKO') > -1){
    loadingPage();
  } else if(window.location.href.indexOf('JSA') > -1){
    loadingPage();
  } else if(window.location.href.indexOf('ADL') > -1){
    loadingPage();
  } else if(window.location.href.indexOf('IPB') > -1){
    loadingPage();
  } else if(window.location.href.indexOf('utility') > -1){
    loadingPage();
  } else if(window.location.href.indexOf('list') > -1){
    loadingPage();
  } else if(window.location.href.indexOf('role') > -1){
    loadingPage();
  } else if(window.location.href.indexOf('add') > -1){
    loadingPage();
  } else if(window.location.href.indexOf('profile') > -1){
    loadingPage();
  } else {
    dataLoad();
  }
}

function loadingPage(){
  setTimeout(function() {
    $('#loading-page').fadeOut('slow');
  }, 500);
}

function IKObadgeCheck(){
  if($('li#iko_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.iko-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.iko-num').show();
    }, 400)
  }
  if(!$('li#iko_sidebar').hasClass('menu-open')) {
    $('.iko-badge').removeClass('badge bg-warning').addClass('fa fa-angle-left');
    $('.iko-num').hide();
  }
  if($('li#jsa_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.jsa-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.jsa-num').show();
    }, 400);
  }
  if($('li#adl_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.adl-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.adl-num').show();
    }, 400);
  }
  if($('li#ipb_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.ipb-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.ipb-num').show();
    }, 400);
  }
}
function JSAbadgeCheck(){
  if($('li#jsa_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.jsa-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.jsa-num').show();
    }, 400);
  }
  if(!$('li#jsa_sidebar').hasClass('menu-open')){
    $('.jsa-badge').removeClass('badge bg-warning').addClass('fa fa-angle-left');
    $('.jsa-num').hide();
  }
  if($('li#iko_sidebar').hasClass('menu-open')){
      setTimeout(function(){
        $('.iko-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
        $('.iko-num').show();
      },400);
    }
  if($('li#adl_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.adl-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.adl-num').show();
    }, 400);
  }
  if($('li#ipb_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.ipb-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.ipb-num').show();
    }, 400);
  }
}
function ADLbadgeCheck(){
  if($('li#adl_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.adl-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.adl-num').show();
    }, 400);
  }
  if(!$('li#adl_sidebar').hasClass('menu-open')){
    $('.adl-badge').removeClass('badge bg-warning').addClass('fa fa-angle-left');
    $('.adl-num').hide();
  }
  if($('li#iko_sidebar').hasClass('menu-open')){
      setTimeout(function(){
        $('.iko-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
        $('.iko-num').show();
      },400);
    }
    if($('li#jsa_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.jsa-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.jsa-num').show();
    }, 400);
  }
  if($('li#ipb_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.ipb-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.ipb-num').show();
    }, 400);
  }
}
function IPBbadgeCheck(){
  if($('li#ipb_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.ipb-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.ipb-num').show();
    }, 400);
  }
  if(!$('li#ipb_sidebar').hasClass('menu-open')){
    $('.ipb-badge').removeClass('badge bg-warning').addClass('fa fa-angle-left');
    $('.ipb-num').hide();
  }
  if($('li#adl_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.adl-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.adl-num').show();
    }, 400);
  }
  if($('li#iko_sidebar').hasClass('menu-open')){
      setTimeout(function(){
        $('.iko-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
        $('.iko-num').show();
      },400);
    }
    if($('li#jsa_sidebar').hasClass('menu-open')){
    setTimeout(function(){
      $('.jsa-badge').removeClass('fa fa-angle-left').addClass('badge bg-warning');
      $('.jsa-num').show();
    }, 400);
  }
}


$(document).ready(function() {

  $('li#iko_sidebar').on('click', function(){
    IKObadgeCheck();
  });

  $('li#jsa_sidebar').on('click', function(){
    JSAbadgeCheck();
  });

  $('li#adl_sidebar').on('click', function(){
    ADLbadgeCheck();
  })

  $('li#ipb_sidebar').on('click', function() {
    IPBbadgeCheck();
  })

  var url = window.location;
// for sidebar menu but not for treeview submenu
$('ul.nav-sidebar a').filter(function() {
    return this.href == url;
}).parent().siblings().removeClass('active').end().addClass('active');
// for treeview which is like a submenu
$('ul.has-treeview a').filter(function() {
    return this.href == url;
}).parentsUntil(".nav-sidebar > .has-treeview").siblings().removeClass('active').end().addClass('active');
    });


</script>

@yield('script')
</body>
</html>
