<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Perikon - Verify</title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/mdb.min.css">
    <link rel="stylesheet" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/master.css">
    <link rel="stylesheet" href="/css/datepicker.css">
     <link href="https://fonts.googleapis.com/css?family=Roboto:100,300" rel="stylesheet">
    <?php
  $bg = array('bg-01.jpg', 'bg-02.jpg', 'bg-03.jpg', 'bg-04.jpg', 'bg-05.jpg', 'bg-06.jpg', 'bg-07.jpg' ); // array of filenames

  $i = rand(0, count($bg)-1); // generate random number size of the array
  $selectedBg = "$bg[$i]"; // set variable equal to which random filename was chosen
?>
<style type="text/css">
body{
background: url(/images/<?php echo $selectedBg; ?>) no-repeat;
background-size: cover;
background-position: center bottom;
}
</style>
  </head>
  <body>
      <div class="mask flex-center rgba-black-strong">
          <div class="container text-white">
            <div class="row">
              <div class="col">

              </div>
              <div class="col-12 col-sm-10 col-md-6 col-lg-4">
                <form action="{{ url('/contractor'. '/' . $id . '/' . 'verif' . '/'. $code_id) }}" method="post">
                  {{ csrf_field() }}
                  <div class="md-form">
                    <input type="text" name="code" id="code" class="form-control form-control-lg text-white text-center" placeholder="Verification Code" autocomplete="off" required>
                  </div>

                  <button type="submit" id="submit" class="btn btn-primary waves-effect btn-block" name="button">Submit</button>
                </form>
              </div>
              <div class="col">

              </div>
            </div>
          </div>
      </div>
  </body>
  <script src="{{ asset('js/jquery.js') }}" charset="utf-8"></script>
  <script src="{{ asset('/js/jquery-ui.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/mdb.min.js') }}" charset="utf-8"></script>
  <script type="text/javascript">

  </script>
</html>
