<table>
  <thead>
    <tr>
      <th>NO JSA</th>
      <th>Aktivitas</th>
      <th>Potensi Bahaya</th>
      <th>Pengendalian Bahaya</th>
      <th>Penanggung Jawab</th>
      <th>Keterangan</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($JSA as $key => $value)
      <?php
        $aktivitas = explode(', ', $value->aktivitas);
        $aktivitasLength = count($aktivitas)+1;
        $penanggungJawab = explode(', ', $value->penanggung_jawab);
        $keterangan = explode(', ', $value->keterangan);
       ?>
      <tr>
        <td rowspan="{{{ $aktivitasLength }}}">{{{ $value->no_jsa }}}</td>
        {{-- <td>{{ $value->aktivitas }}</td> --}}
        @foreach (explode(', ', $value->aktivitas) as $key1 => $aktivitas)
            <td>{{{ $aktivitas }}}</td>
            <td>
              <table>
              @foreach (explode(', ', $value->potensi_bahaya[$key1]) as $key2 => $potensi)
                <tr>
                  <td>{{{ $potensi }}}</td>
                </tr>
              @endforeach
            </table>
            </td>
            <td>
              <table>
                @foreach (explode(', ', $value->pengendalian_bahaya[$key1]) as $key2 => $pengendalian)
                  <tr>
                    <td>{{{ $pengendalian }}}</td>
                  </tr>
                @endforeach
              </table>
            </td>
            <td>{{{ $penanggungJawab[$key1] }}}</td>
            <td>{{{ $keterangan[$key1] }}}</td>
        @endforeach
      </tr>
    @endforeach
  </tbody>
</table>
