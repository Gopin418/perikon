<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Perikon</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mdb.min.css') }}">
    <link rel="stylesheet" href="/css/master.css">
     <link href="https://fonts.googleapis.com/css?family=Roboto:100,300" rel="stylesheet">
  </head>
  <body>

    <div class="flex-center">
      <div class="container text-center">
        <div class="row">
          <div class="col">
            <h1>Terimakasih telah mengisi Perikon</h1>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <h3>Kami telah mengirim email ke {{ $email->email }}</h3>
            <small>
              Tidak menerima email? <a href="{{ route('finish') }}">kirim ulang</a>
            </small>
          </div>
        </div>
      </div>
    </div>


    <script src="{{ asset('js/jquery.js') }}" charset="utf-8"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" charset="utf-8"></script>
    <script src="{{ asset('js/mdb.min.js') }}" charset="utf-8"></script>
  </body>
</html>
