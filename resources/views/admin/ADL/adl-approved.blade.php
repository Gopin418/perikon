@extends('admin.template.approved')
@section('loading-skin')
    bg-green
@endsection
@section('loading-style')
    fa-envira fa-shake
@endsection
@section('content-header')
Analisa Dampak Lingkungan
@endsection

@section('jenis')
<a href="{{ route('adl-approved') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;ADL&nbsp;&nbsp;<i class="fa fa-caret-right">&nbsp;&nbsp;</i></a>
@endsection

@section('approved-datatable')
@if( $ADL_approved == 0 )
<div class="card-body">
    <p>Tidak ada Data ADL Tersedia</p>
</div>
@else
  <table class="table table-bordered table-striped table-hover table-sm">
    <tr>
      <th class="align-middle">No ADL</th>
      <th class="align-middle">Nama Kontraktor</th>
      <th class="align-middle">Nama Pekerjaan</th>
      <th class="align-middle">Jenis Pekerjaan</th>
      <th class="align-middle">Lokasi</th>
      <th class="align-middle">Tanggal Mulai</th>
      <th class="align-middle">Tanggal Selesai</th>
      <th class="align-middle text-center">Action</th>
  </tr>
@endif
@foreach( $adl as $dataIKO )
  <tr>
      <td class="align-middle">{{ $dataIKO->no_adl }}</td>
      <td class="align-middle">{{ $dataIKO->nama_kontraktor }}</td>
      <td class="align-middle">{{ $dataIKO->nama_pekerjaan }}</td>
      <td class="align-middle">{{ $dataIKO->jenis }}</td>
      <td class="align-middle">{{ 'Nutrifood ' . $dataIKO->lokasi . ', ' . $dataIKO->area }}</td>
      <td class="align-middle">{{ $dataIKO->tgl_mulai }}</td>
      <td class="align-middle">{{ $dataIKO->tgl_selesai }}</td>
      <td class="align-middle text-center">
          <button type="button" class="mb-1 pl-2 pr-2 btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default{{$dataIKO->id}}">Detail</button>
      </td>
  </tr>
@endforeach
</table>
    @foreach ($adl as $key => $dataIKO)
      {{-- Modal --}}
      <div class="modal fade centered-modal" role="dialog" id="modal-default{{$dataIKO->id}}">
              <div class="modal-dialog modal-xxl">
                  <div class="modal-content">
                    <div class="modal-header bg-success">
                      <h4 class="modal-title">Detail ADL {{ $dataIKO->nama_kontraktor }}</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                      <div class="modal-body">
                          <div class="row">
                              <div class="col-4" style="border-right: 1px solid #ddd">


                                  <div class="card no-border text-muted scroll" style="height: 500px; font-size: 1.2em; font-style: 'Verdana'; card-shadow: none;">
                                      <div class="card-body p-0">
                                          <table class="table table-striped no-border">
                                            <tr>
                                              <td>Nama Kontraktor</td>
                                              <td>:</td>
                                              <td class="text-right">{{ $dataIKO->nama_kontraktor }}</td>
                                            </tr>
                                            <tr>
                                              <td>Nama Pekerjaan</td>
                                              <td>:</td>
                                              <td class="text-right">{{ $dataIKO->nama_pekerjaan }}</td>
                                            </tr>
                                            <tr>
                                              <td>Jenis Pekerjaan</td>
                                              <td>:</td>
                                              <td class="text-right">{{ $dataIKO->jenis }}</td>
                                            </tr>
                                            <tr>
                                              <td>Lokasi Pekerjaan</td>
                                              <td>:</td>
                                              <td class="text-right">{{ 'Nutrifood ' . $dataIKO->lokasi }}</td>
                                            </tr>
                                            <tr>
                                              <td>Area Pekerjaan</td>
                                              <td>:</td>
                                              <td class="text-right">{{ $dataIKO->area }}</td>
                                            </tr>
                                            <tr>
                                              <td>Tanggal Mulai</td>
                                              <td>:</td>
                                              <td class="text-right">{{ $dataIKO->tgl_mulai }}</td>
                                            </tr>
                                            <tr>
                                              <td>Tanggal Selesai</td>
                                              <td>:</td>
                                              <td class="text-right">{{ $dataIKO->tgl_selesai }}</td>
                                            </tr>
                                            <tr>
                                              <td>Deskripsi Pekerjaan</td>
                                              <td>:</td>
                                              <td></td>
                                            </tr>
                                            <tr>
                                              <td colspan="3" class="bg-white">
                                                <p class="text-muted">{{ $dataIKO->deskripsi }}</p>
                                              </td>
                                            </tr>
                                          </table>
                                      </div>
                                  </div>


                              </div>

                              <div class="col-8">


                                      <div class="card card-success scroll" style="height: 500px;font-style: 'Verdana'; card-shadow: none;">
                                          <div class="card-header bg-green text-center"><span class="text-white"><b>DETAILS</b></span></div>
                                          <div class="card-body p-0">
                                            <table class="table table-striped table-bordered table-condensed">
                                              <tr>
                                                <th class="align-middle">Nama Aktivitas</th>
                                                <th class="align-middle">Alat dan Bahan</th>
                                                <th class="align-middle">Potensi Pencemaran</th>
                                                <th class="align-middle">Jenis Pencemaran</th>
                                                <th class="align-middle">Pengendalian</th>
                                                <th class="align-middle">Penanggung Jawab</th>
                                                <th class="align-middle">Keterangan</th>
                                              </tr>
                                                <?php
                                                  $dataADL = $adl[$key];
                                                  $adb = explode('","', $dataADL->adb);
                                                  $potensi_pencemaran = explode('","', $dataADL->potensi_pencemaran);
                                                  $jenis_pencemaran = explode('","', $dataADL->jenis_pencemaran);
                                                  $pengendalian = explode('","', $dataADL->pengendalian);
                                                  $penanggung_jawab = explode(',', $dataADL->penanggung_jawab);
                                                  $keterangan = explode('",', $dataADL->keterangan);
                                                 ?>
                                                @foreach (explode(',', $dataADL->aktivitas) as $key => $aktivitas)
                                                  <tr>
                                                    <td class="align-middle">
                                                      {{ str_replace('"', '', $aktivitas) }}
                                                    </td>
                                                    <td class="align-middle">
                                                      <ul class="inside no-padding">
                                                        @foreach (explode(', ', $adb[$key]) as $key1 => $value)
                                                          <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $value) }}</li>
                                                        @endforeach
                                                      </ul>
                                                    </td>
                                                    <td class="align-middle">
                                                      <ul class="inside no-padding">
                                                        @foreach (explode(', ', $potensi_pencemaran[$key]) as $key2 => $value)
                                                          <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $value) }}</li>
                                                        @endforeach
                                                      </ul>
                                                    </td>
                                                    <td class="align-middle">
                                                      <ul class="inside no-padding">
                                                        @foreach (explode(', ', $jenis_pencemaran[$key]) as $key3 => $value)
                                                            <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $value) }}</li>
                                                        @endforeach
                                                      </ul>
                                                    </td>
                                                    <td class="align-middle">
                                                      <ul class="inside no-padding">
                                                        @foreach (explode(', ', $pengendalian[$key]) as $key4 => $value)
                                                          <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $value) }}</li>
                                                        @endforeach
                                                      </ul>
                                                    </td>
                                                    <td class="align-middle">
                                                      {{ preg_replace('/[^a-zA-Z 0-9]+/', '', $penanggung_jawab[$key]) }}
                                                    </td>
                                                    <td class="align-middle">
                                                      {{ preg_replace('/[^a-zA-Z 0-9]+/', '', $keterangan[$key]) }}
                                                    </td>
                                                  </tr>
                                                @endforeach
                                              </table>
                                              </div>
                                      </div>


                                  </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

    @endforeach
@endsection
