@extends('admin.template.approved')
@section('skin')
skin-red
@endsection
@section('loading-skin')
    bg-danger
@endsection
@section('loading-style')
    fa-fire fa-shake
@endsection
@section('content-header')
Izin Pekerjaan Berbahaya
@endsection

@section('jenis')
  <a href="{{ route('ipb-approved') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;IPB&nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>&nbsp;&nbsp;
@endsection


@section('approved-datatable')
    @if( $IPB_approved == 0 )
    <div class="card-body">
        <p>Tidak ada data IPB yang tersedia</p>
    </div>
    @else
    <table class="table table-bordered table-striped table-hover">
        <tr>
            <th>No IPB</th>
            <th>Nama Kontraktor</th>
            <th>Nama Pekerjaan</th>
            <th>Jenis Pekerjaan</th>
            <th>Lokasi Pekerjaan</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Jenis Pekerjaan Berbahaya</th>
            <th>Action</th>
        </tr>
    @foreach ($ipbs as $key => $ipb)
        <tr>
            <td style="vertical-align:middle">{{ $ipb->no_ipb }}</td>
            <td style="vertical-align:middle">{{ $ipb->nama_kontraktor }}</td>
            <td style="vertical-align:middle">{{ $ipb->nama_pekerjaan }}</td>
            <td style="vertical-align:middle">{{ $ipb->jenis }}</td>
            <td style="vertical-align:middle">{{ 'Nutrifood ' . $ipb->lokasi . ', ' . $ipb->area }}</td>
            <td style="vertical-align:middle">{{ $ipb->tgl_mulai }}</td>
            <td style="vertical-align:middle">{{ $ipb->tgl_selesai }}</td>
            <td style="vertical-align:middle">
                <ul class="inside no-padding">
                @foreach ( explode(', ', $ipb->pekerjaan_berbahaya) as $jenis)
                    <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $jenis) }}</li>
                @endforeach
                </ul>
            </td>
            <td style="vertical-align:middle">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default{{$ipb->id}}" style="margin-bottom:5px">Detail</button>
            </td>
        </tr>
    @endforeach
    </table>
    @foreach ($ipbs as $key => $ipb)
    <div class="modal fade" role="dialog" id="modal-default{{$ipb->id}}">
            <div class="modal-dialog modal-xxl">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Detail IPB {{ $ipb->nama_kontraktor }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5" style="border-right: 1px solid #ddd">
                                <div class="box no-border text-muted scroll" style="height: 460px;font-size: 1.2em; font-style: 'Verdana'; box-shadow: none;">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-12">
                                                Nama Kontraktor : <p class="pull-right">{{ $ipb->nama_kontraktor }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Nama Pekerjaan : <p class="pull-right">{{ $ipb->nama_pekerjaan }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Jenis Pekerjaan : <p class="pull-right">{{ $ipb->jenis }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Lokasi Pekerjaan : <p class="pull-right">{{ 'Nutrifood ' . $ipb->lokasi }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                            Area Pekerjaan : <p class="pull-right">{{ $ipb->area }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Tanggal Mulai : <p class="pull-right">{{ $ipb->tgl_mulai }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Tanggal Selesai : <p class="pull-right">{{ $ipb->tgl_selesai }}</p>
                                            </div>
                                        </div><br>
                                        Deskripsi Pekerjaan
                                            <br>
                                        <p>{{ $ipb->deskripsi }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7" style="border-right: 1px solid #ddd">
                                    <div class="box no-border" style="font-style: 'Verdana'; box-shadow: none;">
                                        <div class="box-body no-padding scroll" style="height: 500px;">
                                            @if ($ipb->kebakaran == 'true')
                                                @include('admin.IPB.detail.kebakaran')
                                            @else
                                                <div class="card border mt-5">
                                                    <div class="card-heading bg-danger p-3">
                                                        <h4 class="card-title">
                                                            Kebakaran
                                                        </h4>
                                                    </div>
                                                  <div class="card-body">
                                                        Pekerjaan tidak membutuhkan IPB Kebakaran
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($ipb->ketinggian == 'true')
                                                @include('admin.IPB.detail.ketinggian')
                                            @else
                                                <div class="card border mt-5">
                                                    <div class="card-heading bg-danger">
                                                        <h4 class="card-title">
                                                            Kebakaran
                                                        </h4>
                                                    </div>
                                                    <div class="card-body">
                                                        Pekerjaan tidak membutuhkan IPB Ketinggian
                                                    </div>
                                                </div>
                                            @endif
                                            @if($ipb->ruang_terbatas == 'true')
                                                @include('admin.IPB.detail.ruang-terbatas')
                                            @else
                                                <div class="card border mt-5">
                                                    <div class="card-heading bg-danger p-3">
                                                        <h4 class="card-title">
                                                            Kebakaran
                                                        </h4>
                                                    </div>
                                                    <div class="card-body">
                                                        Pekerjaan tidak membutuhkan IPB Ruang Terbatas
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
@endsection
