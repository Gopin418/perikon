<div class="card border">
  <div class="card-heading p-3 bg-danger">
    <h4 class="card-title">
      Kebakaran
    </h4>
  </div>
  <div class="card-body no-padding scroll" style="height: 290px">
      <table class="table table-bordered table-condensed">
          <tr>
            <th class="col-xs-5 text-center" rowspan="2" style="vertical-align:middle">Persyaratan</th>
            <th class="col-xs-2 text-center" colspan="2">Pemenuhan</th>
            <th class="col-xs-5 text-center" rowspan="2" style="vertical-align:middle">Keterangan</th>
          </tr>
          <tr>
            <th class="col-xs-1 text-center">Ya</th>
            <th class="col-xs-1 text-center">Tidak</th>
          </tr>

          <tr>
            <th colspan="4">Umum</th>
          </tr>

          {{-- Poin 1 --}}
          <tr>
            <td>
              Tersedia alat pemadam api (APAR) ?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($kebakaran[$key]->pemenuhan_1 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($kebakaran[$key]->pemenuhan_1 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $kebakaran[$key]->keterangan_1 }}
            </td>
          </tr>

          {{-- Poin 2 --}}
          <tr>
            <td>
              Tersedia alarm kebakaran ?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($kebakaran[$key]->pemenuhan_2 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($kebakaran[$key]->pemenuhan_2 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $kebakaran[$key]->keterangan_2 }}
            </td>
          </tr>

          {{-- Poin 3 --}}
          <tr>
            <td>
              Peralatan yang dipakai dalam kondisi baik ?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($kebakaran[$key]->pemenuhan_3 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($kebakaran[$key]->pemenuhan_3 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $kebakaran[$key]->keterangan_3 }}
            </td>
          </tr>

            {{-- Poin 4 --}}
            <tr>
              <td>
                Rute evakuasi memadai bagi pekerja ?
              </td>
              <td class="text-center" style="vertical-align:middle">
                @if ($kebakaran[$key]->pemenuhan_4 == 'Ya')
                  <i class="fa fa-check"></i>
                @endif
              </td>
              <td class="text-center" style="vertical-align:middle">
                @if ($kebakaran[$key]->pemenuhan_4 == 'Tidak')
                  <i class="fa fa-check"></i>
                @endif
              </td>
              <td style="vertical-align:middle">
                {{ $kebakaran[$key]->keterangan_4 }}
              </td>
            </tr>

              {{-- Poin 5 --}}
              <tr>
                <td>
                  Perisai perlindungan memadai ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_5 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_5 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_5 }}
                </td>
              </tr>

              {{-- Poin 6 --}}
              <tr>
                <td>
                  Memasang rambu ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_6 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_6 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_6 }}
                </td>
              </tr>

              {{-- Poin 7 --}}
              <tr>
                <td class="text-center" style="vertical-align:middle">
                  Tidak berada pada area yang berdekatan dengan
                  bahan mudah terbakar atau memicu ledakan ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_7 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_7 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_7 }}
                </td>
              </tr>

              {{-- Poin 8 --}}
              <tr>
                <td>
                  Membersihkan area kerja sebelum pelaksanaan
                  kegiatan (bersih dari debu, serat, minyak) ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_8 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_8 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_8 }}
                </td>
              </tr>

              {{-- Poin 9 --}}
              <tr>
                <td>
                  Lantai mudah terbakar atau bereaksi terhadap api
                  perlu dilapisi dengan bahan/terpal yang lebih tahan api ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_9 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_9 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_9 }}
                </td>
              </tr>

              {{-- Poin 10 --}}
              <tr>
                <td>
                  Pastikan ruangan bersih dari uap yang muddah terbakar
                  (bahan bakar/chemical) ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_10 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_10 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_10 }}
                </td>
              </tr>

              {{-- Poin 11 --}}
              <tr>
                <td>
                  Bejana tekan, perpipaan dan peralatan bebas dari perbaikan,
                   isolasi, dan angin ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_11 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_11 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_11 }}
                </td>
              </tr>

              <tr>
                <th colspan="4">Pengawasan Kebakaran</th>
              </tr>

              {{-- Poin 12 --}}
              <tr>
                <td>
                  <i>
                    Pelaksana atau pengawas kegiatan paham
                  bagaimana cara penggunaan APAR ?
                  </i>
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_12 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_12 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_12 }}
                </td>
              </tr>

              {{-- Poin 13 --}}
              <tr>
                <td>
                  <i>
                    Melakukan pengecekan area kerja
                    setelah pekerjaan selesai (mematikan alat yang
                     dipakai dan sumber bahan bakar yang dipakai) ?
                  </i>
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_13 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($kebakaran[$key]->pemenuhan_13 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $kebakaran[$key]->keterangan_13 }}
                </td>
              </tr>

        </table>
  </div>
</div>
