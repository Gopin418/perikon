<div class="card border mt-5">
  <div class="card-heading bg-danger p-3">
    <h4 class="card-title">
      Ruang Terbatas
    </h4>
  </div>
  <div class="card-body no-padding scroll" style="height: 290px">
      <table class="table table-bordered table-condensed">
          <tr>
            <th class="col-lg-5 text-center" rowspan="2" style="vertical-align:middle">Persyaratan</th>
            <th class="col-lg-2 text-center" colspan="2">Pemenuhan</th>
            <th class="col-lg-5 text-center" rowspan="2" style="vertical-align:middle">Keterangan</th>
          </tr>
          <tr>
            <th class="col-lg-1 text-center">Ya</th>
            <th class="col-lg-1 text-center">Tidak</th>
          </tr>

          {{-- Poin 1 --}}
          <tr>
            <td>
              Apakah anda terlatih untuk menjalankan pekerjaan ini ?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ruang_terbatas[$key]->pemenuhan_1 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ruang_terbatas[$key]->pemenuhan_1 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $ruang_terbatas[$key]->keterangan_1 }}
            </td>
          </tr>

          {{-- Poin 2 --}}
          <tr>
            <td>
              Ruangan telah diisolasi dari semua jaringan pipa ?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ruang_terbatas[$key]->pemenuhan_2 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ruang_terbatas[$key]->pemenuhan_2 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $ruang_terbatas[$key]->keterangan_2 }}
            </td>
          </tr>

          {{-- Poin 3 --}}
          <tr>
            <td>
              Ruangan telah dibersihkan dengan uap/air/udara ?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ruang_terbatas[$key]->pemenuhan_3 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ruang_terbatas[$key]->pemenuhan_3 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $ruang_terbatas[$key]->keterangan_3 }}
            </td>
          </tr>

            {{-- Poin 4 --}}
            <tr>
              <td>
                Ruangan telah terkunci secara elektrik ?
              </td>
              <td class="text-center" style="vertical-align:middle">
                @if ($ruang_terbatas[$key]->pemenuhan_4 == 'Ya')
                  <i class="fa fa-check"></i>
                @endif
              </td>
              <td class="text-center" style="vertical-align:middle">
                @if ($ruang_terbatas[$key]->pemenuhan_4 == 'Tidak')
                  <i class="fa fa-check"></i>
                @endif
              </td>
              <td style="vertical-align:middle">
                {{ $ruang_terbatas[$key]->keterangan_4 }}
              </td>
            </tr>

              {{-- Poin 5 --}}
              <tr>
                <td>
                  Ruangan telah terkunci secara mekanik
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_5 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_5 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_5 }}
                </td>
              </tr>

              {{-- Poin 6 --}}
              <tr>
                <td>
                  Suhu ruangan berada dibawah 30°C pada pendinginan penuh ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_6 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_6 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_6 }}
                </td>
              </tr>

              {{-- Poin 7 --}}
              <tr>
                <td>
                  Apakah pintu masuk cukup besar untuk dilewati pada saat darurat ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_7 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_7 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_7 }}
                </td>
              </tr>

              {{-- Poin 8 --}}
              <tr>
                <td>
                  Apakah persediaan udara/ventilasi yang dibutuhkan terpenuhi ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_8 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_8 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_8 }}
                </td>
              </tr>

              {{-- Poin 9 --}}
              <tr>
                <td>
                  Apakah sarana akses masuk dan keluar dapat diterima ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_9 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_9 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_9 }}
                </td>
              </tr>

              {{-- Poin 10 --}}
              <tr>
                <td>
                  Apakah alat bantu bernafas dibawa dan dalam keadaan baik ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_10 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_10 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_10 }}
                </td>
              </tr>

              {{-- Poin 11 --}}
              <tr>
                <td>
                  Apakah tali/tripod/harness dan peralatan bantuan lainnya dibawa ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_11 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_11 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_11 }}
                </td>
              </tr>


              {{-- Poin 12 --}}
              <tr>
                <td>
                  Apakah ada pengaturan darurat yang memadai ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_12 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_12 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_12 }}
                </td>
              </tr>

              {{-- Poin 13 --}}
              <tr>
                <td>
                  Apakah ada orang terlatih yang bersiaga di pintu masuk ?
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_13 == 'Ya')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td class="text-center" style="vertical-align:middle">
                  @if ($ruang_terbatas[$key]->pemenuhan_13 == 'Tidak')
                    <i class="fa fa-check"></i>
                  @endif
                </td>
                <td style="vertical-align:middle">
                  {{ $ruang_terbatas[$key]->keterangan_13 }}
                </td>
              </tr>

        </table>
  </div>
</div>
