<div class="card border mt-5">
  <div class="card-heading bg-danger p-3">
    <h4 class="card-title">
      Ketinggian
    </h4>
  </div>
  <div class="card-body no-padding scroll" style="height: 290px;">
      <table class="table table-bordered table-condensed">
          <tr>
            <th class="col-xs-5 text-center" rowspan="2" style="vertical-align:middle">Persyaratan</th>
            <th class="col-xs-2 text-center" colspan="2">Pemenuhan</th>
            <th class="col-xs-5 text-center" rowspan="2" style="vertical-align:middle">Keterangan</th>
          </tr>
          <tr>
            <th class="col-xs-1 text-center">Ya</th>
            <th class="col-xs-1 text-center">Tidak</th>
          </tr>

          {{-- Poin 1 --}}
          <tr>
            <td>
              Apakah tersedia APD yang memadai (full body hardness, helm, sepatu)?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ketinggian[$key]->pemenuhan_1 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ketinggian[$key]->pemenuhan_1 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $ketinggian[$key]->keterangan_1 }}
            </td>
          </tr>

          {{-- Poin 2 --}}
          <tr>
            <td>
              Apakah tersedia peralatan yang memadai (tangga, platform) ?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ketinggian[$key]->pemenuhan_2 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ketinggian[$key]->pemenuhan_2 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $ketinggian[$key]->keterangan_2 }}
            </td>
          </tr>

          {{-- Poin 3 --}}
          <tr>
            <td>
              Apakah dilakukan pengecekan terhadap kondisi tangga ?
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ketinggian[$key]->pemenuhan_3 == 'Ya')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td class="text-center" style="vertical-align:middle">
              @if ($ketinggian[$key]->pemenuhan_3 == 'Tidak')
                <i class="fa fa-check"></i>
              @endif
            </td>
            <td style="vertical-align:middle">
              {{ $ketinggian[$key]->keterangan_3 }}
            </td>
          </tr>

            {{-- Poin 4 --}}
            <tr>
              <td>
                Apakah scaffolding/perancah diperiksa kondisinya
                (pada saat awal, pada paling tidak 7 hari sekali, dan pada kondisi darurat) ?
              </td>
              <td class="text-center" style="vertical-align:middle">
                @if ($ketinggian[$key]->pemenuhan_4 == 'Ya')
                  <i class="fa fa-check"></i>
                @endif
              </td>
              <td class="text-center" style="vertical-align:middle">
                @if ($ketinggian[$key]->pemenuhan_4 == 'Tidak')
                  <i class="fa fa-check"></i>
                @endif
              </td>
              <td style="vertical-align:middle">
                {{ $ketinggian[$key]->keterangan_4 }}
              </td>
            </tr>

        </table>
  </div>
</div>
