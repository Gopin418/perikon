@extends('admin.template.pending')
@section('skin')
  skin-red
@endsection
@section('loading-skin')
    bg-red
@endsection
@section('loading-style')
    fa-fire fa-shake
@endsection
@section('content-header')
Izin Pekerjaan Berbahaya
@endsection

@section('jenis')
<a href="{{ route('ipb-approved') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;IPB</a>&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;
@endsection

@section('class-jenis-pen')
  card-danger
@endsection

@section('pending-datatable')
  @if( $IPB_pending == 0 )
  <div class="card-body">
      <p>Tidak ada IPB yang sedang menunggu persetujuan</p>
  </div>
  @else

<table class="table table-bordered table-striped table-hoveered">
  <tr>
    <th>No IPB</th>
    <th>Nama Kontraktor</th>
    <th>Nama Pekerjaan</th>
    <th>Jenis Pekerjaan</th>
    <th>Lokasi Pekerjaan</th>
    <th>Tanggal Mulai</th>
    <th>Tanggal Selesai</th>
    <th>Jenis Pekerjaan Berbahaya</th>
    <th>Action</th>
  </tr>
  @endif
  @foreach ($ipb as $key => $dataIPB)
    <form action="{{ route('ipb-approve', $dataIPB->id) }}" method="post">
      {{ csrf_field() }}
    <tr>
      <td style="vertical-align: middle">{{ $dataIPB->no_ipb }}</td>
      <td style="vertical-align: middle">{{ $dataIPB->nama_kontraktor }}</td>
      <td style="vertical-align: middle">{{ $dataIPB->nama_pekerjaan }}</td>
      <td style="vertical-align: middle">{{ $dataIPB->jenis }}</td>
      <td style="vertical-align: middle">{{ 'Nutrifood ' . $dataIPB->lokasi . ', ' . $dataIPB->area }}</td>
      <td style="vertical-align: middle">{{ $dataIPB->tgl_mulai }}</td>
      <td style="vertical-align: middle">{{ $dataIPB->tgl_selesai }}</td>
      <td style="vertical-align: middle">
        <ul class="inside no-padding">
          @foreach ( explode(', ', $dataIPB->pekerjaan_berbahaya) as $jenis)
            <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $jenis) }}</li>
          @endforeach
        </ul>
      </td>
      <td style="vertical-align: middle">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default{{$dataIPB->id}}" style="margin-bottom:5px">Detail</button>
        <input type="hidden" name="_method" value="PUT">
        <input type="submit" name="submit" value="Approve" class="btn btn-success btn-xs">
      </td>
    </tr>
  @endforeach
  </table>
  @foreach ($ipb as $key => $ipb)

    <div class="modal fade" role="dialog" id="modal-default{{$ipb->id}}">
            <div class="modal-dialog modal-xxl">
                <div class="modal-content">
                  <div class="modal-header bg-danger">
                    <h4 class="modal-title">Detail IPB {{ $ipb->nama_kontraktor }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5" style="border-right: 1px solid #ddd">


                                <div class="card border-0 text-muted scroll" style="height: 440px;font-size: 1.2em; font-style: 'Verdana'; card-shadow: none;">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                Nama Kontraktor : <p class="pull-right">{{ $ipb->nama_kontraktor }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Nama Pekerjaan : <p class="pull-right">{{ $ipb->nama_pekerjaan }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Jenis Pekerjaan : <p class="pull-right">{{ $ipb->jenis }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Lokasi Pekerjaan : <p class="pull-right">{{ 'Nutrifood ' . $ipb->lokasi }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                          <div class="col-12">
                                            Area Pekerjaan : <p class="pull-right">{{ $ipb->area }}</p>
                                          </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Tanggal Mulai : <p class="pull-right">{{ $ipb->tgl_mulai }}</p>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-12">
                                                Tanggal Selesai : <p class="pull-right">{{ $ipb->tgl_selesai }}</p>
                                            </div>
                                        </div><br>
                                        Deskripsi Pekerjaan
                                            <br>
                                        <p>{{ $ipb->deskripsi }}</p>
                                    </div>
                                </div>


                            </div>

                            <div class="col-7" style="border-right: 1px solid #ddd">


                                    <div class="card border-0" style="font-style: 'Verdana'; card-shadow: none;">
                                        <div class="card-body no-padding scroll" style="height: 440px;">

                                            @if ($ipb->kebakaran == 'true')
                                                @include('admin.IPB.detail.kebakaran')
                                            @else
                                                <div class="card border ">
                                                    <div class="card-heading bg-danger p-3">
                                                        <h4 class="card-title">
                                                            Kebakaran
                                                        </h4>
                                                    </div>
                                                    <div class="card-body">
                                                        Pekerjaan ini tidak membutuhkan IPB Kebakaran
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($ipb->ketinggian == 'true')
                                                @include('admin.IPB.detail.ketinggian')
                                            @else
                                                <div class="card border mt-5">
                                                    <div class="card-heading bg-danger p-3">
                                                        <h4 class="card-title">
                                                            Ketinggian
                                                        </h4>
                                                    </div>
                                                    <div class="card-body">
                                                        Pekerjaan ini tidak membutuhkan IPB Ketinggian
                                                    </div>
                                                </div>
                                            @endif
                                            @if($ipb->ruang_terbatas == 'true')
                                                @include('admin.IPB.detail.ruang-terbatas')
                                            @else
                                                <div class="card border mt-5">
                                                    <div class="card-heading bg-danger p-3">
                                                        <h4 class="card-title">
                                                            Ruang Terbatas
                                                        </h4>
                                                    </div>
                                                    <div class="card-body">
                                                        Pekerjaan ini tidak membutuhkan IPB Ruang Terbatas
                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                    </div>


                                </div>
                        </div>
                    </div>
                    @if(Auth::user()->hasRole('Admin Utama') || Auth::user()->hasRole('Admin Developer'))
                    <div class="modal-footer">
                        <input type="submit" value="Approve" class="btn btn-success">
                    </div>
                    @elseif(Auth::user()->hasRole("Admin Biasa"))
                    @endif
                </div>
            </div>
        </div>
    <input type="hidden" name="_method" value="PUT">
  </form>
  @endforeach
@endsection
