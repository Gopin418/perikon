@extends('admin')
@section('skin')
  skin-blue
@endsection
@section('loading-skin')
    bg-blue
@endsection
@section('loading-style')
    fa-briefcase fa-shake
@endsection
@section('content-header')
User Management
@endsection

@section('content-header-description')
{{ Auth::user()->name }}
@endsection

@section('breadcrumb')
<li><a href="{{ route('user-list') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;User Management&nbsp;&nbsp;<i class="fa fa-caret-right">&nbsp;&nbsp;</i></a></li>
<li class="active">Roles</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-solid card-info no-border elevation-3">
            <div class="card-header">
                <div class="card-title">User List</div>
            </div>

            <div class="card-body p-0">
                <table class="table table-borderless table-hover table-striped">
                    <tr>
                        <th>Id Role</th>
                        <th>Nama Role</th>
                        <th>Deskripsi Role</th>
                    </tr>
                    @foreach( $list as $lists )
                    <tr>
                        <td>{{ $lists->id }}</td>
                        <td>{{ $lists->role_name }}</td>
                        <td>{{ $lists->description }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection