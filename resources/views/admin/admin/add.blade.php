@extends('admin')
@section('loading-style')
    fa-user-plus fa-shake
@endsection
@section('content-header')
User Management
@endsection

@section('content-header-description')
{{ Auth::user()->name }}
@endsection

@section('breadcrumb')
<li><a href="{{ route('user-list') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;User Management&nbsp;&nbsp;<i class="fa fa-caret-right">&nbsp;&nbsp;</i></a></li>
<li class="active">Add User</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-info card-solid p-0 elevation-3">
            <div class="card-header">
                <h3 class="card-title">Add User</h3>
            </div>
            <form action="{{ route('store-user') }}" method="POST">

            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="card card-default p-0 m-0 no-shadow no-border">
                            <div class="card-body">
                              <label for="nama">Nama User</label>
                                <div class="row">
                                  <div class="col-6">
                                    <div class="form-group">
                                        <input type="text" name="firstname" id="nama" class="form-control" placeholder="Alvin" required>
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="form-group">
                                        <input type="text" name="lastname" id="nama" class="form-control" placeholder="Ardiansyah" required>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email User</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="example@mail.com" required>
                                </div>
                                <div class="form-group">
                                  <label for="username">Username</label>
                                  <input type="text" name="username" class="form-control" id="username" placeholder="username" required>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="text" name="password" id="password" class="form-control" placeholder="Must Contain 'HSE' " required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card card-default p-0 no-shadow no-border" style="card-shadow: none;">
                            <div class="card-body">
                                <div class="row">
                                  <div class="col-4">
                                    <div class="form-group">
                                      <label for="company">Company</label>
                                      <input type="text" class="form-control" name="company" id="company" placeholder="Company" value="Nutrifood" disabled style="cursor:default">
                                    </div>
                                  </div>
                                  <div class="col-4">
                                      <label for="sub">Sub</label>
                                      <select class="form-control select2" name="sub">
                                        <option value="Ciawi">Ciawi</option>
                                        <option value="Cibitung">Cibitung</option>
                                      </select>
                                  </div>
                                  <div class="col-4">
                                    <div class="form-group">
                                      <label for="departement">Departement</label>
                                      <select class="form-control select2" name="departement">
                                        @foreach($departement as $dept)
                                          <option value="{{ $dept->departement }}">{{ $dept->departement }}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <select class="form-control select2" name="role">
                                        <option value="Admin Level 2">Admin Biasa</option>
                                        <option value="Admin Level 1">Admin Utama</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <input type="submit" value="Save" class="btn btn-success pull-right col-1">
                {{ csrf_field() }}
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {
  $('.select2').select2({
    minimumResultsForSearch: -1,
  });
})
</script>
@endsection