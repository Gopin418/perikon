@extends('admin')
@section('loading-style')
    fa-users fa-shake
@endsection
@section('content-header')
User Management
@endsection


@section('content-header-description')
{{ Auth::user()->name }}
@endsection

@section('breadcrumb')
    @if(Auth::user()->hasRole('Admin Developer') || Auth::user()->hasRole('Admin Utama'))
        <li><a href="{{ route('user-list') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;User Management&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;</a></li>
        <li><a href="#" class="text-dark active">Users</a></li>
    @elseif(Auth::user()->hasRole('Admin Biasa'))
        <li>Users</li>
    @endif
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-info no-border card-solid elevation-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h4 class="card-title">User List</h4>
                    </div>
                    <div class="col-6">
                    @if(Auth::user()->hasRole('Admin Developer') || Auth::user()->hasRole('Admin Utama'))
                        <a href="{{ route('add-user') }}"><button class="btn btn-success bg-green pull-right btn-sm border border-white">Add User</button></a>
                    @elseif(Auth::user()->hasRole('Admin Biasa'))

                    @endif
                    </div>
                </div>
            </div>

            <div class="card-body p-0">
                <table class="table table-borderless table-hover table-striped">
                    <tr>
                        <th>Nama User</th>
                        <th>Email User</th>
                        <th>Role User</th>
                        <th>Deskripsi Role</th>
                        <th class="text-center">Action</th>
                    </tr>
                    @foreach( $list as $lists )

                    <tr>
                        <td>{{ $lists->firstname . ' ' . $lists->lastname }}</td>
                        <td>{{ $lists->email }}</td>
                        <td>{{ $lists->role_name }}</td>
                        <td>{{ $lists->description }}</td>
                        <td class="text-center">
                            <form action="{{ route('user-delete', $lists->id) }}" method="post">
                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#detail{{ $lists->id }}">Detail</button>
                                @if ( Auth::user()->hasRole('Admin Developer') || Auth::user()->hasRole('Admin Utama'))
                                <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#edit{{ $lists->id }}">Edit</button>
                                <input type="submit" value="Delete" class="btn btn-danger btn-xs">
                                @elseif(Auth::user()->hasRole('Admin Biasa'))

                                @endif
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                            </form>
                        </td>
                    </tr>
    {{-- Modal --}}
    <div class="modal fade" role="dialog" id="edit{{$lists->id}}">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Admin {{ $lists->firstname . ' ' . $lists->lastname }}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('user-edit', $lists->id) }}" method="post">
                      <label for="name">Nama Admin</label>
                        <div class="row">
                          <div class="col-6">
                            <div class="form-group">
                                <input type="text" name="firstname" id="name" class="form-control" placeholder="Alvin" value="{{ $lists->firstname }}" required>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                                <input type="text" name="lastname" id="name" class="form-control" placeholder="Ardiansyah" value="{{ $lists->lastname }}" required>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email Admin</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="example@mail.com" value="{{ $lists->email }}" required>
                        </div>
                        <div class="form-group">
                            <label for="password">New Password</label>
                            <input type="text" name="password" id="password" class="form-control" placeholder="must contain 'hse'" required>
                        </div>
                        <div class="form-group">
                            <label for="roles">Admin Role</label>
                            <br>
                            <select class="form-control select2" name="roles">
                                <option value="2">Admin Biasa</option>
                                <option value="1">Admin Utama</option>
                            </select>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary col-4 pull-right" value="Save">
                </div>
                <input type="hidden" name="_method" value="PUT">
                {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
    {{-- Modal --}}
    <div class="modal fade" role="dialog" id="detail{{$lists->id}}">
            <div class="modal-dialog modal-lg h-100 d-flex flex-column justify-content-center my-0">
                <div class="modal-content" style="background: url('{{url('images/'.$lists->bg)}}') center center; background-size: cover;">
                    <div class="modal-header no-border">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:#fff">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-0">
                      <div class="row ml-0 mr-0 mb-5" style="background-color: rgba(255,255,255,0.9); min-height: 250px;">
                        <div class="col-3" >
                          <img src="{{url('images/'.$lists->image)}}" alt="User Avatar" class="img-circle" style="object-fit:cover;margin-left:15%;margin-top: 8%;height:220px; width:220px;">
                        </div>
                        <div class="col-1">
                        </div>
                        <div class="col-8 p-4">
                          <h3>{{ $lists->firstname . ' ' . $lists->lastname }}</h3>
                          <div class="text-muted">
                            <h5>{{ $lists->email }}</h5>
                            <h5>{{ $lists->phone }}</h5>
                            <h5>{{ $lists->company . ' ' . $lists->sub . ' @ ' . $lists->departement}}</h5>
                            @if($lists->bio == null)

                            @else
                            <blockquote class="no-border">
                              <i>{{ '" ' . $lists->bio .' "' }}</i>
                              <footer>{{ $lists->firstname . ' ' . $lists->lastname }}</footer>
                            </blockquote>
                          @endif
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
