    <div class="card-body p-0">
        <table class="table table-condensed">
            @foreach ($ongoing as $key=> $item)
                <tr>
                    <td class="align-middle">{{$item->nama_kontraktor}}</td>
                    <td class="align-middle">{{$item->nama_pekerjaan}}</td>
                    <td class="align-middle">{{$item->jenis}}</td>
                    <td class="align-middle" width="40%">
                        <div class="progress">
                            <div class="progress-bar
                            @if($item->jenis == 'Project')
                                bg-blue
                            @elseif($item->jenis == 'Maintenance')
                                bg-yellow
                            @endif" style="width: {{$projectRange[$key]}}%"></div>
                        </div>
                    </td>
                    <td class="align-middle">
                        <div class="dropdown">
                            <button class="btn bg-transparent" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <div class="dropdown-item">Start&nbsp;:&nbsp;{{ $item->tgl_mulai }}</div>
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-item">End&nbsp;&nbsp;&nbsp;:&nbsp;{{ $item->tgl_selesai }}</div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
