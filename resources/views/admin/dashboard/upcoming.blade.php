@if ($upcomingData == 0)
<div class="card-body">
    Tidak Ada Data
</div>
@else
    <div class="card-body table-responsive p-0">
        <table class="table table-striped table-sm" id="upcomingTable">
            <tr>
                <th>Nama Kontraktor</th>
                <th>Nama Pekerjaan</th>
                <th>Jenis Pekerjaan</th>
                <th class="text-center">Status</th>
                <th class="text-center">Permission Required</th>
            </tr>
            @foreach ($upcoming as $item)
            <tr>
                <td class="align-middle">{{ $item->nama_kontraktor }}</td>
                <td class="align-middle">{{ $item->nama_pekerjaan }}</td>
                <td class="align-middle">{{ $item->jenis }}</td>
                <td class="align-middle text-center">
                    <span class="badge
                        @if($item->status == 'Pending')
                            badge-warning
                        @elseif($item->status == 'Upcoming')
                            badge-info
                        @elseif($item->status == 'Ongoing')
                            badge-primary
                        @elseif($item->status == 'Finished')
                            badge-success
                        @endif
                    ">{{ $item->status }}</span>
                </td>
                <td class="align-middle text-center">
                @if ($item->iko_status == 'Pending')
                    <small class="badge bg-warning">IKO</small>
                @elseif ($item ->iko_status == 'Approved')
                    <small class="badge bg-success">IKO</small>
                @endif
                @if ($item->jsa_status == 'Pending')
                    <small class="badge bg-warning">JSA</small>
                @elseif ($item ->jsa_status == 'Approved')
                    <small class="badge bg-success">JSA</small>
                @endif
                @if ($item->adl_status == 'Pending')
                    <small class="badge bg-warning">ADL</small>
                @elseif ($item ->adl_status == 'Approved')
                    <small class="badge bg-success">ADL</small>
                @endif
                @if (empty($item->ipb_status))

                @else
                @if ($item->ipb_status == 'Pending')
                    <small class="badge bg-warning">IPB</small>
                @elseif ($item ->ipb_status == 'Approved')
                    <small class="badge bg-success">IPB</small>
                @endif
                @endif
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@endif
    <div class="card-footer pull-right">
        {{ $upcoming->links() }}
    </div>
