@extends('admin')

@section('skin')
  skin-purple
@endsection

@section('loading-skin')
  bg-purple
@endsection

@section('loading-style')
    fa-user fa-shake
@endsection

@section('breadcrumb')
<li><a href="#" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;Admin&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;</a></li>
<li><a href="{{ route('profile') }}" class="text-dark active">Profile</a></li>
@endsection

@section('content-header')
  User profile
@endsection

@section('content')

  <div class="row">

    <div class="col-4">
      <div class="card card-widget widget-user elevation-3">
        <div class="widget-user-header bg-black" style="background: url('{{url('images/'.$profile->bg)}}') center center;background-size: cover;">
        </div>
        <div class="widget-user-image cover">
          <img class="img-circle" src="{{url('images/'.$profile->image)}}" alt="User Avatar" style="object-fit:cover;">
        </div>
        <div class="card-body text-center">
          <br>
          <h3>{{Auth::user()->firstname . '  ' . Auth::user()->lastname}}</h3>
          <p class="text-muted">{{Auth::user()->email}}</p>
          <p class="text-muted">{{ $profile->phone }}</p>
          <i class="fa fa-map-marker"></i>&nbsp;&nbsp;<span class="text-muted">{{ $profile->company.', '.$profile->sub}}</span>
          <br>
          <i class="fa fa-building"></i>&nbsp;&nbsp;<span class="text-muted">{{ $profile->departement }}</span>
          <blockquote class="text-muted no-border">
            <i>" {{ $profile->bio }} "</i>

            <footer>{{Auth::user()->name}}</footer>
          </blockquote>
        </div>
      </div>
    </div>

    <div class="col-8">

      <div class="row text-center">

        <div class="col-6">
          <form action="{{ route('profile-picture') }}" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <div class="card no-border elevation-3">
              <div class="card-header no-border">
                <h2 class="card-title">Profile Image</h2>
              </div>
              <div class="card-body">

                    <img src="{{url('images/'.$profile->image)}}" alt="User Avatar" height="128">
                    <div class="input-group mt-5">
                      <div class="custom-file">
                        <input type="file" name="image" id="image" class="form-control">
                      </div>
                      <div class="input-group-append">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="submit" value="Upload" class="btn btn-info">
                      </div>
                    </div>

                </div>

              </div>
            </form>

        </div>
        <div class="col-6">
          <form class="" action="{{ route('profile-background') }}" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <div class="card no-border elevation-3">
              <div class="card-header no-border">
                <h2 class="card-title">Profile Background</h2>
              </div>
              <div class="card-body">

                    <img src="{{url('images/'.$profile->bg)}}" alt="User Background" height="128">
                      <div class="input-group mt-5">
                        <div class="custom-file">
                          <input type="file" name="bg" class="form-control">
                        </div>
                        <div class="input-group-append">
                          <input type="hidden" name="_method" value="PUT">
                          <input type="submit" value="Upload" class="btn btn-info">
                        </div>
                      </div>
                </div>

              </div>
            </form>
        </div>

      </div>

        <div class="card no-border elevation-3">
          <div class="card-header">
            <h3 class="card-title">Profile Detail</h3>
          </div>
          <div class="card-body">

            <div class="row">
              <div class="col-4">
                <div class="form-group">
                  <label for="company">Company</label>
                  <input type="text" class="form-control" id="company" placeholder="Company" value="Nutrifood" readonly>
                </div>
              </div>
              <div class="col-4">
                <div class="form-group">
                  <label for="sub">Sub</label>
                  <select class="form-control select2" name="sub">
                      <option value="Ciawi">Ciawi</option>
                      <option value="Cibitung">Cibitung</option>
                  </select>
                </div>
              </div>
              <div class="col-4">
                <div class="form-group">
                  <label for="dept">Departement</label>
                  <select class="form-control select2" name="dept">
                      @foreach($departement as $dept)
                        <option value="{{ $dept->departement }}">{{ $dept->departement }}</option>
                      @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="row" style="margin-top:2%;">
              <div class="col-6">
                <div class="form-group">
                  <label for="first">First Name</label>
                  <input type="text" class="form-control" id="first" placeholder="e.g Alvin">
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label for="last">Last Name</label>
                  <input type="text" class="form-control" id="last" placeholder="e.g Ardianyah">
                </div>
              </div>
            </div>

            <div class="row" style="margin-top:2%">
              <div class="col-6">
                <div class="form-group">
                  <label for="tel">Phone Number</label>
                    <input type="tel" class="form-control" placeholder="e.g 08892312312" id="tel">
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" placeholder="example@mail.com">
                </div>
              </div>
            </div>

            <div class="row" style="margin-top:2%">
              <div class="col-12">
                <div class="form-group">
                  <label for="bio">Bio</label>
                  <textarea name="bio" class="form-control" style="resize:none !important;"></textarea>
                </div>
              </div>
            </div>

          </div>

          <div class="card-footer">
            <div class="row">
              <div class="col-4">
              </div>
              <div class="col-4">
                <button type="button" name="button" class="btn btn-primary col-12 btn-md">Save</button>
              </div>
            </div>
          </div>

        </div>

    </div>
  </div>
@endsection
@section('script')
    <script>
    $(document).ready(function(){
      $('.select2').select2({
        minimumResultsForSearch : -1
      })
    })
    </script>
@endsection