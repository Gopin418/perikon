@extends('admin.template.pending')
@section('header-color')
    bg-warning
@endsection
@section('brand-color')
    bg-warning
@endsection
@section('sidebar-color')
    sidebar-dark-warning
@endsection
@section('loading-style')
    fa-cog fa-pulse
@endsection
@section('content-header')
Izin Kerja Kontraktor
@endsection
@section('jenis')
<a href="{{ route('iko-approved') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;IKO&nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>&nbsp;&nbsp;
@endsection

@section('class-jenis-pen')
  card-warning
@endsection

@section('pending-datatable')
@if( $IKO_pending == 0 )
<div class="card-body">
    <p>Tidak ada IKO yang sedang menunggu persetujuan</p>
</div>
@else
<table class="table table-sm table-bordered table-striped table-hover w-100">
  <tr>
      <th>No IKO</th>
      <th>Nama Kontraktor</th>
      <th>Nama Pekerjaan</th>
      <th>Jenis Pekerjaan</th>
      <th>Lokasi</th>
      <th>Tanggal Mulai</th>
      <th>Tanggal Selesai</th>
      <th>Action</th>
  </tr>
  @endif
  @php
    $i = 0;
    $len = count($data);
  @endphp
  @foreach($data as $datas)
  <form action="{{ route('iko-approve', $datas->id) }}" method="post">
      <tr>
          <td style="vertical-align:middle">{{ $datas->no_iko }}</td>
          <td style="vertical-align:middle">{{ $datas->nama_kontraktor }}</td>
          <td style="vertical-align:middle">{{ $datas->nama_pekerjaan }}</td>
          <td style="vertical-align:middle">{{ $datas->jenis }}</td>
          <td style="vertical-align:middle">{{ 'Nutrifood ' . $datas->lokasi . ', ' . $datas->area }}</td>
          <td style="vertical-align:middle">{{ $datas->tgl_mulai }}</td>
          <td style="vertical-align:middle">{{ $datas->tgl_selesai }}</td>
          <td style="vertical-align:middle">
              <button style="margin-bottom:5px" type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default{{$datas->id}}">Detail</button>
              @if( Auth::user()->hasRole( 'Admin Utama') || Auth::user()->hasRole('Admin Developer'))
              <input style="margin-bottom:5px" type="submit" value="Approve" class="btn btn-success btn-xs">
              @else
              @endif
          </td>
      </tr>
      {{ csrf_field() }}
      <input type="hidden" name="_method" value="PUT">
  </form>
      @endforeach
    </table>
    @foreach ($data as $datas)
    <form action="{{ route('iko-approve', $datas->id) }}" method="POST">
    <div class="modal fade centered-modal" role="dialog" tab-index="-1" id="modal-default{{$datas->id}}">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title">Detail IKO {{ $datas->nama_kontraktor }}</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6" style="border-right: 1px solid #ddd;">
                                <div class="card no-border text-muted scroll" style="font-size: 1.2em; font-style: 'Verdana'; card-shadow: none; height: 430px;">
                                    <div class="card-body p-0">
                                        <table class="table table-striped no-border">
                                            <tr>
                                                <td>Nama Kontraktor</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->nama_kontraktor }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->nama_pekerjaan }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->jenis }}</td>
                                            </tr>
                                            <tr>
                                                <td>Lokasi Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ 'Nutrifood '. $datas->lokasi }}</td>
                                            </tr>
                                            <tr>
                                                <td>Area Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->area }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Mulai</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->tgl_mulai }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Selesai</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->tgl_selesai }}</td>
                                            </tr>
                                            <tr>
                                                <td>Deskripsi Pekerjaan</td>
                                                <td>:</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white" colspan="3">
                                                    <p class="text-muted">{{ $datas->deskripsi }}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card no-border scroll" style="padding-right: 10px;card-shadow:none; height: 430px;">
                                    <div class="card-body p-0">
                                        <table class="table table-striped no-border">
                                            <tr>
                                                <td>Penanggung Jawab Kontraktor</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->pj_kontraktor }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Telp.</td>
                                                <td>:</td>
                                                <td class="text-right font-weight-bold">{{ $datas->telp_pj_kontraktor }}</td>
                                            </tr>
                                            <tr>
                                                <td>Penanggung Jawab Lapangan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->pj_lapangan }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Telp.</td>
                                                <td>:</td>
                                                <td class="text-right font-weight-bold">{{ $datas->telp_pj_lapangan }}</td>
                                            </tr>
                                            <tr>
                                                <td>Penanggung Jawab Project</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->pj_project }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Telp.</td>
                                                <td>:</td>
                                                <td class="text-right font-weight-bold">{{ $datas->telp_pj_project }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="pr-0 pl-0 bg-white"><hr></td>
                                            </tr>
                                            <tr>
                                                <td>Bekerja pada area GMP</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->area_gmp }}</td>
                                            </tr>
                                            <tr>
                                                <td>Barang yang disimpan</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="p-0 inside">
                                                        @foreach(explode(',', $datas->brg_yang_disimpan) as $data)
                                                        <li>{{ str_replace('"', '', $data)}}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Lokasi gudang</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->lokasi_gudang }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jenis pekerjaan berbahaya</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="p-0 inside">
                                                        @foreach(explode(',', $datas->pekerjaan_berbahaya) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Dokumen Lampiran</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="p-0 inside">
                                                        @foreach(explode(',', $datas->doc_lampiran) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kebutuhan utility</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="p-0 inside">
                                                        @foreach(explode(',', $datas->utility) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Sistem yang akan terganggu</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="p-0 inside">
                                                        @foreach(explode(',', $datas->sistem_terganggu) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->hasRole('Admin Utama') || Auth::user()->hasRole('Admin Developer'))
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="Approve">
                    </div>
                    @elseif(Auth::user()->hasRole('Admin Biasa'))

                    @endif
                </div>
            </div>
        </div>
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
        </form>
    @endforeach
@endsection
