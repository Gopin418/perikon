@extends('admin.template.approved')
@section('loading-style')
    fa-cog fa-pulse
@endsection
@section('jenis')
<a href="{{ route('iko-approved') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;IKO</a>&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;
@endsection
@section('export')
    <a href="{{ route('export',['type'=>'xlsx']) }}">
        <button type="button" class="btn btn-success bg-success border border-white btn-sm">
            <i class="fa fa-share-square-o"></i>
            Export
        </button>
    </a>
@endsection
@section('approved-datatable')
    @if( $IKO_approved == 0)
    <div class="card-body">
        <p>Tidak ada Data IKO Tersedia</p>
    </div>
    @else
    <table class="table table-bordered table-striped table-hover table-sm">
        <tr>
            <th class="align-middle">No IKO</th>
            <th class="align-middle">Nama Kontraktor</th>
            <th class="align-middle">Nama Pekerjaan</th>
            <th class="align-middle">Jenis Pekerjaan</th>
            <th class="align-middle">Lokasi</th>
            <th class="align-middle">Tanggal Mulai</th>
            <th class="align-middle">Tanggal Selesai</th>
            <th class="align-middle">Action</th>
        </tr>
    @endif
    @php
        $i = 0;
        $len = count($data);
    @endphp
    @foreach($data as $key => $datas)
        <tr class="align-middle">
            <td class="align-middler">{{ $datas->no_iko }}</td>
            <td class="align-middler">{{ $datas->nama_kontraktor }}</td>
            <td class="align-middler">{{ $datas->nama_pekerjaan }}</td>
            <td class="align-middler">{{ $datas->jenis }}</td>
            <td class="align-middler">{{ 'Nutrifood ' . $datas->lokasi . ', ' . $datas->area }}</td>
            <td class="align-middler">{{ $datas->tgl_mulai }}</td>
            <td class="align-middler">{{ $datas->tgl_selesai }}</td>
            <td class="align-middle flex-center text-center">
                <button type="button" class="btn btn-primary btn-xs ml-auto mr-auto" data-toggle="modal" data-target="#modal-default{{$datas->id}}">Detail</button>
            </td>
        </tr>
    @endforeach
    </table>
    @foreach($data as $key => $datas)
    <div class="modal fade" role="dialog" id="modal-default{{$datas->id}}">
            <div class="modal-dialog modal-xl h-100 d-flex flex-column justify-content-center my-0 text-secondary">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Detail IKO {{ $datas->nama_kontraktor }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6" style="border-right: 1px solid #ddd">
                                <div class="card no-border text-muted scroll" style="height: 460px; font-size: 1.2em; font-style: 'Verdana'; card-shadow: none;">
                                    <div class="card-body p-0">
                                        <table class="table table-striped no-border">
                                            <tr>
                                                <td>Nama Kontraktor</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->nama_kontraktor }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->nama_pekerjaan }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->jenis }}</td>
                                            </tr>
                                            <tr>
                                                <td>Lokasi Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ 'Nutrifood ' . $datas->lokasi }}</td>
                                            </tr>
                                            <tr>
                                                <td>Area Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->area }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Mulai</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->tgl_mulai }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Selesai</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->tgl_selesai }}</td>
                                            </tr>
                                            <tr>
                                                <td>Deskripsi Pekerjaan</td>
                                                <td>:</td>
                                                <td></td>
                                            </tr>
                                            <tr class="bg-white">
                                                <td colspan="3">
                                                    <p class="text-muted">{{ $datas->deskripsi }}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card no-border scroll" style="height: 460px;card-shadow:none">
                                    <div class="card-body p-0">
                                        <table class="table table-striped no-border">
                                            <tr>
                                                <td>Penanggung jawab kontraktor</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->pj_kontraktor }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Telp.</td>
                                                <td>:</td>
                                                <td class="text-right font-weight-bold">{{ $datas->telp_pj_kontraktor }}</td>
                                            </tr>
                                            <tr>
                                                <td>Penanggung jawab lapangan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->pj_lapangan }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Telp.</td>
                                                <td>:</td>
                                                <td class="text-right font-weight-bold">{{ $datas->telp_pj_lapangan }}</td>
                                            </tr>
                                            <tr>
                                                <td>Penanggung jawab project</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->pj_project }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Telp.</td>
                                                <td>:</td>
                                                <td class="text-right font-weight-bold">{{ $datas->telp_pj_project }}</td>
                                            </tr>
                                            <tr class="bg-white">
                                                <td colspan="3"><hr></td>
                                            </tr>
                                            <tr>
                                                <td>Bekerja pada area GMP</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->area_gmp }}</td>
                                            </tr>
                                            <tr>
                                                <td>Barang yang disimpan</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="inside p-0">
                                                        @foreach(explode(',', $datas->brg_yang_disimpan) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Lokasi Gudang</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $datas->lokasi_gudang }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jenis pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="inside no-padding">
                                                        @foreach(explode(',', $datas->pekerjaan_berbahaya) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Dokumen Lampiran</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="inside no-padding">
                                                        @foreach(explode(',', $datas->doc_lampiran) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kebutuhan utility</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="inside no-padding">
                                                        @foreach(explode(',', $datas->utility) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Sistem yang akan terganggu</td>
                                                <td>:</td>
                                                <td class="text-right" dir="rtl">
                                                    <ul class="inside no-padding">
                                                        @foreach(explode(',', $datas->sistem_terganggu) as $data)
                                                        <li>{{ str_replace('"', '', $data) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
