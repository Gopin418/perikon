@extends('admin')

@section('loading-style')
fa-dashboard fa-shake
@endsection

@section('content-header')
Contractor Request
@endsection

@section('breadcrumb')
<li><a href="{{ route('request') }}" class="text-dark active">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;Contractor Request</a></li>
@endsection

@section('content-header-description')
{{ Auth::user()->name }}
@endsection

@section('content')

  <div class="card">
    <div class="card-body">
      @if (count($contractor) == 0)
        Tidak ada kontraktor yang meminta perizinan
      @else
        <table class="table table-striped table-hover table-sm">
          <thead>
            <tr>
              <td>No</td>
              <td>Nama Kontraktor</td>
              <td>Nama Pekerjaan</td>
              <td>Tanggal Mulai</td>
              <td>Tanggal Selesai</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            @foreach ($contractor as $key => $value)
              <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->nama_kontraktor }}</td>
                <td>{{ $value->nama_pekerjaan }}</td>
                <td>{{ $value->tgl_mulai }}</td>
                <td>{{ $value->tgl_selesai }}</td>
                <td>
                  <form action="{{ route('requestApprove', $value->id) }}" method="post">
                    {{ csrf_field() }}
                    <button type="submit" name="button" class="btn btn-xs btn-success">Approve</button>
                    <input type="hidden" name="_method" value="PUT">
                  </form>
                  <form action="{{ route('requestDecline', $value->id) }}" method="post">
                    {{ csrf_field() }}
                    <button type="submit" name="button" class="btn btn-xs btn-danger">Decline</button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      @endif
    </div>
  </div>

@endsection
@section('script')
<script>
</script>
@endsection
