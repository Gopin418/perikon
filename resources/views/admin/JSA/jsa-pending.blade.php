@extends('admin.template.pending')
@section('loading-style')
    fa-plus fa-shake
@endsection
@section('content-header')
Job Safety Analysis
@endsection

@section('jenis')
<a href="{{ route('jsa-approved') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;JSA&nbsp;&nbsp;<i class="fa fa-caret-right">&nbsp;&nbsp;</i></a>
@endsection

@section('class-jenis-pen')
  card-primary
@endsection

@section('pending-datatable')
@if( $JSA_pending == 0 )
<div class="card-body">
    <p>Tidak ada JSA yang sedang menunggu persetujuan</p>
</div>
@else
<table class="table table-bordered table-striped table-hover table-sm">
    <tr>
        <th class="align-middle">No JSA</th>
        <th class="align-middle">Nama Kontraktor</th>
        <th class="align-middle">Nama Pekerjaan</th>
        <th class="align-middle">Jenis Pekerjaan</th>
        <th class="align-middle">Lokasi</th>
        <th class="align-middle">Tanggal Mulai</th>
        <th class="align-middle">Tanggal Selesai</th>
        <th class="align-middle text-center">Action</th>
    </tr>

@endif
@foreach( $iko as $dataIKO )
    <form action="{{ route('jsa-approve', $dataIKO->id) }}" method="POST">
    {{ csrf_field() }}
    <tr>
        <td class="align-middle">{{ $dataIKO->no_jsa }}</td>
        <td class="align-middle">{{ $dataIKO->nama_kontraktor }}</td>
        <td class="align-middle">{{ $dataIKO->nama_pekerjaan }}</td>
        <td class="align-middle">{{ $dataIKO->jenis }}</td>
        <td class="align-middle">{{ 'Nutrifood ' . $dataIKO->lokasi . ', ' . $dataIKO->area }}</td>
        <td class="align-middle">{{ $dataIKO->tgl_mulai }}</td>
        <td class="align-middle">{{ $dataIKO->tgl_selesai }}</td>
        <td style="width: 12.5%" class="align-middle text-center">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default{{$dataIKO->id}}">Detail</button>
                @if( Auth::user()->hasRole('Admin Utama') || Auth::user()->hasRole('Admin Developer') )
                <input type="submit" value="Approve" class="btn btn-success btn-xs">
                @elseif(Auth::user()->hasRole('Admin Biasa'))
                @endif
        </td>
    </tr>
    <input type="hidden" name="_method" value="PUT">
    </form>
    @endforeach
</table>

@foreach ($iko as $key => $dataIKO)
    <form action="{{ route('jsa-approve', $dataIKO->id) }}" method="POST">
    {{ csrf_field() }}
    {{-- Modal --}}
    <div class="modal fade centered-modal" role="dialog" id="modal-default{{$dataIKO->id}}">
            <div class="modal-dialog modal-xxl" style="z-index: 100">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <h4 class="modal-title text-dark">Detail JSA {{ $dataIKO->nama_kontraktor }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5" style="border-right: 1px solid #ddd">
                                <div class="card no-border scroll text-muted" style="height: 460px;font-size: 1.2em; font-style: 'Verdana'; card-shadow: none;">
                                    <div class="card-body p-0">
                                        <table class="table table-striped no-border">
                                            <tr>
                                                <td>Nama Kontraktor</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $dataIKO->nama_kontraktor }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $dataIKO->nama_pekerjaan }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $dataIKO->jenis }}</td>
                                            </tr>
                                            <tr>
                                                <td>Lokasi Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $dataIKO->lokasi }}</td>
                                            </tr>
                                            <tr>
                                                <td>Area Pekerjaan</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $dataIKO->area }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Mulai</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $dataIKO->tgl_mulai }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Selesai</td>
                                                <td>:</td>
                                                <td class="text-right">{{ $dataIKO->tgl_selesai }}</td>
                                            </tr>
                                            <tr>
                                                <td>Deskripsi Pekerjaan</td>
                                                <td>:</td>
                                                <td></td>
                                            </tr>
                                            <tr class="bg-white">
                                                <td colspan="3">
                                                    <p class="text-muted">{{ $dataIKO->deskripsi }}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="card no-border scroll" style="height: 460px;card-shadow:none">
                                    <div class="card-header bg-light text-center"><span class="text-dark"><b>DETAILS</b></span></div>
                                        <div class="card-body p-0">
                                            <div class="table-responsive">
                                                <table class="table responsive table-bordered success table-striped">
                                                    <tr>
                                                        <th class="align-middle">Nama Aktivitas</th>
                                                        <th class="align-middle">Potensi Bahaya</th>
                                                        <th class="align-middle">Pengendalian Bahaya</th>
                                                        <th class="align-middle">Penanggung Jawab</th>
                                                        <th class="align-middle">Keterangan</th>
                                                    </tr>
                                                    <?php
                                                        $dataJSA = $iko[$key];
                                                        $potensi = explode('","', $dataJSA->potensi_bahaya);
                                                        $pengendalian = explode('","', $dataJSA->pengendalian_bahaya);
                                                        $pj = explode(',', $dataJSA->penanggung_jawab);
                                                        $keterangan = explode(',', $dataJSA->keterangan);
                                                    ?>
                                                    @foreach (explode(',', $dataJSA->aktivitas) as $key => $aktivitas)
                                                    <tr>
                                                        <td class="align-middle">
                                                            {{ str_replace('"', '', $aktivitas) }}
                                                        </td>
                                                        <td class="align-middle">
                                                            <ul class="inside p-0">
                                                            @foreach (explode(', ', $potensi[$key]) as $key2 => $potensi_bahaya)
                                                                <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $potensi_bahaya) }}</li>
                                                            @endforeach
                                                            </ul>
                                                        </td>
                                                        <td class="align-middle">
                                                            <ul class="inside p-0">
                                                            @foreach (explode(', ', $pengendalian[$key]) as $key3 => $pengendalian_bahaya)
                                                                <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $pengendalian_bahaya) }}</li>
                                                            @endforeach
                                                            </ul>
                                                        </td>
                                                        <td class="align-middle">
                                                            {{ preg_replace('/[^a-zA-Z 0-9]+/', '', $pj[$key]) }}
                                                        </td>
                                                        <td class="align-middle">
                                                            {{ preg_replace('/[^a-zA-Z 0-9]+/', '', $keterangan[$key]) }}
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->hasRole('Admin Utama') || Auth::user()->hasRole('Admin Developer'))
                        <div class="modal-footer">
                            <input type="submit" value="Approve" class="btn btn-success">
                        </div>
                        @elseif(Auth::user()->hasRole('Admin Biasa'))

                        @endif
                    </div>
                </div>
            </div>
        <input type="hidden" name="_method" value="PUT">
    </form>

@endforeach

@endsection
