@extends('admin.template.approved')
@section('loading-style')
    fa-plus fa-shake
@endsection
@section('content-header')
Job Safety Analysis
@endsection

@section('jenis')
<a href="{{ route('jsa-approved') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;JSA&nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>&nbsp;&nbsp;</li>
@endsection
@section('card-header-color')
    bg-info
@endsection
@section('approved-datatable')
@if( $JSA_approved == 0 )
<div class="card-body">
    <p>Tidak ada Data JSA Tersedia</p>
</div>
@else
<table class="table table-bordered table-striped table-sm table-hover">
    <tr>
        <th class="align-middle">No JSA</th>
        <th class="align-middle">Nama Kontraktor</th>
        <th class="align-middle">Nama Pekerjaan</th>
        <th class="align-middle">Jenis Pekerjaan</th>
        <th class="align-middle">Lokasi</th>
        <th class="align-middle">Tanggal Mulai</th>
        <th class="align-middle">Tanggal Selesai</th>
        <th class="align-middle">Action</th>
    </tr>
@foreach( $iko as $key => $dataIKO )
    <tr>
        <td class="align-middle">{{ $dataIKO->no_jsa }}</td>
        <td class="align-middle">{{ $dataIKO->nama_kontraktor }}</td>
        <td class="align-middle">{{ $dataIKO->nama_pekerjaan }}</td>
        <td class="align-middle">{{ $dataIKO->jenis }}</td>
        <td class="align-middle">{{ 'Nutrifood ' . $dataIKO->lokasi . ', ' . $dataIKO->area }}</td>
        <td class="align-middle">{{ $dataIKO->tgl_mulai }}</td>
        <td class="align-middle">{{ $dataIKO->tgl_selesai }}</td>
        <td class="align-middle">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-default{{$dataIKO->id}}">Detail</button>
        </td>
    </tr>
@endforeach
</table>
@endif
@foreach($iko as $key => $dataIKO)
{{-- Modal --}}
<div class="modal fade centered-modal" role="dialog" id="modal-default{{$dataIKO->id}}">
        <div class="modal-dialog modal-xxl">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title">Detail JSA {{ $dataIKO->nama_kontraktor }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4" style="border-right: 1px solid #ddd">


                            <div class="card no-border scroll text-muted" style="height: 460px;font-size: 1.2em; font-style: 'Verdana'; card-shadow: none;">
                                <div class="card-body p-0">
                                    <table class="table table-striped no-border">
                                        <tr>
                                            <td>Nama Kontraktor</td>
                                            <td>:</td>
                                            <td class="text-right">{{ $dataIKO->nama_kontraktor }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Pekerjaan</td>
                                            <td>:</td>
                                            <td class="text-right">{{ $dataIKO->nama_pekerjaan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Pekerjaan</td>
                                            <td>:</td>
                                            <td class="text-right">{{ $dataIKO->jenis }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lokasi Pekerjaan</td>
                                            <td>:</td>
                                            <td class="text-right">{{ 'Nutrifood ' . $dataIKO->lokasi }}</td>
                                        </tr>
                                        <tr>
                                            <td>Area Pekerjaan</td>
                                            <td>:</td>
                                            <td class="text-right">{{ $dataIKO->area }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Mulai</td>
                                            <td>:</td>
                                            <td class="text-right">{{ $dataIKO->tgl_mulai }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Selesai</td>
                                            <td>:</td>
                                            <td class="text-right">{{ $dataIKO->tgl_selesai }}</td>
                                        </tr>
                                        <tr>
                                            <td>Deskripsi Pekerjaan</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="bg-white">
                                                <p class="text-muted">{{ $dataIKO->deskripsi }}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="card no-border scroll" style="height: 460px;card-shadow:none">
                                <div class="card-header bg-info">
                                    <h4 class="card-title">
                                        DETAILS
                                    </h4>
                                </div>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                      <table class="table responsive table-bordered table-striped nowrap">
                                        <tr>
                                          <th>Nama Aktivitas</th>
                                          <th>Potensi Bahaya</th>
                                          <th>Pengendalian Bahaya</th>
                                          <th>Penanggung Jawab</th>
                                          <th>Keterangan</th>
                                        </tr>
                                          <?php
                                            $data = $iko[$key];
                                            $potensi = explode('","', $data->potensi_bahaya);
                                            $pengendalian = explode('","', $data->pengendalian_bahaya);
                                            $pj = explode(',', $data->penanggung_jawab);
                                            $keterangan = explode(',', $data->keterangan);
                                          ?>
                                          @foreach (explode(',', $data->aktivitas) as $key => $aktivitas)
                                            <tr>
                                              <td>
                                                {{ str_replace('"', '', $aktivitas) }}
                                              </td>
                                              <td>
                                                <ul style="list-style-position: inside; padding-left: 0">
                                                  @foreach (explode(', ', $potensi[$key]) as $key2 => $potensi_bahaya)
                                                    <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $potensi_bahaya) }}</li>
                                                  @endforeach
                                                </ul>
                                              </td>
                                              <td>
                                                <ul style="list-style-position: inside; padding-left: 0">
                                                  @foreach (explode(', ', $pengendalian[$key]) as $key3 => $pengendalian_bahaya)
                                                    <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $pengendalian_bahaya) }}</li>
                                                  @endforeach
                                                </ul>
                                              </td>
                                              <td>
                                                {{ preg_replace('/[^a-zA-Z 0-9]+/', '', $pj[$key]) }}
                                              </td>
                                              <td>
                                                {{ preg_replace('/[^a-zA-Z 0-9]+/', '', $keterangan[$key]) }}
                                              </td>
                                            </tr>
                                          @endforeach
                                      </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach

@endsection
