@extends('admin')


@section('breadcrumb')
<li>@yield('jenis')</li>
<li><a href="#" class="text-dark active">Pending</a></li>
@endsection


@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header bg-info no-border">
                <h3 class="card-title">Pending Permission</h3>
            </div>

            <div class="card-body table-responsive p-0">



                
                    @yield('pending-datatable')



            </div>
        </div>
    </div>
</div>
@endsection
