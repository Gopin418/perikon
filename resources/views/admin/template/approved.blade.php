@extends('admin')

@section('content-header')
Izin Kerja Kontraktor
@endsection

@section('breadcrumb')
    @if(Auth::user()->hasRole('Admin Utama') || Auth::user()->hasRole('Admin Developer'))
        <li>@yield('jenis')</li>
        <li><a href="#" class="text-dark active">Approved</a></li>
    @elseif(Auth::user()->hasRole('Admin Biasa'))
        <li class="active">@yield('jenis')</li>
    @endif
@endsection


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card @yield('class-jenis-app')">
                @if(Auth::user()->hasRole('Admin Utama') || Auth::user()->hasRole('Admin Developer'))
                <div class="card-header bg-info">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="card-title">Approved Contract</h3>
                        </div>
                        <div class="col-6">
                            <div class="pull-right">
                                @yield('export')
                            </div>
                        </div>
                    </div>
                </div>
                @elseif(Auth::user()->hasRole('Admin Biasa'))

                @endif

            <div class="card-body table-responsive p-0 ">

                    @yield('approved-datatable')
            </div>
        </div>
    </div>
</div>
@endsection
