@extends('admin')

@section('skin')
skin-purple
@endsection

@section('loading-skin')
bg-purple
@endsection

@section('loading-style')
fa-dashboard fa-shake
@endsection

@section('content-header')
Dashboard Admin
@endsection

@section('content-header-description')
{{ Auth::user()->name }}
@endsection

@section('content')

{{-- Box info --}}
<div class="row">

    <div class="col-lg-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="card bg-info-gradient elevation-3" id="calendarCard">
                    <div class="card-body p-0 cal">
                        <div id="calendar" style="width:100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card pb-2 bg-info-gradient no-border elevation-3" id="cardUtility">
                    <div class="card-header no-border">
                        <h4 class="card-title">Penanggung Jawab Utility</h4>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-sm table-hover no-border">
                            @foreach ($utility as $item)
                            <tr>
                                <td>{{ $item->jenis }}</td>
                                <td>:</td>
                                <td>{{ $item->name }}</td>
                                <td>|</td>
                                <td>{{ $item->departement }}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-8">
        <div class="row">
            <div class="col-lg-6">
                <div class="card bg-info-gradient text-center elevation-3" id="cardChart" style="border-color: #605ca8 !important">
                    <div class="card-body">
                        <canvas id="contractor" height="195"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card small-box bg-warning no-border elevation-3">
                            <div class="overlay" id="ikoLoad">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                            <div class="inner iko">
                                <div class="inner_IKO">
                                    <h3 id="jumlah_IKO"></h3>
                                </div>
                            </div>
                            <div class="inner_IKO">
                                <div class="icon iko ">
                                    <i class="fa fa-cogs"></i>
                                </div>
                            </div>
                            <a href="
                                            @if($IKO_approved > $IKO_pending)
                                                {{ route('iko-approved') }}
                                            @elseif($IKO_approved <= $IKO_pending || $jumlah == 0)
                                                {{ route('iko-pending') }}
                                            @endif
                                            "
                              class="small-box-footer iko inner_IKO">
                                More Details <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card small-box bg-primary elevation-3">
                            <div class="overlay" id="jsaLoad">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                            <div class="inner jsa ">
                                <h3 id="jumlah_JSA"></h3>
                            </div>
                            <div class="icon jsa ">
                                <i class="fa fa-plus"></i>
                            </div>
                            <a href="
                                                    @if($JSA_approved > $JSA_pending)
                                                      {{ route('jsa-approved') }}
                                                    @elseif($JSA_approved <= $JSA_pending || $jumlahJSA == 0)
                                                      {{ route('jsa-pending') }}
                                                    @endif
                                                        "
                              class="small-box-footer jsa ">
                                More Details <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card small-box bg-success elevation-3">
                            <div class="overlay" id="adlLoad">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                            <div class="inner adl">
                                <h3 id="jumlah_ADL"></h3>
                            </div>
                            <div class="icon adl ">
                                <i class="fa fa-envira"></i>
                            </div>
                            <a href="
                                                      @if($ADL_approved > $ADL_pending)
                                                        {{ route('adl-approved') }}
                                                      @elseif($ADL_approved <= $ADL_pending || $jumlahADL == 0)
                                                        {{ route('adl-pending') }}
                                                      @endif
                                                        "
                              class="small-box-footer adl ">
                                More Details <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card small-box bg-danger elevation-3">
                            <div class="overlay" id="ipbLoad">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                            <div class="inner ipb">
                                <h3 id="jumlah_IPB"></h3>
                            </div>
                            <div class="icon ipb ">
                                <i class="fa fa-fire"></i>
                            </div>
                            <a href="
                                                      @if($IPB_approved > $IPB_pending)
                                                        {{ route('ipb-approved') }}
                                                      @elseif($IPB_approved <= $IPB_pending || $jumlahIPB == 0)
                                                        {{ route('ipb-pending') }}
                                                      @endif
                                                        "
                              class="small-box-footer ipb ">
                                More Details <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Project Data --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="card no-border upcoming shadow" id="cardUpcoming">
                    <div class="card-header bg-info-gradient">
                        <h4 class="card-title">Project Data</h4>
                    </div>
                    <div id="upcoming">
                      @include('admin.dashboard.upcoming')
                    </div>
                </div>
            </div>
        </div>

        @if ($ongoingData == 0)

        @else
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow">
                    <div class="card-header bg-info-gradient" id="cardOngoing">
                        <h4 class="card-title">Ongoing</h4>
                    </div>
                    <div id="ongoing">
                      @include('admin.dashboard.ongoing')
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
@section('script')
<script>
    $.widget.bridge('uibutton', $.ui.button)

    function dataLoad() {
        // sidebar
        $('.label.pull-right.bg-yellow.iko').load('{{ url('/api/iko/pending') }}');
        $('.label.pull-right.bg-yellow.jsa').load('{{ url('/api/jsa/pending') }}');
        $('.label.pull-right.bg-yellow.adl').load('{{ url('/api/adl/pending') }}');
        $('.label.pull-right.bg-yellow.ipb').load('{{ url('/api/ipb/pending') }}');



        // IKO
        $('#jumlah_IKO').load('{{ url('/api/iko ') }}', function() {
            $('#ikoLoad').fadeOut('slow');
        });

        // JSA
        $('#jumlah_JSA').load('{{ url('/api/jsa ') }}', function() {
            $('#jsaLoad').fadeOut('slow');
        });

        // ADL
        $('#jumlah_ADL').load('{{ url('/api/adl ') }}', function() {
            $('#adlLoad').fadeOut('slow');
        });

        // IPB
        $('#jumlah_IPB').load('{{ url('/api/ipb ') }}', function() {
            $('#ipbLoad').fadeOut('slow')
        });

        $('#loading-page').fadeOut('slow');


    }


    $(window).on('load', function() {
        $('#cardUtility').hide();
        $('#cardUtility').height(0);
        $('#cardChart').hide();
        $('#cardChart').height(0);
        $('.small-box').hide();
        $('#cardUpcoming').hide();
        $('#cardOngoing').hide();
    })
    $(document).ready(function() {

      
        $('.inner').css('height', '100px');

        $('#calendarCard').animate({
            height: '400px'
        }, {
            duration: 'slow',
            complete: function() {
                $('#cardUtility').show();
                $('#cardUtility').animate({
                    height: '200px'
                }, 'slow');
            }
        });
        $('#calendar').datepicker({
            todayHighlight: true,
        });

        $('#cardChart').show();
        $('#cardChart').animate({
            height: '275px'
        }, {
            duration: 'slow',
            complete: function() {
                $('#cardUpcoming').fadeIn('slow', function() {
                    $('#cardOngoing').fadeIn('slow');
                });
            }
        });
        // $('#upcoming').load('{{ url('admin/dashboard/upcoming') }}', function() {
        //             $('#cardUpcoming').fadeIn('slow', function() {
        //                 $('#ongoing').load('{{ url('/admin/dashboard/ongoing') }}', function() {
        //                     $('#cardOngoing').fadeIn('slow');
        //                 });
        //             })
        //         });




        $('.small-box.bg-warning').fadeIn('normal', function() {
            $('.small-box.bg-primary').fadeIn('normal', function() {
                $('.small-box.bg-success').fadeIn('normal', function() {
                    $('.small-box.bg-danger').fadeIn('normal');
                })
            });
        });

        $('#upcoming').on('click', '.paginate a', function(e) {
            e.preventDefault();
            var url = $(this).attr(data);

            $.get(url, function(data) {
                $('#upcoming').html(data);
            });
        });


        var project = '{{ $project }}';
        var maintenance = '{{ $maintenance }}';
        var ctx = $('#contractor');


        var DoughnutChart = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [project, maintenance],
                    backgroundColor: ['#0277bd', '#ffa726']
                }],
                labels: ['Project', 'Maintenance']
            },
            options: {
                legend: {
                    display: true,
                    labels: {
                        fontColor: '#fff',
                        boxWidth: 15
                    },
                    reverse: true,
                },
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });

        var pusher = new Pusher('9e51398a10f7aacc64d9', {
            cluster: 'ap1',
            encrypted: true
        });
        Pusher.logToConsole = true;
        //Also remember to change channel and event name if your's are different.
        var channel = pusher.subscribe('notify');
        $('#close').click(function() {
            $('.notif').hide(100);
        });
        channel.bind('notify-event', function(message) {
            $('.notif').hide(100);
            $('.notif').show(100);

            setTimeout(function() {
                $('.notif').hide(100);
            }, 10000);

            dataLoad();
        });
    });
</script>
@endsection
