@extends('admin')

@section('skin')
  skin-purple
@endsection

@section('content-header')
Dashboard Admin
@endsection

@section('content-header-description')
{{ Auth::user()->name }}
@endsection

@section('content')
  <div class="error-page">
    <h2 class="headline text-red">500</h2>

    <div class="error-content">
      <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>

      <p>
        We will work on fixing that right away.
        Meanwhile, you may <a href="{{ route('dashboard') }}">return to dashboard</a> or try using the search form.
      </p>

    </div>
  </div>
@endsection
