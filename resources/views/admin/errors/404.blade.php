@extends('admin')

@section('skin')
  skin-purple
@endsection

@section('breadcrumb')
<li class="active">404</li>
@endsection

@section('content-header-description')
{{ Auth::user()->name }}
@endsection

@section('content')
      <div class="error-page" style="margin-top:7%">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content" style="padding-top: 20px">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <a href="{{ route('dashboard') }}">return to dashboard</a>.
          </p>

        </div>
        <!-- /.error-content -->
      </div>
@endsection
