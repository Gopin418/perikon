@extends('admin')
@section('skin')
  skin-red
@endsection
@section('loading-skin')
    bg-red
@endsection
@section('loading-style')
    fa-wrench fa-shake
@endsection
@section('content-header')
Approval Utility
@endsection

@section('breadcrumb')
<li><a href="{{ route('pj-utility') }}" class="text-dark">&nbsp;&nbsp;<i class="fa fa-caret-right"></i>&nbsp;&nbsp;Approval Utility</a></li>
@endsection

@section('content')

<div class="row mb-5">

  {{-- Air --}}
  <div class="col-3">
    <div class="card card-solid card-info elevation-4">

      {{-- Header --}}
      <div class="card-header">
        <h3 class="card-title">Penanggung Jawab Air</h3>
      </div>

      {{-- Body Air --}}
      <div class="card-body">
        <form action="/admin/utility/air/{{ $air->id }}" method="post">
          {{ csrf_field() }}

          {{-- Nama --}}
          <div class="form-group">
            <label for="nama_air">Nama</label>
            <input type="text" name="name_air" class="form-control" placeholder="e.g Alvin Ardiansyah" value="{{ $air->name }}">
          </div>

          {{-- email --}}
          <div class="form-group">
            <label for="email_pj_air">Email</label>
            <input type="email" name="email_pj_air" class="form-control" placeholder="e.g alvinardiansyah@example.com" value="{{ $air->email }}">
          </div>

          {{-- Telepon --}}
          <div class="form-group">
            <label for="telp_pj_air">No. Telp</label>
            <input type="text" name="telp_pj_air" class="form-control" placeholder="e.g 08983201224" value="{{ $air->telp }}">
          </div>

          {{-- Departement --}}
          <div class="form-group">
              <label for="departement_air">Departement</label>
              <select class="form-control select2" name="departement_air">
                  @foreach($departement as $dept)
                    <option value="{{ $dept->departement }}"
                      @if($dept->departement == $air->departement)
                        selected="selected"
                      @endif
                      >
                        {{ $dept->departement }}
                      </option>
                  @endforeach
              </select>
          </div>

          {{-- Submit --}}
          <div class="form-group">
            <input type="hidden" name="_method" value="PUT">
            <input type="submit" name="submit" value="Save" class="btn btn-info btn-block">
          </div>

        </form>
      </div>

    </div>
  </div>

  {{-- Angin --}}
  <div class="col-3">
    <div class="card card-solid card-info elevation-4">

      {{-- Header --}}
      <div class="card-header">
        <h3 class="card-title">Penanggung Jawab Angin</h3>
      </div>

      {{-- Body --}}
      <div class="card-body">
        <form action="/admin/utility/angin/{{ $angin->id }}" method="post">
          {{ csrf_field() }}

          {{-- Nama --}}
          <div class="form-group">
            <label for="nama_angin">Nama</label>
            <input type="text" name="name_angin" class="form-control" placeholder="e.g Alvin Ardiansyah" value="{{ $angin->name }}">
          </div>

          {{-- email --}}
          <div class="form-group">
            <label for="email_pj_angin">Email</label>
            <input type="email" name="email_pj_angin" class="form-control" placeholder="e.g alvinardiansyah@example.com" value="{{ $angin->email }}">
          </div>

          {{-- Telepon --}}
          <div class="form-group">
            <label for="telp_pj_angin">No. Telp</label>
            <input type="text" name="telp_pj_angin" class="form-control" placeholder="e.g 08983201224" value="{{ $angin->telp }}">
          </div>

          {{-- Departement --}}
          <div class="form-group">
              <label for="departement_angin">Departement</label>
              <select class="form-control select2" name="departement_angin">
                  @foreach ($departement as $dept)
                    <option value="{{ $dept->departement }}"
                      @if($dept->departement == $angin->departement)
                        selected=="selected"
                      @endif
                      >
                      {{ $dept->departement }}
                    </option>
                  @endforeach
              </select>
          </div>

          {{-- Submit --}}
          <div class="form-group">
            <input type="hidden" name="_method" value="PUT">
            <input type="submit" name="submit" value="Save" class="btn btn-info col-12">
          </div>

        </form>
      </div>

    </div>
  </div>

  {{-- Listrik --}}
  <div class="col-3">
    <div class="card card-solid card-info elevation-4">

      {{-- Header --}}
      <div class="card-header">
        <h3 class="card-title">Penanggung Jawab Listrik</h3>
      </div>

      {{-- Body --}}
      <div class="card-body">
        <form action="/admin/utility/listrik/{{ $listrik->id }}" method="post">
          {{ csrf_field() }}

          {{-- Nama --}}
          <div class="form-group">
            <label for="nama_listrik">Nama</label>
            <input type="text" name="name_listrik" class="form-control" placeholder="e.g Alvin Ardiansyah" value="{{ $listrik->name }}">
          </div>

          {{-- email --}}
          <div class="form-group">
            <label for="email_pj_listrik">Email</label>
            <input type="email" name="email_pj_listrik" class="form-control" placeholder="e.g alvinardiansyah@example.com" value="{{ $listrik->email }}">
          </div>

          {{-- Telepon --}}
          <div class="form-group">
            <label for="telp_pj_listrik">No. Telp</label>
            <input type="text" name="telp_pj_listrik" class="form-control" placeholder="e.g 08983201224" value="{{ $listrik->telp }}">
          </div>

          {{-- Departement --}}
          <div class="form-group">
              <label for="departement_listrik">Departement</label>
              <select class="form-control select2" name="departement_listrik">
                  @foreach ($departement as $dept)
                    <option value="{{ $dept->departement }}"
                      @if($dept->departement == $listrik->departement)
                        selected="selected"
                      @endif
                      >
                      {{ $dept->departement }}
                    </option>
                  @endforeach
              </select>
          </div>

          {{-- Submit --}}
          <div class="form-group">
            <input type="hidden" name="_method" value="PUT">
            <input type="submit" name="submit" value="Save" class="btn btn-info col-12">
          </div>

        </form>
      </div>

    </div>
  </div>

  {{-- Steam --}}
  <div class="col-3">
    <div class="card card-solid card-info elevation-4">

      {{-- Header --}}
      <div class="card-header ">
        <h3 class="card-title">Penanggung Jawab Steam</h3>
      </div>

      {{-- Body --}}
      <div class="card-body">
        <form action="/admin/utility/steam/{{ $steam->id }}" method="post">
          {{ csrf_field() }}

          {{-- Nama --}}
          <div class="form-group">
            <label for="nama_steam">Nama</label>
            <input type="text" name="name_steam" class="form-control" placeholder="e.g Alvin Ardiansyah" value="{{ $steam->name }}">
          </div>

          {{-- email --}}
          <div class="form-group">
            <label for="email_pj_steam">Email</label>
            <input type="email" name="email_pj_steam" class="form-control" placeholder="e.g alvinardiansyah@example.com" value="{{ $steam->email }}">
          </div>

          {{-- Telepon --}}
          <div class="form-group">
            <label for="telp_pj_steam">No. Telp</label>
            <input type="text" name="telp_pj_steam" class="form-control" placeholder="e.g 08983201224" value="{{ $steam->telp }}">
          </div>

          {{-- Departement --}}
          <div class="form-group">
              <label for="departement_steam">Departement</label>
              <select class="form-control select2" name="departement_steam">
                  @foreach ($departement as $dept)
                    <option value="{{ $dept->departement }}"
                      @if($dept->departement == $steam->departement)
                        selected="selected"
                      @endif
                      >
                      {{ $dept->departement }}
                    </option>
                  @endforeach
              </select>
          </div>

          {{-- Submit --}}
          <div class="form-group">
            <input type="hidden" name="_method" value="PUT">
            <input type="submit" name="submit" value="Save" class="btn btn-info col-12">
          </div>

        </form>
      </div>

    </div>
  </div>

</div>
@endsection
@section('script')
<script>
$(document).ready(function() {
  $('.select2').select2({
    minimumResultsForSearch : -1
  });
})
</script>
@endsection