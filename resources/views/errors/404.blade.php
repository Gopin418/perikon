<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>404 - Not Found</title>
    {{ Asset::add('Bootstrap', '/css/bootstrap.css') }}
    {{ Asset::add('master', '/css/master.css') }}
    {{ Asset::add('fontawesome', '/css/font-awesome.css') }}

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <style media="screen">
      body {
        font-family: 'roboto'
      }
    </style>
  </head>
  <body class="bg-dark">
    <div class="d-sm-none d-lg-block">
      <div class="container text-center">
        <div class="mt-5 pt-5 text-light">
          <div class="display-1">
            <span class="fa fa-cogs fa-2x"></span>
          </div>
          <h1 class="display-2 font-weight-bold">404</h1>
          <h3>Oops! Page not Found <span class="fa fa-frown-o"></span></h3>
          <p>
            We could not find the page you were looking for.
            Meanwhile, you may return <a class="text-secondary" href="{{ redirect()->back() }}">back</a> or
          </p>
          <a href="{{ route('iko') }}">
            <button type="button" name="button" class="btn btn-dark btn-lg border-light ">Go Home</button>
          </a>
        </div>
      </div>
    </div>

    <div class="d-block d-lg-none">
      <div class="container text-center">
        <div class="mt-5 pt-5 text-light">
          <div class="display-1">
            <p style="font-size: 4em">404</p>
          <p>Oops! Page not Found <span class="fa fa-frown-o"></span></p>
          <p style="font-size: 0.6em">
            We could not find the page you were looking for.
            Meanwhile, you may return <a class="text-secondary" href="{{ redirect()->back() }}">back</a> or
          </p>
          <br>
          <a href="{{ route('iko') }}">
            <button type="button" name="button" class="btn btn-dark border-light p-5" style="font-size: 1em;">Go Home</button>
          </a>
          </div>
        </div>
    </div>
  </body>
</html>
