<table class="table table-bordered table-striped nowrap">
  <tr>
    <th>Nama Aktivitas</th>
    <th>Potensi Bahaya</th>
    <th>Pengendalian Bahaya</th>
    <th>Penanggung Jawab</th>
    <th>Keterangan</th>
  </tr>
  @foreach ($aktivitasJSA as $dataJSA)
    <?php
      $potensi = explode('","', $dataJSA->potensi_bahaya);
      $pengendalian = explode('","', $dataJSA->pengendalian_bahaya);
      $pj = explode(',', $dataJSA->penanggung_jawab);
      $keterangan = explode(',', $dataJSA->keterangan);
    ?>
    @foreach (explode(',', $dataJSA->aktivitas) as $key => $aktivitas)
      <tr>
        <td>
          {{ str_replace('"', '', $aktivitas) }}
        </td>
        <td>
          <ul style="list-style-position: inside; padding-left: 0">
            @foreach (explode(', ', $potensi[$key]) as $key2 => $potensi_bahaya)
              <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $potensi_bahaya) }}</li>
            @endforeach
          </ul>
        </td>
        <td>
          <ul style="list-style-position: inside; padding-left: 0">
            @foreach (explode(', ', $pengendalian[$key]) as $key3 => $pengendalian_bahaya)
              <li>{{ preg_replace('/[^a-zA-Z 0-9]+/', '', $pengendalian_bahaya) }}</li>
            @endforeach
          </ul>
        </td>
        <td>
          {{ preg_replace('/[^a-zA-Z 0-9]+/', '', $pj[$key]) }}
        </td>
        <td>
          {{ preg_replace('/[^a-zA-Z 0-9]+/', '', $keterangan[$key]) }}
        </td>
      </tr>
    @endforeach
  @endforeach
</table>
