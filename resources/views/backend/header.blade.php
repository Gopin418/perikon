<nav class="main-header navbar navbar-expand bg-info-gradient navbar-light border-bottom" style="z-index:9">
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
          <a href="{{ route('logout') }}"
            class="nav-link text-white"
            onclick="event.preventDefault();
                      document.getElementById('logout-form').submit()">
            <i class="fa fa-sign-out"></i> logout
          </a>
        </li>
    </ul>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
  </nav>