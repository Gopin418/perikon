
<aside class="main-sidebar sidebar-collapse sidebar-dark-info elevation-5" style="overflow:hidden;position:fixed;z-index:10">

        <a href="{{route('dashboard')}}" class="brand-link bg-info-gradient">
          <img class="brand-image img-circle elevation-3" src="{{url('images/AdminLTELogo.png')}}" alt="Logo">
          <span class="brand-text font-weight-light">HSE IKO Admin</span>
        </a>
    <div class="sidebar scroll">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{url('images/'.$profile->image)}}" class="img-circle" alt="User Image">
        </div>
        <div class="info">
          <a href="{{route('profile')}}" class="d-block">{{Auth::user()->firstname . ' ' . Auth::user()->lastname}}</a>
        </div>
      </div>
      <nav class="mt-2">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview">
          <li class="nav-item">
            <a href="{{route('dashboard')}}" class="nav-link {!! Request::is('admin') ? 'active' : '' !!}">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>Dashboard</p>
            </a>
          </li>
          {{-- Contractor request --}}
          <li class="nav-item">
            <a href="{{route('request')}}" class="nav-link {!! Request::is('admin/request') ? 'active' : '' !!}">
              <i class="nav-icon fa fa-pencil-square-o"></i>
              <p>Contractor Request</p>
            </a>
          </li>
          {{-- IKO --}}
          @if( Auth::user()->hasRole('Admin Developer') || Auth::user()->hasRole('Admin Utama'))
          <li class="nav-item has-treeview" id="iko_sidebar">
            <a href="#" class="nav-link {!! Request::is('admin/IKO/*') ? 'active' : '' !!}">
              <i class="nav-icon fa fa-cogs"></i>
              <p>IKO
                @if($IKO_pending == 0)
                  <i class="right fa fa-angle-left"></i>
                @else
                  <i class="right fa fa-angle-bottom"></i>
                  <small class="right badge bg-warning iko iko-badge"><p class="iko-num">{{ $IKO_pending }}</p></small>
                @endif
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('iko-pending')}}" class="nav-link {!! Request::is('admin/IKO/pending') ? 'active' : '' !!}">
                  <i class="nav-icon fa fa-clock-o"></i>
                  <p>Pending
                  @if($IKO_pending == 0)

                  @else
                    <small class="badge right bg-warning iko">{{ $IKO_pending }}</small>
                  @endif
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('iko-approved')}}" class="nav-link {!! Request::is('admin/IKO/approved') ? 'active' : '' !!}">
                  <i class="nav-icon fa fa-check-square-o"></i>
                  <p>Approved</p>
                </a>
              </li>
            </ul>
          </li>
          @elseif(Auth::user()->hasRole('Admin Biasa') )
          <li class="nav-item">
            <a href="{{route('iko-approved')}}" class="nav-link {!! Request::is('admin/IKO/*') ? 'active' : '' !!}">
              <i class="nav-icon fa fa-cogs"></i>
              <p>IKO</p>
            </a>
          </li>
          @endif

          {{-- JSA --}}
          @if( Auth::user()->hasRole('Admin Developer') || Auth::user()->hasRole('Admin Utama'))
          <li class="nav-item has-treeview" id="jsa_sidebar">
            <a href="#" class="nav-link {!! Request::is('admin/JSA/*') ? 'active' : ''!!}">
              <i class="nav-icon fa fa-plus"></i>
              <p>JSA
              @if($JSA_pending == 0)
                <i class="right fa fa-angle-left"></i>
              @else
                <small class="badge right bg-warning jsa-badge"><p class="jsa-num">{{ $JSA_pending }}</p></small>
              @endif
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('jsa-pending')}}" class="nav-link {!! Request::is('admin/JSA/pending') ? 'active' : '' !!}">
                  <i class="nav-icon fa fa-clock-o"></i>
                  <p>Pending
                  @if($JSA_pending == 0)

                  @else
                    <small class="badge right bg-warning jsa">{{ $JSA_pending }}</small>
                  @endif
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('jsa-approved')}}" class="nav-link {!! Request::is('admin/JSA/approved') ? 'active' : '' !!}">
                  <i class="nav-icon fa fa-check-square-o"></i>
                  <p>Approved</p>
                </a>
              </li>
            </ul>
          </li>
          @elseif(Auth::user()->hasRole('Admin Biasa'))
          <li class="nav-item">
            <a href="{{route('jsa-approved')}}" class="nav-link {!! Request::is('admin/JSA/*') ? 'active' : '' !!}">
              <i class="nav-icon fa fa-plus"></i>
              <p>JSA</p>
            </a>
          </li>
          @endif

          {{-- ADL --}}
          @if( Auth::user()->hasRole('Admin Developer') || Auth::user()->hasRole('Admin Utama'))
          <li class="nav-item has-treeview" id="adl_sidebar">
            <a href="#" class="nav-link {!! Request::is('admin/ADL/*') ? 'active' : '' !!}">
              <i class="nav-icon fa fa-envira"></i>
              <p>ADL
              @if($ADL_pending == 0)
                <i class="right fa fa-angle-left"></i>
              @else
                <small class="badge right bg-warning adl-badge"><p class="adl-num">{{ $ADL_pending }}</p></small>
              @endif
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adl-pending')}}" class="nav-link {!! Request::is('admin/ADL/pending') ? 'active' : '' !!}">
                  <i class="nav-icon fa fa-clock-o"></i>
                  <p>Pending
                  @if($ADL_pending == 0)

                  @else
                    <small class="badge right bg-warning adl">{{ $ADL_pending }}</small>
                  @endif
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adl-approved')}}" class="nav-link {!! Request::is('admin/ADL/approved') ? 'active' : '' !!}">
                  <i class="nav-icon fa fa-check-square-o"></i>
                  <p>Approved</p>
                </a>
              </li>
            </ul>
          </li>
          @elseif(Auth::user()->hasRole('Admin Biasa'))
          <li class="nav-item">
            <a href="{{route('adl-approved')}}" class="nav-link {!! Request::is('admin/ADL/*') ? 'active' : '' !!}">
              <i class="nav-icon fa fa-envira"></i>
              <p>ADL</p>
            </a>
          </li>
          @endif

          {{-- IPB --}}
          @if(Auth::user()->hasRole('Admin Developer') || Auth::user()->hasRole('Admin Utama'))
          <li class="nav-item has-treeview" id="ipb_sidebar">
            <a href="#" class="nav-link {!! Request::is('admin/IPB/*') ? 'active' : '' !!}">
              <i class="nav-icon fa fa-fire"></i>
              <p>IPB
              @if( $IPB_pending == 0)
                <i class="right fa fa-angle-left"></i>
              @else
                <small class="badge right bg-warning ipb-badge"><p class="ipb-num">{{ $IPB_pending }}</p></small>
              @endif
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('ipb-pending')}}" class="nav-link {!! Request::is('admin/IPB/pending') ? 'active' : '' !!}">
                  <i class="nav-icon fa fa-clock-o"></i>
                  <p>Pending
                  @if($IPB_pending == 0)
                    <small></small>
                  @else
                    <small class="badge right bg-warning ipb">{{ $IPB_pending }}</small>
                  @endif
                  </p>
                </a>
              </li>
            <li class="nav-item">
              <a href="{{route('ipb-approved')}}" class="nav-link {!! Request::is('admin/IPB/approved') ? 'active' : '' !!}">
                <i class="nav-icon fa fa-check-square-o"></i>
                <p>Approved</p>
              </a>
            </li>
          </ul>
        </li>
        @elseif(Auth::user()->hasRole('Admin Biasa'))
        <li class="nav-item">
          <a href="{{route('ipb-approved')}}" class="nav-link {!! Request::is('admin/IPB/*') ? 'active' : '' !!}">
            <i class="nav-icon fa fa-fire"></i>
            <p>IPB</p>
          </a>
        </li>
        @endif

        {{-- Other Navigation --}}
        <li class="nav-item">
          <a href="{{route('pj-utility')}}" class="nav-link {!! Request::is('admin/utility') ? 'active' : '' !!}">
            <i class="nav-icon fa fa-wrench"></i>
            <p>Approval Utility</p>
          </a>
        </li>
        {{-- User Navigation --}}
        @if( Auth::user()->hasRole('Admin Developer') || Auth::user()->hasRole('Admin Utama') )
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link {!! Request::is('admin/add') || Request::is('admin/list') || Request::is('admin/role') ? 'active' : '' !!}">
            <i class="nav-icon fa fa-users"></i>
            <p>User Management
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{route('user-list')}}" class="nav-link {!! Request::is('admin/list') ? 'active' : '' !!}">
                <i class="nav-icon fa fa-user"></i>
                <p>Users</p>
              </a>
            </li>
            @if(Auth::user()->hasRole('Admin Developer'))
            <li class="nav-item">
              <a href="{{route('role')}}" class="nav-link {!! Request::is('admin/role') ? 'active' : '' !!}">
                <i class="nav-icon fa fa-briefcase"></i>
                <p>Roles</p>
              </a>
            </li>
            @else

            @endif
            <li class="nav-item">
              <a href="{{route('add-user')}}" class="nav-link {!! Request::is('admin/add') ? 'active' : '' !!}">
                <i class="nav-icon fa fa-user-plus"></i>
                <p>Add User</p>
              </a>
            </li>
          </ul>
        </li>
        @elseif(Auth::user()->hasRole('Admin Biasa'))
        <li class="nav-item">
          <a href="{{route('user-list')}}" class="nav-link">
            <i class="nav-icon fa fa-users"></i>
            <p>Users</p>
          </a>
        </li>
        @endif
      </ul>
    </nav>
  </div>
</aside>
@section('script')
<script>
  $(document).ready(function() {
    $('.button-collapse').sideNav();

    var sideNavScrollbar = document.querySelector('.custom.scrollbar');
    Ps.initialize(sideNavScrollbar);
  })
</script>
@endsection
