<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Perikon</title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/mdb.min.css">
    <link rel="stylesheet" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/master.css">
    <link rel="stylesheet" href="/css/datepicker.css">
     <link href="https://fonts.googleapis.com/css?family=Roboto:100,300" rel="stylesheet">
    <?php
  $bg = array('bg-01.jpg', 'bg-02.jpg', 'bg-03.jpg', 'bg-04.jpg', 'bg-05.jpg', 'bg-06.jpg', 'bg-07.jpg' ); // array of filenames

  $i = rand(0, count($bg)-1); // generate random number size of the array
  $selectedBg = "$bg[$i]"; // set variable equal to which random filename was chosen
?>
<style type="text/css">
body{
background: url(images/<?php echo $selectedBg; ?>) no-repeat;
background-size: cover;
background-position: center bottom;
}
</style>
  </head>
  <body>
      <div class="mask flex-center rgba-black-strong">
          <div class="container text-white">
            <h1 class="display-3 flex-center">Perikon</h1>
            <p class="flex-center">Enter your email and code below to start working</p>
            <div class="row">
              <div class="col">

              </div>
              <div class="col-12 col-sm-10 col-md-8 col-lg-6">
                <form action="{{ route('start') }}" method="post">
                  {{ csrf_field() }}
                  <div class="md-form">
                    <i class="fa fa-building prefix"></i>
                    <input type="text" name="nama_kontraktor" id="nama_kontraktor" class="form-control form-control-lg text-white" autocomplete="off" required>
                    <label for="nama_kontraktor">Nama Kontraktor</label>
                  </div>
                  <div class="md-form">
                    <i class="fa fa-envelope prefix"></i>
                    <input type="email" name="email" id="email" class="form-control form-control-lg text-white" autocomplete="off" required>
                    <label for="email">Email</label>
                  </div>
                  <div class="md-form">
                    <i class="fa fa-wrench prefix"></i>
                    <input type="text" name="nama_pekerjaan" id="nama_pekerjaan" class="form-control form-control-lg text-white" autocomplete="off" required>
                    <label for="nama_pekerjaan">Nama Pekerjaan</label>
                  </div>
                  <div class="row text-center">
                    <div class="col">
                      <div class="md-form primary text-center">
                          <input type="hidden" name="tanggal_mulai" id="tgl_mulai">
                          <input type="text" name="tgl_mulai" id="tanggal_mulai" class="form-control text-white" style="cursor: pointer;"
                            readonly>
                          <label for="tanggal_mulai">Tanggal Mulai</label>
                      </div>
                    </div>
                    <div class="col-2">
                     <h3 class="mt-3">sd.</h3>
                    </div>
                    <div class="col">
                      <div class="md-form primary text-center">
                          <input type="hidden" name="tanggal_selesai" id="tgl_selesai">
                          <input type="text" name="tgl_selesai" id="tanggal_selesai" class="form-control text-white" style="cursor: pointer;"
                            readonly>
                          <label for="tanggal_selesai">Tanggal Selesai</label>
                      </div>
                    </div>
                  </div>
                  <button type="submit" id="submit" class="btn btn-primary waves-effect btn-block" name="button">Submit</button>
                </form>
              </div>
              <div class="col">

              </div>
            </div>
          </div>
      </div>
  </body>
  <script src="{{ asset('js/jquery.js') }}" charset="utf-8"></script>
  <script src="{{ asset('/js/jquery-ui.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/mdb.min.js') }}" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#tanggal_mulai').datepicker({
          todayHighlight: true,
          showAnim: "fadeIn",
          dateFormat: "DD, d MM yy",
          altField: "#tgl_mulai",
          altFormat: "yy-mm-dd",
          dayNamesMin: [
              "Min",
              "Sen",
              "Sel",
              "Rab",
              "Kam",
              "Jum",
              "Sab"
          ],
          dayNames: [
              "Minggu",
              "Senin",
              "Selasa",
              "Rabu",
              "Kamis",
              "Jum'at",
              "Sabtu"
          ],
          monthNames: [
              "Januari",
              "Februari",
              "Maret",
              "April",
              "Mei",
              "Juni",
              "Juli",
              "Agustus",
              "September",
              "Oktober",
              "November",
              "Desember"
          ],
          monthNamesShort: [
              "Jan",
              "Feb",
              "Mar",
              "Apr",
              "Mei",
              "Jun",
              "Jul",
              "Agu",
              "Sep",
              "Okt",
              "Nov",
              "Des"
          ],
          "firstDay": 1
      });

      $('#tanggal_selesai').datepicker({
          todayHighlight: true,
          showAnim: "fadeIn",
          dateFormat: "DD, d MM yy",
          altField: "#tgl_selesai",
          altFormat: "yy-mm-dd",
          dayNamesMin: [
              "Min",
              "Sen",
              "Sel",
              "Rab",
              "Kam",
              "Jum",
              "Sab"
          ],
          dayNames: [
              "Minggu",
              "Senin",
              "Selasa",
              "Rabu",
              "Kamis",
              "Jum'at",
              "Sabtu"
          ],
          monthNames: [
              "Januari",
              "Februari",
              "Maret",
              "April",
              "Mei",
              "Juni",
              "Juli",
              "Agustus",
              "September",
              "Oktober",
              "November",
              "Desember"
          ],
          monthNamesShort: [
              "Jan",
              "Feb",
              "Mar",
              "Apr",
              "Mei",
              "Jun",
              "Jul",
              "Agu",
              "Sep",
              "Okt",
              "Nov",
              "Des"
          ],
          "firstDay": 1
      });


    });
  </script>
</html>
