<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use File;
use Auth;
use App\IPB_Status;
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\departement;
use App\profile;

class profileController extends Controller
{

    public function index(){
      if(Auth::guest()) {
        return redirect()->route('404');
      }
      $profile = profile::where('user_id', Auth::user()->id)->first();
      $departement = departement::get();

      // Sidebar
      $IKO_pending = IKO_Status::where('status', 'Pending')->count();
      $JSA_pending = JSA_Status::where('status', 'Pending')->count();
      $ADL_pending = ADL_Status::where('status', 'Pending')->count();
      $IPB_pending = IPB_Status::where('status', 'Pending')->count();

      return view('admin/profile', [
        'profile' => $profile,
        'departement' => $departement,
        'IKO_pending' => $IKO_pending,
        'JSA_pending' => $JSA_pending,
        'ADL_pending' => $ADL_pending,
        'IPB_pending' => $IPB_pending
      ]);
    }

    public function update(Request $request) {
      $image = $request->file('image');
      $extension = $image->getClientOriginalExtension();
      Storage::disk('public')->put($image->getFilename().'.'.$extension, File::get($image));

      $profile = profile::where('user_id', Auth::user()->id)->first();
      $profile->image = $image->getfilename().'.'.$extension;
      $profile->mime = $image->getClientmimeType();
      $profile->original_filename = $image->getClientOriginalName();
      $profile->save();

      return redirect()->back()->with('success', 'Image Uploaded');
    }

    public function updateBg(Request $request) {
      $image = $request->file('bg');
      $extension = $image->getClientOriginalExtension();
      Storage::disk('public')->put($image->getFilename().'.'.$extension, File::get($image));

      $profile = profile::where('user_id', Auth::user()->id)->first();
      $profile->bg = $image->getfilename().'.'.$extension;
      $profile->bg_mime = $image->getClientmimeType();
      $profile->bg_original_filename = $image->getClientOriginalName();
      $profile->save();

      return redirect()->back()->with('success', 'Image Uploaded');
    }
}
