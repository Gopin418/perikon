<?php

namespace App\Http\Controllers;

//Library
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

// Export
use App\Exports\DataExport;

//Model
use App\pengantar;
use App\JSA;
use App\ADL;
use App\IPB;
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\profile;
use App\ProjectStatus;

class IKO extends Controller
{

    /**
     * Export to excel
     * 
     * @return void
     */
    public function export($type){
        return Excel::download(new DataExport, 'IKO.' . $type);
    }

    public function pending(Request $request){
        if(Auth::guest()){
            return redirect()->route('404');
        }
        $request->user()->authorizeRoles(['Admin Developer', 'Admin Utama']);

        // join data utama IKO
        $data = pengantar::join('kontraktor','pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('gmp', 'pengantar.id', '=', 'gmp.id')
        ->join('catatan', 'pengantar.id', '=', 'catatan.id')
        ->join('nutrifood', 'pengantar.id', '=', 'nutrifood.id')
        ->join('iko_status' ,'pengantar.id', '=', 'iko_status.id')
        ->where('status', '=', 'Pending')
        ->orderBy('pengantar.id')
        ->get();

        // sidebar
        $jumlah = Pengantar::count();
        $ADL_pending = ADL_Status::where('status', 'pending')->count();
        $JSA_pending = JSA_Status::where('status', 'pending')->count();
        $IKO_pending = IKO_Status::where('status', 'pending')->count();
        $IPB_pending = IPB_Status::where('status', 'pending')->count();
        $profile = profile::where('user_id', Auth::user()->id)->first();

        return view('admin/IKO/iko-pending', [
            'profile' => $profile,
            'ADL_pending' => $ADL_pending,
            'JSA_pending' => $JSA_pending,
            'IKO_pending' => $IKO_pending,
            'IPB_pending' => $IPB_pending,
            'data' => $data,
            'jumlah' => $jumlah
        ]);
    }

    public function approved(){
        if(Auth::guest()){
            return redirect()->route('404');
        }
        // join data utama IKO
        $data = pengantar::join('kontraktor','pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('gmp', 'pengantar.id', '=', 'gmp.id')
        ->join('catatan', 'pengantar.id', '=', 'catatan.id')
        ->join('nutrifood', 'pengantar.id', '=', 'nutrifood.id')
        ->join('iko_status' ,'pengantar.id', '=', 'iko_status.id')
        ->where('status', '=', 'Approved')
        ->orderBy('pengantar.id')
        ->get();

        // sidebar
        $jumlah = Pengantar::count();
        $ADL_pending = ADL_Status::where('status', 'pending')->count();
        $JSA_pending = JSA_Status::where('status', 'pending')->count();
        $IKO_pending = IKO_Status::where('status', 'pending')->count();
        $IPB_pending = IPB_Status::where('status', 'pending')->count();

        $IKO_approved = IKO_Status::where('status', 'approved')->count();

        $profile = profile::where('user_id', Auth::user()->id)->first();

        return view('admin/IKO/iko-approved', [
            'profile' => $profile,
            'IKO_approved' => $IKO_approved,
            'ADL_pending' => $ADL_pending,
            'JSA_pending' => $JSA_pending,
            'IKO_pending' => $IKO_pending,
            'IPB_pending' => $IPB_pending,
            'data' => $data,
            'jumlah' => $jumlah
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $iko_status = IKO_Status::where('id' , '=', $id)->first();
    $iko_status->status = 'Approved';
    $iko_status->save();

    $no_IKO = IKO_Status::join('pengantar', 'iko_status.id', '=', 'pengantar.id')
    ->where([
        ['iko_status.id', $id],
        ['status', 'Approved']
    ])
    ->pluck('no_iko')
    ->first();
    
    $projectStatus = ProjectStatus::where('no_iko', $no_IKO)->first();
    $projectStatus->iko_status = 'Approved';

    if(empty($projectStatus['no_ipb']) 
    && $projectStatus['iko_status'] == 'Approved'
    && $projectStatus['jsa_status'] == 'Approved'
    && $projectStatus['adl_status'] == 'Approved'
    ){
        $projectStatus->project_status = 'Approved';
        if($projectStatus['tanggal_mulai'] > now()->toDateString()){
            $projectStatus->status = 'Upcoming';
        } else if($projectStatus['tanggal_mulai'] <= now()->toDateString() && $projectStatus['tanggal_selesai'] >= now()->toDateString()){
            $projectStatus->status = 'Ongoing';
        } else if($projectStatus['tanggal_selesai'] < now()->toDateString()){
            $projectStatus->status = 'Finished';
        }
    }

    $projectStatus->save();
        

    return redirect()->back();

    }


}
