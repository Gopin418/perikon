<?php

namespace App\Http\Controllers;

//Library
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

//Model
use App\pengantar;
use App\JSA;
use App\ADL;
use App\ipb;
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\profile;
use App\ProjectStatus;
use App\utility;
use App\contractor;
use App\Code;

// Mail
use App\Mail\HoldMail;

// Events
use App\Events\DataUpdate;

class AdminController extends Controller
{
    /**
     * data table limit
     *
     * @var int
     */
    protected $data_page = 3;

    /**
     * Show upcoming data table
     *
     * @var $data_page
     */
    public function upcoming()
    {
        $projectStatus = projectStatus::select('ipb_status', 'tanggal_mulai', 'status')->get();

        $upcoming = pengantar::join('kontraktor', 'pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('gmp', 'pengantar.id', '=', 'gmp.id')
        ->join('nutrifood', 'pengantar.id', '=', 'nutrifood.id')
        ->join('catatan', 'pengantar.id', '=', 'catatan.id')
        ->join('project_status', 'pengantar.no_iko', '=', 'project_status.no_iko')
        ->select(
          'pengantar.id',
          'nama_kontraktor',
          'nama_pekerjaan',
          'jenis',
          'tgl_mulai',
          'tgl_selesai',
          'project_status.tanggal_mulai',
          'project_status.tanggal_selesai',
          'status',
          'project_status',
          'project_status.no_iko',
          'iko_status',
          'jsa_status',
          'adl_status',
          'ipb_status'
        )
        ->orderBy('pengantar.id')
      ->paginate($this->data_page, ['*'], 'dataProject');

        $upcomingData = $upcoming->count();

        foreach ($upcoming as $key => $value) {
            if ($value['tanggal_mulai'] > now()->toDateString() && $value->project_status == 'Approved') {
                $updateStatus = projectStatus::where('no_iko', $value->no_iko)->first();
                $updateStatus->status = 'Upcoming';
                $updateStatus->save();
            }
            if ($value['tanggal_mulai'] <= now()->toDateString() && $value->project_status == 'Approved') {
                $updateStatus = projectStatus::where('no_iko', $value->no_iko)->first();
                $updateStatus->status = 'Ongoing';
                $updateStatus->save();
            }
            if ($value['tanggal_selesai'] < now()->ToDateString() && $value->project_status == 'Approved') {
                $updateStatus = projectStatus::where('no_iko', $value->no_iko)->first();
                $updateStatus->status = 'Finished';
                $updateStatus->save();
            }
        }

        return view('admin/dashboard/upcoming', [
        'upcoming' => $upcoming,
        'upcomingData' => $upcomingData
      ]);
    }

    /**
     * Show ongoing data
     *
     * @return void
     */
    public function ongoing()
    {
        $projectStatus = projectStatus::where('status', 'ongoing')->select('ipb_status', 'tanggal_mulai', 'status')->get();

        $ongoing = pengantar::join('kontraktor', 'pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('gmp', 'pengantar.id', '=', 'gmp.id')
        ->join('nutrifood', 'pengantar.id', '=', 'nutrifood.id')
        ->join('catatan', 'pengantar.id', '=', 'catatan.id')
        ->join('project_status', 'pengantar.id', '=', 'project_status.id')
        ->select(
          'pengantar.id',
          'nama_kontraktor',
          'nama_pekerjaan',
          'jenis',
          'tgl_mulai',
          'tgl_selesai',
          'project_status.tanggal_mulai',
          'project_status.tanggal_selesai'
        )
        ->where('status', 'Ongoing')
        ->orderBy('pengantar.id')
        ->paginate($this->data_page, ['*'], 'ongpingShow');

        $projectStartIPB = projectStatus::whereNotNull('ipb_status')
      ->where([
        ['iko_status', 'Approved'],
        ['jsa_status', 'Approved'],
        ['adl_status', 'Approved'],
        ['ipb_status', 'Approved'],
        ['status', 'Ongoing']
      ])
      ->select('no_iko', 'tanggal_mulai', 'tanggal_selesai')
      ->get();
        $projectStart = projectStatus::where([
        ['iko_status', 'Approved'],
        ['jsa_status', 'Approved'],
        ['adl_status', 'Approved'],
        ['status', 'Ongoing']
      ])
      ->select('tanggal_mulai', 'tanggal_selesai')
      ->first();

        foreach ($ongoing as $key => $value) {
            $start = strtotime($value->tanggal_mulai);
            $end = strtotime($value->tanggal_selesai) + 1;
            $current = strtotime(date('y-m-d')) + 1;
            $projectRange[$key] = (($current - $start) / ($end - $start)) * 100;
        }

        return view('admin/dashboard/ongoing', [
        'ongoing' => $ongoing,
        'projectRange' => $projectRange
      ]);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     * @return \Illuminate\http\Request
     */
    public function index(Request $request)
    {
        if (Auth::guest()) {
            return redirect()->route('404');
        }

        $upcoming = pengantar::join('kontraktor', 'pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('gmp', 'pengantar.id', '=', 'gmp.id')
        ->join('nutrifood', 'pengantar.id', '=', 'nutrifood.id')
        ->join('catatan', 'pengantar.id', '=', 'catatan.id')
        ->join('project_status', 'pengantar.no_iko', '=', 'project_status.no_iko')
        ->select(
          'pengantar.id',
          'nama_kontraktor',
          'nama_pekerjaan',
          'jenis',
          'tgl_mulai',
          'tgl_selesai',
          'project_status.tanggal_mulai',
          'project_status.tanggal_selesai',
          'status',
          'project_status',
          'project_status.no_iko',
          'iko_status',
          'jsa_status',
          'adl_status',
          'ipb_status'
        )
        ->orderBy('pengantar.id')
        ->paginate($this->data_page,['*'], 'upcomingData');
        $upcomingData = $upcoming->count();

        $ongoing = pengantar::join('kontraktor', 'pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('gmp', 'pengantar.id', '=', 'gmp.id')
        ->join('nutrifood', 'pengantar.id', '=', 'nutrifood.id')
        ->join('catatan', 'pengantar.id', '=', 'catatan.id')
        ->join('project_status', 'pengantar.id', '=', 'project_status.id')
        ->select(
          'pengantar.id',
          'nama_kontraktor',
          'nama_pekerjaan',
          'jenis',
          'tgl_mulai',
          'tgl_selesai',
          'status'
        )
        ->orderBy('pengantar.id')
        ->get();
        $projectStartIPB = projectStatus::whereNotNull('ipb_status')
      ->where([
        ['iko_status', 'Approved'],
        ['jsa_status', 'Approved'],
        ['adl_status', 'Approved'],
        ['ipb_status', 'Approved'],
        ['status', 'Ongoing']
      ])
      ->select('no_iko', 'tanggal_mulai', 'tanggal_selesai')
      ->get();
        $projectStart = projectStatus::where([
        ['iko_status', 'Approved'],
        ['jsa_status', 'Approved'],
        ['adl_status', 'Approved'],
        ['status', 'Ongoing']
      ])
      ->select('tanggal_mulai', 'tanggal_selesai')
      ->first();

      $projectRange[] = 0;

        foreach ($ongoing as $key => $value) {
            $start = strtotime($value->tanggal_mulai);
            $end = strtotime($value->tanggal_selesai) + 1;
            $current = strtotime(date('y-m-d'));
            $projectRange[$key] = (($current - $start) / ($end - $start)) * 100;
        }


        $utility = utility::all();

        $project = pengantar::join('iko_status', 'pengantar.id', '=', 'iko_status.id')
        ->where([
          ['jenis', 'Project'],
          ['status', 'Approved']
        ])
        ->count();
        $maintenance = pengantar::join('iko_status', 'pengantar.id', '=', 'iko_status.id')
        ->where([
          ['jenis', 'Maintenance'],
          ['status', 'Approved']
        ])
        ->count();

        $ongoingData = $ongoing->where('status', 'Ongoing')->count();

        $profile = profile::where('user_id', Auth::user()->id)->first();

        $projectStatusUpcomingIPB = projectStatus::whereNotNull('ipb_status')
        ->where([
          ['iko_status', 'Approved'],
          ['jsa_status', 'Approved'],
          ['adl_status', 'Approved'],
          ['ipb_status', 'Approved'],
          ['status' , 'Upcoming']
        ])
        ->count();

        $projectStatusUpcoming = projectStatus::where([
          ['iko_status', 'Approved'],
          ['jsa_status', 'Approved'],
          ['adl_status', 'Approved'],
          ['status' , 'Upcoming']
        ])
        ->count();

        $projectStatusOngoingIPB = projectStatus::whereNotNull('ipb_status')
        ->where([
          ['iko_status', 'Approved'],
          ['jsa_status', 'Approved'],
          ['adl_status', 'Approved'],
          ['ipb_status', 'Approved'],
          ['status' , 'Ongoing']
        ])
        ->count();

        $projectStatusOngoing = projectStatus::where([
          ['iko_status', 'Approved'],
          ['jsa_status', 'Approved'],
          ['adl_status', 'Approved'],
          ['status' , 'Ongoing']
        ])
        ->count();

        $projectStatusFinishedIPB = projectStatus::whereNotNull('ipb_status')
        ->where([
          ['iko_status', 'Approved'],
          ['jsa_status', 'Approved'],
          ['adl_status', 'Approved'],
          ['ipb_status', 'Approved'],
          ['status' , 'Finished']
        ])
        ->count();

        $projectStatusFinished = projectStatus::where([
          ['iko_status', 'Approved'],
          ['jsa_status', 'Approved'],
          ['adl_status', 'Approved'],
          ['status' , 'Finished']
        ])
        ->count();

        // label approved
        $IKO_approved = IKO_Status::where('status', 'Approved')->count();
        $JSA_approved = JSA_Status::where('status', 'Approved')->count();
        $ADL_approved = ADL_Status::where('status', 'Approved')->count();
        $IPB_approved = IPB_Status::where('status', 'Approved')->count();

        // label pending
        $ADL_pending = ADL_status::where('status', 'Pending')->count();
        $JSA_pending = JSA_Status::where('status', 'Pending')->count();
        $IKO_pending = IKO_Status::where('status', 'Pending')->count();
        $IPB_pending = IPB_Status::where('status', 'pending')->count();


        // label count (sidebar)
        $jumlahADL = ADL::count();
        $jumlahJSA = JSA::count();
        $jumlahIPB = ipb::count();
        $jumlah = Pengantar::count();

        // count status IKO

        $data = ProjectStatus::where('status', 'Pending')->get();

        foreach ($data as $key => $value) {
          $dataStatus = ProjectStatus::where('status', 'Pending')->first();
            if ($value['no_ipb'] == null) {
                if ($value['iko_status ']== 'Approved'
            &&  $value['jsa_status ']== 'Approved'
            &&  $value['adl_status ']== 'Approved'
            ) {
                    $value['project_status'] = 'Approved';
                    if ($value['tanggal_mulai'] > now()->toDateString()) {
                        $value['status'] = 'Upcoming';
                    } elseif ($value['tanggal_mulai'] <= now()->toDateString() && $value['tanggal_selesai'] >= now()->toDateString()) {
                        $value['status'] = 'Ongoing';
                    } elseif ($value['tanggal_selesai'] < now()->toDateString()) {
                        $value['status'] = 'Finished';
                    }
                }
            }

            if ($value['no_ipb'] != null) {
                if ($value['iko_status'] == 'Approved'
                          &&  $value['jsa_status'] == 'Approved'
                          &&  $value['adl_status'] == 'Approved'
                          &&  $value['ipb_status'] == 'Approved'
                          ) {
                    $value['project_status'] = 'Approved';
                    if ($value['tanggal_mulai'] > now()->toDateString()) {
                        $dataStatus->status = 'Upcoming';
                    } elseif ($value['tanggal_mulai'] <= now()->toDateString() && $value['tanggal_selesai'] >= now()->toDateString()) {
                        $dataStatus->status = 'Ongoing';
                    } elseif ($value['tanggal_selesai'] < now()->toDateString()) {
                        $dataStatus->status = 'Finished';
                    }
                }
            }
            $dataStatus->save();
        }






        return view('admin/dashboard', [
            'upcoming' => $upcoming,
            'upcomingData' => $upcomingData,
            'ongoing' => $ongoing,
            'projectRange' => $projectRange,
            'projectStatusUpcoming' => $projectStatusUpcoming,
            'projectStatusUpcomingIPB' => $projectStatusUpcoming,
            'projectStatusOngoing' => $projectStatusOngoing,
            'projectStatusOngoingIPB' => $projectStatusOngoingIPB,
            'projectStatusFinished' => $projectStatusFinished,
            'projectStatusFinishedIPB' => $projectStatusFinishedIPB,
            'profile' => $profile,
            'IKO_approved' => $IKO_approved,
            'JSA_approved' => $JSA_approved,
            'ADL_approved' => $ADL_approved,
            'IPB_approved' => $IPB_approved,
            'IKO_pending' => $IKO_pending,
            'JSA_pending' => $JSA_pending,
            'ADL_pending' => $ADL_pending,
            'IPB_pending' => $IPB_pending,
            'jumlahADL' => $jumlahADL,
            'jumlahJSA' => $jumlahJSA,
            'jumlahIPB' => $jumlahIPB,
            'jumlah' => $jumlah,
            'ongoingData' => $ongoingData,
            'project' => $project,
            'maintenance' => $maintenance,
            'utility' => $utility
        ]);
    }

    public function request(Request $request) {
      if (Auth::guest()) {
          return redirect()->route('404');
      }

      // contractor data
      $contractor = contractor::where('status', 'Pending')->orderBy('created_at', 'asc')->get();

      // Profile data
      $profile = profile::where('user_id', Auth::user()->id)->first();

      // label approved
      $IKO_approved = IKO_Status::where('status', 'Approved')->count();
      $JSA_approved = JSA_Status::where('status', 'Approved')->count();
      $ADL_approved = ADL_Status::where('status', 'Approved')->count();
      $IPB_approved = IPB_Status::where('status', 'Approved')->count();

      // label pending
      $ADL_pending = ADL_status::where('status', 'Pending')->count();
      $JSA_pending = JSA_Status::where('status', 'Pending')->count();
      $IKO_pending = IKO_Status::where('status', 'Pending')->count();
      $IPB_pending = IPB_Status::where('status', 'pending')->count();


      // label count (sidebar)
      $jumlahADL = ADL::count();
      $jumlahJSA = JSA::count();
      $jumlahIPB = ipb::count();
      $jumlah = Pengantar::count();


      $pass = [
        'contractor' => $contractor,
        'profile' => $profile,
        'IKO_approved' => $IKO_approved,
        'JSA_approved' => $JSA_approved,
        'ADL_approved' => $ADL_approved,
        'IPB_approved' => $IPB_approved,
        'IKO_pending' => $IKO_pending,
        'JSA_pending' => $JSA_pending,
        'ADL_pending' => $ADL_pending,
        'IPB_pending' => $IPB_pending,
        'jumlahADL' => $jumlahADL,
        'jumlahJSA' => $jumlahJSA,
        'jumlahIPB' => $jumlahIPB,
        'jumlah' => $jumlah,
      ];

      return view('admin.request', $pass);
    }

    public function requestApprove($id){
      $contractor = contractor::where('id', $id)->first();
      $contractor->status = 'Approved';
      Mail::to($contractor->email)->send(new HoldMail());
      $contractor->save();
      return back();
    }

    public function requestDecline($id){
      contractor::find($id)->delete();
      Code::where('user_id', $id)->delete();
      return back();
    }
}
