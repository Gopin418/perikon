<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;

use App\User;

class ExportController extends Controller
{

    /**
     * Collection
     */
    public function collection(){
        return User::all();
    }

    /**
     * Export data IKO
     */
    public function exportIKO() {
        return Excel::download(new UserExport, 'users.xlsx');
    }
}
