<?php

namespace App\Http\Controllers;

//Library
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

//Model
use App\pengantar;
use App\JSA;
use App\ADL;
use App\IPB;
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\profile;
use App\ProjectStatus;

class ADLController extends Controller
{
    public function pending(Request $request) {
        if(Auth::guest()){
            return redirect()->route('404');
        }
        $request->user()->authorizeRoles(['Admin Developer', 'Admin Utama']);

        $adl = pengantar::join('kontraktor', 'pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('adl', 'pengantar.id', '=', 'adl.id')
        ->join('adl_status', 'pengantar.id', '=', 'adl_status.id')
        ->where('status', '=', 'pending')
        ->get();

        $aktivitasADL = DB::table('adl')
        ->join('pengantar', 'adl.id', '=', 'pengantar.id')
        ->join('adl_status', 'adl.id', '=', 'adl_status.id')
        ->where('status', '=', 'pending')
        ->get();


        $jumlah      = ADL::count();
        $ADL_pending = ADL_Status::where('status', 'pending')->count();
        $JSA_pending = JSA_Status::where('status', 'pending')->count();
        $IKO_pending = IKO_Status::where('status', 'pending')->count();
        $IPB_pending = IPB_Status::where('status', 'pending')->count();

        $profile = profile::where('user_id', Auth::user()->id)->first();

        return view('admin/ADL/adl-pending', [
            'profile' => $profile,
            'jumlah'      => $jumlah,
            'ADL_pending' => $ADL_pending,
            'JSA_pending' => $JSA_pending,
            'IKO_pending' => $IKO_pending,
            'IPB_pending' => $IPB_pending,
            'adl' => $adl,
            'aktivitasADL' => $aktivitasADL
        ]);
    }

    public function approved() {
        if(Auth::guest()){
            return redirect()->route('404');
        }
        $adl = pengantar::join('kontraktor', 'pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('adl', 'pengantar.id', '=', 'adl.id')
        ->join('adl_status', 'pengantar.id', '=', 'adl_status.id')
        ->where('status', '=', 'Approved')
        ->get();

        $aktivitasADL = DB::table('adl')
        ->join('pengantar', 'adl.id', '=', 'pengantar.id')
        ->join('adl_status', 'adl.id', '=', 'adl_status.id')
        ->where('status', '=', 'approved')
        ->get();

        $jumlah      = ADL::count();
        $ADL_pending = ADL_Status::where('status', 'pending')->count();
        $JSA_pending = JSA_Status::where('status', 'pending')->count();
        $IKO_pending = IKO_Status::where('status', 'pending')->count();
        $IPB_pending = IPB_Status::where('status', 'pending')->count();
        $ADL_approved = ADL_Status::where('status', 'approved')->count();

        $profile = profile::where('user_id', Auth::user()->id)->first();

        return view('admin/ADL/adl-approved', [
            'profile' => $profile,
            'ADL_approved' => $ADL_approved,
            'jumlah'      => $jumlah,
            'ADL_pending' => $ADL_pending,
            'JSA_pending' => $JSA_pending,
            'IKO_pending' => $IKO_pending,
            'IPB_pending' => $IPB_pending,
            'adl' => $adl,
            'aktivitasADL' => $aktivitasADL
        ]);
    }

    public function update(Request $request, $id) {
    $adl_status = ADL_Status::where('id' , '=', $id)->first();
    $adl_status->status = 'Approved';
    $adl_status->save();

    $no_adl = ADL_Status::join('adl', 'adl_status.id', '=', 'adl.id')
    ->where([
        ['adl_status.id', $id],
        ['status', 'Approved']
    ])
    ->pluck('no_adl')
    ->first();

    $projectStatus = projectStatus::where('no_adl', $no_adl)->first();
    $projectStatus->adl_status = 'Approved';

    if(empty($projectStatus['no_ipb'])
    && $projectStatus['iko_status'] == 'Approved'
    && $projectStatus['jsa_status'] == 'Approved'
    && $projectStatus['jsa_status'] == 'Approved'
    ){
        $projectStatus->project_status = 'Approved';
        if($projectStatus['tanggal_mulai'] > now()->toDateString()){
            $projectStatus->status = 'Upcoming';
        } else if($projectStatus['tanggal_mulai'] <= now()->toDateString() && $projectStatus['tanggal_selesai'] >= now()->toDateString()){
            $projectStatus->status = 'Ongoing';
        } else if($projectStatus['tanggal_selesai'] < now()->toDateString()){
            $projectStatus->status = 'Finished';
        }
    }

    $projectStatus->save();

    return redirect()->back();

    }
}
