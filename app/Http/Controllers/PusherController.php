<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pusher\Pusher;

class PusherController extends Controller
{
  public function sendNotification() {
    $options = array(
   'cluster' => 'ap1',
   'encrypted' => true
 );
 $pusher = new pusher(
   '9e51398a10f7aacc64d9',
   '34b3f273b25ac9ec2132',
   '583424',
   $options
 );

 $data['message'] = 'hello world';
 $pusher->trigger('notify', 'notify-event', $data);
  }

  public function sendIPB() {
    $options = array(
   'cluster' => 'ap1',
   'encrypted' => true
 );
 $pusher = new pusher(
   '9e51398a10f7aacc64d9',
   '34b3f273b25ac9ec2132',
   '583424',
   $options
 );

 $data['message'] = 'hello world';
 $pusher->trigger('ipb', 'ipb-event', $data);
  }

}
