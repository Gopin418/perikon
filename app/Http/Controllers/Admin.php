<?php

namespace App\Http\Controllers;

//Library
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

//Model
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\User;
use App\Role;
use App\profile;
use App\departement;

class Admin extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guest()){
            return redirect()->route('404');
        } elseif(Auth::user()->hasRole('Admin Biasa') || Auth::user()->hasRole('Admin Utama')){
            $list = DB::table('role_user')
            ->leftJoin('roles', 'role_user.role_id', '=', 'roles.id')
            ->rightJoin('users', 'users.id', '=', 'role_user.user_id')
            ->join('profiles', 'users.id', 'profiles.user_id')
            ->where('role_name', '!=', 'Super Admin')
            ->orderBy('users.created_at', 'asc')
            ->get();
        } elseif(Auth::user()->hasRole('Admin Developer')) {
            $list = DB::table('role_user')
            ->leftJoin('roles', 'role_user.role_id', '=', 'roles.id')
            ->rightJoin('users', 'users.id', '=', 'role_user.user_id')
            ->join('profiles', 'users.id', 'profiles.user_id')
            ->orderBy('users.created_at', 'asc')
            ->get();
        }
        $ADL_pending = ADL_status::where('status', 'Pending')->count();
        $JSA_pending = JSA_Status::where('status', 'Pending')->count();
        $IKO_pending = IKO_Status::where('status', 'Pending')->count();
        $IPB_pending = IPB_Status::where('status', 'Pending')->count();
        $profile = profile::where('user_id', Auth::user()->id)->first();
        return view('admin/admin/list', [
            'profile' => $profile,
            'list' => $list,
            'IKO_pending' => $IKO_pending,
            'JSA_pending' => $JSA_pending,
            'ADL_pending' => $ADL_pending,
            'IPB_pending' => $IPB_pending
        ]);
    }

    public function role(Request $request) {
        if(Auth::guest() || !Auth::user()->hasRole('Admin Developer')){
            return redirect()->route('404');
        }

        $list = DB::table('roles')->orderBy('id')->get();

        $ADL_pending = ADL_Status::where('status', 'Pending')->count();
        $JSA_pending = JSA_Status::where('status', 'Pending')->count();
        $IKO_pending = IKO_Status::where('status', 'Pending')->count();
        $IPB_pending = IPB_Status::where('status', 'Pending')->count();
        $profile = profile::where('user_id', Auth::user()->id)->first();
        return view('admin/admin/role', [
            'profile' => $profile,
            'list' => $list,
            'IKO_pending' => $IKO_pending,
            'JSA_pending' => $JSA_pending,
            'ADL_pending' => $ADL_pending,
            'IPB_pending' => $IPB_pending
        ]);
    }

    public function add(Request $request) {
        if(Auth::guest() || Auth::user()->hasRole('Admin Biasa')){
            return redirect()->route('404');
        }
        $departement = departement::get();

        // sidebar
        $ADL_pending = ADL_Status::where('status', 'Pending')->count();
        $JSA_pending = JSA_Status::where('status', 'Pending')->count();
        $IKO_pending = IKO_Status::where('status', 'Pending')->count();
        $IPB_pending = IPB_Status::where('status', 'Pending')->count();
        $profile = profile::where('user_id', Auth::user()->id)->first();
        return view('admin/admin/add', [
            'departement' => $departement,
            'profile' => $profile,
            'IKO_pending' => $IKO_pending,
            'JSA_pending' => $JSA_pending,
            'ADL_pending' => $ADL_pending,
            'IPB_pending' => $IPB_pending
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = Role::where('role_name', '=', $request->role)->first();
        $user = new User;
        $user->firstname     = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email    = $request->email;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->roles()->attach($role);

        $id = User::orderBy('id', 'desc')->first();
        $newUser = User::orderBy('id', 'desc')->first();

        $profile = new profile;
        $profile->user_id = $id->id;
        $profile->image = 'avatar5.png';
        $profile->bg = 'photo1.png';
        $profile->fname = $newUser->firstname;
        $profile->lname = $newUser->lastname;
        $profile->email = $newUser->email;
        $profile->username = $newUser->username;
        $profile->company = 'Nutrifood';
        $profile->sub = $request->sub;
        $profile->departement = $request->departement;
        $profile->save();


        return redirect('/admin/list');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', '=', $id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->roles()->sync([$request->roles]);
        // dd($user);
        $user->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id)->delete();

        $role_user = DB::table('role_user')->where('user_id', '=', $id)->delete();

        return redirect()->back();
    }
}
