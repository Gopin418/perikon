<?php

namespace App\Http\Controllers;

// lisbtn
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

// model
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\profile;
use App\ProjectStatus;

class IPBController extends Controller
{

    public function pending(Request $request) {
        if(Auth::guest()) {
            return redirect()->route('404');
        }
      $request->user()->authorizeRoles(['Admin Developer', 'Admin Utama']);

      $ipb = DB::table('ipbs')
      ->join('pengantar', 'ipbs.id_iko', '=', 'pengantar.id')
      ->join('kontraktor', 'ipbs.id_iko', '=', 'kontraktor.id')
      ->join('emergency', 'ipbs.id_iko', '=', 'emergency.id')
      ->join('desc_project', 'ipbs.id_iko', '=', 'desc_project.id')
      ->join('catatan', 'ipbs.id_iko', '=', 'catatan.id')
      ->join('ipb_status', 'ipbs.id', '=', 'ipb_status.id')
      ->where('status', 'Pending')
      ->get();

      $ketinggian = DB::table('ketinggians')
      ->join('keterangan_ketinggians', 'ketinggians.id', 'keterangan_ketinggians.id')
      ->join('ipbs', 'ketinggians.id', '=', 'ipbs.id')
      ->join('ipb_status', 'ketinggians.id_ipb', '=', 'ipb_status.id')
      ->where('status', 'Pending')
      ->get()
      ->toArray();

      $ruang_terbatas = DB::table('ruang_terbatas')
      ->join('keterangan_ruang_terbatas', 'ruang_terbatas.id', '=', 'keterangan_ruang_terbatas.id')
      ->join('ipbs', 'ruang_terbatas.id', '=', 'ipbs.id')
      ->join('ipb_status', 'ruang_terbatas.id_ipb', '=', 'ipb_status.id')
      ->where('status', 'Pending')
      ->get()
      ->toArray();

      $kebakaran = DB::table('kebakarans')
      ->join('keterangan_kebakarans', 'kebakarans.id', '=', 'keterangan_kebakarans.id')
      ->join('ipbs', 'kebakarans.id', '=', 'ipbs.id')
      ->join('ipb_status', 'kebakarans.id_ipb', '=', 'ipb_status.id')
      ->where('status', 'Pending')
      ->get()
      ->toArray();


      $IKO_pending = IKO_Status::where('status', 'Pending')->count();
      $JSA_pending = JSA_Status::where('status', 'Pending')->count();
      $ADL_pending = ADL_Status::where('status', 'Pending')->count();
      $IPB_pending = IPB_Status::where('status', 'Pending')->count();

      $profile = profile::where('user_id', Auth::user()->id)->first();

      return view('admin/IPB/ipb-pending', [
        'profile' => $profile,
        'ipb' => $ipb,
        'IKO_pending' => $IKO_pending,
        'JSA_pending' => $JSA_pending,
        'ADL_pending' => $ADL_pending,
        'IPB_pending' => $IPB_pending,
        'ketinggian' => $ketinggian,
        'ruang_terbatas' => $ruang_terbatas,
        'kebakaran' => $kebakaran
      ]);

    }

    public function approved() {
        if(Auth::guest()) {
            return redirect()->route('404');
        }
      $ipbs = DB::table('ipbs')
      ->join('pengantar', 'ipbs.id_iko', '=', 'pengantar.id')
      ->join('kontraktor', 'ipbs.id_iko', '=', 'kontraktor.id')
      ->join('emergency', 'ipbs.id_iko', '=', 'emergency.id')
      ->join('desc_project', 'ipbs.id_iko', '=', 'desc_project.id')
      ->join('catatan', 'ipbs.id_iko', '=', 'catatan.id')
      ->join('ipb_status', 'ipbs.id', '=', 'ipb_status.id')
      ->where('status', 'Approved')
      ->get();


      $ketinggian = DB::table('ketinggians')
      ->join('keterangan_ketinggians', 'ketinggians.id', 'keterangan_ketinggians.id')
      ->join('ipbs', 'ketinggians.id', '=', 'ipbs.id')
      ->join('ipb_status', 'ketinggians.id', '=', 'ipb_status.id')
      ->where('status', 'Approved')
      ->get()
      ->toArray();

      $ruang_terbatas = DB::table('ruang_terbatas')
      ->join('keterangan_ruang_terbatas', 'ruang_terbatas.id', '=', 'keterangan_ruang_terbatas.id')
      ->join('ipbs', 'ruang_terbatas.id', '=', 'ipbs.id')
      ->join('ipb_status', 'ruang_terbatas.id', '=', 'ipb_status.id')
      ->where('status', 'Approved')
      ->get()
      ->toArray();

      $kebakaran = DB::table('kebakarans')
      ->join('keterangan_kebakarans', 'kebakarans.id', '=', 'keterangan_kebakarans.id')
      ->join('ipbs', 'kebakarans.id', '=', 'ipbs.id')
      ->join('ipb_status', 'kebakarans.id', '=', 'ipb_status.id')
      ->where('status', 'Approved')
      ->get()
      ->toArray();

      $IKO_pending = IKO_Status::where('status', 'Pending')->count();
      $JSA_pending = JSA_Status::where('status', 'Pending')->count();
      $ADL_pending = ADL_Status::where('status', 'Pending')->count();
      $IPB_pending = IPB_Status::where('status', 'Pending')->count();
      $IPB_approved = IPB_Status::where('status', 'Approved')->count();

      $profile = profile::where('user_id', Auth::user()->id)->first();

      return view('admin/IPB/ipb-approved', [
        'profile' => $profile,
        'ipbs' => $ipbs,
        'IKO_pending' => $IKO_pending,
        'JSA_pending' => $JSA_pending,
        'ADL_pending' => $ADL_pending,
        'IPB_pending' => $IPB_pending,
        'IPB_approved' => $IPB_approved,
        'ketinggian' => $ketinggian,
        'ruang_terbatas' => $ruang_terbatas,
        'kebakaran' => $kebakaran
      ]);
    }

    public function update(Request $request, $id)
    {
    $IPB_Status = IPB_Status::where('id' , '=', $id)->first();
    $IPB_Status->status = 'Approved';
    $IPB_Status->save();

    $no_ipb = IPB_Status::join('ipbs', 'ipb_status.id', '=', 'ipbs.id')
    ->where([
        ['ipb_status.id', $id],
        ['status', 'Approved']
    ])
    ->pluck('no_ipb')
    ->first();


    $projectStatus = ProjectStatus::where('no_ipb', $no_ipb)->first();
    $projectStatus->ipb_status = 'Approved';

    if( $projectStatus->iko_status == 'Approved'
    &&  $projectStatus->jsa_status == 'Approved'
    &&  $projectStatus->adl_status == 'Approved'
    &&  $projectStatus->ipb_status == 'Approved'
    ) {
        $projectStatus->project_status = 'Approved';
        if($projectStatus['tanggal_mulai'] > now()->toDateString()){
            $projectStatus->status = 'Upcoming';
        } else if($projectStatus['tanggal_mulai'] <= now()->toDateString() && $projectStatus['tanggal_selesai'] >= now()->toDateString()){
            $projectStatus->status = 'Ongoing';
        } else if($projectStatus['tanggal_selesai'] < now()->toDateString()){
            $projectStatus->status = 'Finished';
        }
    }

    $projectStatus->save();

    return redirect()->back();

    }
}
