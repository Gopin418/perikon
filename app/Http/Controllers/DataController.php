<?php

namespace App\Http\Controllers;

//Library
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

//Model
use App\pengantar;
use App\JSA;
use App\ADL;
use App\ipb;
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\Code;

class DataController extends Controller
{
    public function ikoData() {
        $data = pengantar::join('kontraktor','pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('gmp', 'pengantar.id', '=', 'gmp.id')
        ->join('catatan', 'pengantar.id', '=', 'catatan.id')
        ->orderBy('pengantar.id')
        ->get();
        $IKO_pending = IKO_Status::where('status', 'pending')->count();
        $jumlah = Pengantar::count();
        return view('admin.data.iko',[
            'IKO_pending' => $IkO_pending,
            'data' => $data,
            'jumlah' => $jumlah
        ]);
    }

    public function key(){
      return Code::orderBy('updated_at', 'desc')->select('code')->first();
    }

    public function iko(){
      return pengantar::count();
    }
    public function ikoPending(){
      return IKO_Status::where('status', 'Pending')->count();
    }
    public function jsa(){
      return JSA::count();
    }
    public function jsaPending(){
      return JSA_Status::where('status', 'Pending')->count();
    }
    public function adl(){
      return ADL::count();
    }
    public function adlPending(){
      return ADL_Status::where('status', 'Pending')->count();
    }
    public function ipb(){
      return ipb::count();
    }
    public function ipbPending(){
      return IPB_Status::where('status', 'Pending')->count();
    }
    public function maintenance(){
      return pengantar::join('iko_status', 'pengantar.id', '=', 'iko_status.id')->where([['jenis', '=', 'Maintenance'],['status', '=', 'Approved']])->count();
    }
    public function project(){
      return pengantar::join('iko_status', 'pengantar.id', '=', 'iko_status.id')->where([['jenis', '=', 'Project'],['status', '=', 'Approved']])->count();
    }
}
