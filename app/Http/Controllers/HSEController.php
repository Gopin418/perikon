<?php

namespace App\Http\Controllers;

//Lirbary
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;
use Asset;
use Illuminate\Support\Facades\Mail;

// Other Packaage
use App\Mail\SendMailable;

//Model
use App\pengantar;
use App\catatan;
use App\desc_project;
use App\emergency;
use App\gmp;
use App\kontraktor;
use App\nutrifood;
use App\JSA;
use App\ADL;
use App\ipb;
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\lampiran;
use App\ruang_terbatas;
use App\keterangan_ruang_terbatas;
use App\Kebakaran;
use App\keterangan_kebakaran;
use App\Ketinggian;
use App\keterangan_ketinggian;
use App\utility;
use App\ProjectStatus;

class HSEController extends Controller
{


    /**
     * Show IKO Form
     *
     * @return \Illuminate\Http\Response
     */
    public function IKO(Request $request) {
      $verify = $request->session()->get('verify');
      if(empty($verify) || $verify->status != 'Verified'){
        return redirect()->route('index');
      }

      $request->session()->get('pengantar');
      $request->session()->get('kontraktor');
      $request->session()->get('nutrifood');
      $request->session()->get('emergency');
      $request->session()->get('deskripsi');
      $request->session()->get('gmp');
      $request->session()->get('catatan');
      $request->session()->get('IKO-Status');
      $request->session()->get('Project-Status');

        $air = utility::where('jenis', 'air')->first();
        $angin = utility::where('jenis', 'angin')->first();
        $listrik = utility::where('jenis', 'listrik')->first();
        $steam = utility::where('jenis', 'steam')->first();
        return view('IKO', [
          'air' => $air,
          'angin' => $angin,
          'listrik' => $listrik,
          'steam' => $steam
        ]);
    }

    /**
     * Post Request stroe Data iko ke dalam session
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

      /**
      * Remove null/false/0/zero array element
      *
      * @var $item
      */
      function array_remove_null($item) {
        if(!is_array($item)) {
          return $item;
        }

        return collect($item)->reject(function($item) {
          return is_null($item);
        })->flatMap(function($item, $key) {

          return is_numeric($key)
              ?  [array_remove_null($item)]
              :  [$key => array_remove_null($item)];
        })
        ->toArray();
      }

        $projectStatus = new ProjectStatus;
        $projectStatus->no_iko          = ProjectStatus::count()+1 . '/' . 'IKO' . '/' . date('Y');
        $projectStatus->iko_status      = 'Pending';
        $projectStatus->no_jsa          = ProjectStatus::count()+1 . '/' . 'JSA' . '/' . date('Y');
        $projectStatus->jsa_status      = 'Pending';
        $projectStatus->no_adl          = ProjectStatus::count()+1 . '/' . 'ADL' . '/' . date('Y');
        $projectStatus->adl_status      = 'Pending';
        $projectStatus->no_ipb          = null;
        $projectStatus->ipb_status      = null;
        $projectStatus->tanggal_mulai   = $request->tanggal_mulai;
        $projectStatus->tanggal_selesai = $request->tanggal_selesai;
        $projectStatus->status          = 'Pending';
        $projectStatus->project_status  = 'Pending';

        $request->session()->put('Project-Status', $projectStatus);

        //Pengantar
        $pengantar = new pengantar;
        $pengantar->no_iko          = pengantar::count() + 1 . '/' . 'IKO' . '/' . date('Y');
        $pengantar->nama_pekerjaan  = $request->nama_pekerjaan;
        $pengantar->jenis           = $request->jenis;
        $pengantar->tgl_mulai       = $request->tgl_mulai;
        $pengantar->tanggal_mulai   = $request->tanggal_mulai;
        $pengantar->tgl_selesai     = $request->tgl_selesai;
        $pengantar->tanggal_selesai = $request->tanggal_selesai;

        //Pengantar menuju session
        $request->session()->put('pengantar', $pengantar);

        //Kontraktor
        $kontraktor = new kontraktor;
        $kontraktor->id                     = 0;
        $kontraktor->nama_kontraktor        = $request->nama_kontraktor;
        $kontraktor->pj_kontraktor          = $request->pj_kontraktor;
        $kontraktor->telp_pj_kontraktor     = $request->telp_pj_kontraktor;
        $kontraktor->pj_lapangan            = $request->pj_lapangan;
        $kontraktor->telp_pj_lapangan       = $request->telp_pj_lapangan;

        //Kontraktor menuju session
        $request->session()->put('kontraktor', $kontraktor);

        //PJ Nutrifood
        $nutrifood = new nutrifood;
        $nutrifood->id              = 0;
        $nutrifood->pj_project      = $request->pj_project;
        $nutrifood->telp_pj_project = $request->telp_pj_project;

        //nutri to session
        $request->session()->put('nutrifood', $nutrifood);

        //emergency
        $emergency = new emergency;
        $emergency->id     = 0;
        $emergency->lokasi = $request->lokasi;

        //emergency to session
        $request->session()->put('emergency', $emergency);

        //Description
        $deskripsi = new desc_project;
        $deskripsi->id        = 0;
        $deskripsi->deskripsi = $request->deskripsi;
        $deskripsi->area      = $request->area;

        //Description to session
        $request->session()->put('deskripsi', $deskripsi);

        //GMP
        $gmp = new gmp;
        $gmp->id       = 0;
        $gmp->area_gmp = $request->area_gmp ?? 'Tidak Bekerja pada Area GMP';



        if(is_array($request->brg_yang_disimpan)) {
          $barang = implode(', ', array_values($request->brg_yang_disimpan));

        } else {
          $barang = $request->brg_yang_disimpan;
        }

        $gmp->brg_yang_disimpan = $barang;
        $gmp->lokasi_gudang     = $request->lokasi_gudang;

        //GMP to session
        $request->session()->put('gmp', $gmp);

        // Pekerjaan Berbahaya Array check
        if (is_array($request->pekerjaan_berbahaya)){
            $pekerjaan_berbahaya = implode(', ',  array_values($request->pekerjaan_berbahaya));
        } else {
            $pekerjaan_berbahaya =  $request->pekerjaan_berbahaya;
        }

        // Dokumen lampiran array check
        if (is_array($request->doc_lampiran)){
            $doc_lampiran = implode(', ',  array_values($request->doc_lampiran));
        } else {
            $doc_lampiran =  $request->doc_lampiran;
        }


        // utility Array Check
        if (is_array($request->utility)){
            $utility = implode(', ',  array_values($request->utility));
        } else {
            $utility =  $request->utility;
        }

        array_values(array_filter($request->sistem_terganggu));

        $sistem_terganggu = array_remove_null($request->sistem_terganggu);

        // sistem terganggu array check
        if (is_array($sistem_terganggu)){
            $sistem_terganggu = implode(', ',  array_values($sistem_terganggu));
        } else {
          $sistem_terganggu = $sistem_terganggu;
        }

        //Catatan
        $catatan = new catatan;
        $catatan->id                    = 0;
        $catatan->pekerjaan_berbahaya   = $pekerjaan_berbahaya;
        $catatan->doc_lampiran          = $doc_lampiran;
        $catatan->utility               = $utility;
        $catatan->sistem_terganggu      = $sistem_terganggu;

        //Catatan to session
        $request->session()->put('catatan', $catatan);

        //Status IKO
        $status = new IKO_Status;
        $status->id     = 0;
        $status->status = 'Pending';
        $request->session()->put('IKO-Status', $status);

        return redirect('/JSA');
    }



    /**
     * Menampilkan Form JSA
     *
     * @return \Illuminate\Http\Response
   */
    public function JSA(Request $request) {
      $email = $request->session()->get('contractor');
      $IKOStatus = $request->session()->get('IKO-Status');
      if(empty($email)){
        return redirect()->route('index');
      } else if (empty($IKOStatus)){
        return redirect()->route('iko');
      }
      $request->session()->get('JSA');
      $request->session()->get('JSA-Status');
        return view('JSA');
    }

    /**
     * Post Request untuk menyimpan data Form JSA ke dalam Session
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeJSA(Request $request) {

      /**
      * Remove null/false/0/zero 1D array element
      *
      * @var $item
      */
      function array_remove_null($item) {
        if(!is_array($item)) {
          return $item;
        }

        return collect($item)->reject(function($item) {
          return is_null($item);
        })->flatMap(function($item, $key) {

          return is_numeric($key)
              ?  [array_remove_null($item)]
              :  [$key => array_remove_null($item)];
        })
        ->toArray();
      }


      function findKey($array, $keySearch) {
        if (count($array) > 1) {
          foreach ($array as $key => $item) {
            if (array_key_exists($keySearch, $array)) {
              $newKey = count($array) - 1;
              $array[$newKey] = $array[$keySearch];
              unset($array[$keySearch]);
              return $array;
            }
          }
        } else {
          return $array;
        }
        return $array;
      }

      // ----------------------------------------------------------------------- //

      $aktivitas = array_remove_null($request->aktivitas);
        if (is_array($aktivitas)){
            $aktivitas = implode(', ',  array_values($aktivitas));
          } else {
            $aktivitas =  $aktivitas;
          }

      // Manipulasi data Potensi Bahaya
      $potensi_bahaya = array_remove_null($request->potensi_bahaya);
      $potensi_bahaya = array_map('array_filter', $potensi_bahaya);
      $potensi_bahaya = array_filter($potensi_bahaya);
      $potensi_bahaya = findKey($potensi_bahaya, '#index#');
      if (is_array($potensi_bahaya)){
        foreach ($potensi_bahaya as $key => $value) {
          $potensi_bahaya[$key] = implode(', ', array_values($potensi_bahaya[$key]));
        }
      } else {
        $potensi_bahaya =  $potensi_bahaya;
      }

      // Manipulasi data Pengendalian Bahaya
      $pengendalian_bahaya = array_remove_null($request->pengendalian_bahaya);
      $pengendalian_bahaya = array_map('array_filter', $pengendalian_bahaya);
      $pengendalian_bahaya = array_filter($pengendalian_bahaya);
      $pengendalian_bahaya = findKey($pengendalian_bahaya, '#index#');
      if (is_array($pengendalian_bahaya)){
        foreach ($pengendalian_bahaya as $key => $value) {
          $pengendalian_bahaya[$key] = implode(', ', array_values($pengendalian_bahaya[$key]));
        }
      } else {
        $pengendalian_bahaya =  $pengendalian_bahaya;
      }

      $penanggung_jawab = array_remove_null($request->penanggung_jawab);
      if (is_array($penanggung_jawab)){
        $penanggung_jawab = implode(', ', array_values($penanggung_jawab));
      } else {
        $penanggung_jawab =  $penanggung_jawab;
      }

      $keterangan = $request->keterangan;
      foreach ($keterangan as $key => $value) {
        if (is_null($value)) {
          $keterangan[$key] = 'Tidak Ada Keterangan';
        }
      }
      $formLength = count(array_remove_null($request->aktivitas));
      unset($keterangan[$formLength]);
      if (is_array($keterangan)){
        $keterangan = implode(', ', array_values($keterangan));
      } else {
        $keterangan =  $request->keterangan;
      }

      // JSA
      $JSA = new JSA;
      $JSA->id                    = 0;
      $JSA->no_jsa                = '0';
      $JSA->aktivitas             = $aktivitas;
      $JSA->potensi_bahaya        = $potensi_bahaya;
      $JSA->pengendalian_bahaya   = $pengendalian_bahaya;
      $JSA->penanggung_jawab      = $penanggung_jawab;
      $JSA->keterangan            = $keterangan;

      //JSA to Session
        $request->session()->put('JSA', $JSA);

        //attach Status JSA
        $status = new JSA_Status;
        $status->status = 'Pending';

        // Attached JSA Status to Session
        $request->session()->put('JSA-Status', $status);
        return redirect('/ADL');
    }

    /**
     * Menampilkan halaman Form ADL
     *
     * @return \Illuminate\Http\Response
     */
    public function ADL(Request $request){
      $email = $request->session()->get('contractor');
      $IKOStatus = $request->session()->get('IKO-Status');
      $JSAStatus = $request->session()->get('JSA-Status');
      if(empty($email)){
        return redirect()->route('index');
      } else if (empty($IKOStatus)){
        return redirect()->route('iko');
      } else if (empty($JSAStatus)){
        return redirect()->route('jsa');
      }
      $request->session()->get('ADL');
      $request->session()->get('ADL-Status');
        return view('ADL');
    }

    /**
     * Post Request untuk menyimpan data Form ADL ke dalam Session
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeADL(Request $request) {

      /**
      * Remove null/false/0/zero 1D array element
      *
      * @var $item
      */
      function array_remove_null($item) {
        if(!is_array($item)) {
          return $item;
        }

        return collect($item)->reject(function($item) {
          return is_null($item);
        })->flatMap(function($item, $key) {

          return is_numeric($key)
              ?  [array_remove_null($item)]
              :  [$key => array_remove_null($item)];
        })
        ->toArray();
      }

      /**
      * Find key is exist
      *
      * @var $array, @keySearch
      */
      function findKey($array, $keySearch) {
        if (count($array) > 1) {
          foreach ($array as $key => $item) {
            if (array_key_exists($keySearch, $array)) {
              $newKey = count($array) - 1;
              $array[$newKey] = $array[$keySearch];
              unset($array[$keySearch]);
              return $array;
            }
          }
        } else {
          return $array;
        }
        return $array;
      }

      // ----------------------------------------------------------------------- //

      // Manipulasi data aktivitas
      $aktivitas = array_remove_null($request->aktivitas);
        if (is_array($aktivitas)){
            $aktivitas = implode(', ', $aktivitas);
        } else {
            $aktivitas =  $aktivitas;
        }

      // Manipulasi data Alat dan Bahan
      $adb = array_remove_null($request->adb);
      $adb = array_map('array_filter', $adb);
      $adb = array_filter($adb);
      $adb = findKey($adb, '#index#');
      if (is_array($request->adb)){
        foreach ($adb as $key => $value) {
          $adb[$key] = implode(', ', $adb[$key]);
        }
      } else {
        $adb =  $adb;
      }

      // Manipulasi data jenis jenis_pencemaran
      $jenis_pencemaran = array_remove_null($request->jenis_pencemaran);
        if (is_array($request->jenis_pencemaran)){
            foreach ($jenis_pencemaran as $key => $value) {
              $jenis_pencemaran[$key] = implode(', ', $jenis_pencemaran[$key]);
            }
        } else {
            $jenis_pencemaran =  $jenis_pencemaran;
        }

      // Manipulasi data potensi pencemaran
      $potensi_pencemaran = array_remove_null($request->potensi_pencemaran);
      $potensi_pencemaran = array_map('array_filter', $potensi_pencemaran);
      $potensi_pencemaran = array_filter($potensi_pencemaran);
      $potensi_pencemaran = findKey($potensi_pencemaran, '#index#');
        if (is_array($request->potensi_pencemaran)){
            foreach ($potensi_pencemaran as $key => $value) {
              $potensi_pencemaran[$key] = implode(', ', $potensi_pencemaran[$key]);
            }
        } else {
            $potensi_pencemaran =  $potensi_pencemaran;
        }

      // Manipulasi data penanggung Jawab
      $penanggung_jawab = array_remove_null($request->penanggung_jawab);
        if (is_array($request->penanggung_jawab)){
            $penanggung_jawab = implode(', ', $penanggung_jawab);
        } else {
            $penanggung_jawab =  $penanggung_jawab;
        }

      // Manipulasi data pengandalian pencemaran
      $pengendalian = array_remove_null($request->pengendalian);
      $pengendalian = array_map('array_filter', $pengendalian);
      $pengendalian = array_filter($pengendalian);
      $pengendalian = findKey($pengendalian, '#index#');
        if (is_array($request->pengendalian)){
            foreach ($pengendalian as $key => $value) {
              $pengendalian[$key] = implode(', ', $pengendalian[$key]);
            }
        } else {
            $pengendalian =  $request->pengendalian;
        }

      // Manipulasi data keterangan
      $keterangan = $request->keterangan;
      foreach ($keterangan as $key => $value) {
        if(is_null($value)) {
          $keterangan[$key] = 'Tidak Ada Keterangan';
        }
      }
      $formLength = count(array_remove_null($request->aktivitas));
      unset($keterangan[$formLength]);
        if (is_array($keterangan)){
            $keterangan = implode('","', $keterangan);
        } else {
            $keterangan =  $keterangan;
        }

        //ADL
        $ADL = new ADL;
        $ADL->id                        = 0;
        $ADL->no_adl                    = '0';
        $ADL->aktivitas                 = $aktivitas;
        $ADL->penanggung_jawab          = $penanggung_jawab;
        $ADL->adb                       = $adb;
        $ADL->jenis_pencemaran          = $jenis_pencemaran;
        $ADL->potensi_pencemaran        = $potensi_pencemaran;
        $ADL->pengendalian              = $pengendalian;
        $ADL->keterangan                = $keterangan;

        //ADL to session
        $request->session()->put('ADL', $ADL);

        // Attach status to ADL
        $status = new ADL_Status;
        $status->status = 'Pending';
        $request->session()->put('ADL-Status', $status);

        $catatan = $request->session()->get('catatan');

        if(false !== strpos($catatan->doc_lampiran, 'IPB')) {
            return redirect()->route('ipb');
        } else {
        $projectStatus = $request->session()->get('Project-Status');
        $projectStatus->save();
        $request->session()->forget('Project-Status');

        $pengantar = $request->session()->get('pengantar');
        $pengantar->save();
        $id = pengantar::orderBy('id', 'desc')->pluck('id')->first();
        $request->session()->forget('pengantar');

        $kontraktor = $request->session()->get('kontraktor');
        $kontraktor['id'] = $id;
        $kontraktor->save();
        $request->session()->forget('kontraktor');

        $nutrifood = $request->session()->get('nutrifood');
        $nutrifood['id'] = $id;
        $nutrifood->save();
        $request->session()->forget('nutrifood');

        $emergency = $request->session()->get('emergency');
        $emergency['id'] = $id;
        $emergency->save();
        $request->session()->forget('emergency');

        $deskripsi = $request->session()->get('deskripsi');
        $deskripsi['id'] = $id;
        $deskripsi->save();
        $request->session()->forget('deskripsi');

        $gmp = $request->session()->get('gmp');
        $gmp['id'] = $id;
        $gmp->save();
        $request->session()->forget('gmp');

        $catatan = $request->session()->get('catatan');
        $catatan['id'] = $id;
        $catatan->save();
        $request->session()->forget('catatan');

        $statusIKO = $request->session()->get('IKO-Status');
        $statusIKO['id'] = $id;
        $statusIKO->save();
        $request->session()->forget('IKO-Status');

        $JSA = $request->session()->get('JSA');
        $JSA['id'] = $id;
        $JSA['no_jsa'] = $id . '/' . 'JSA' . '/' . date('Y');
        $JSA->save();
        $request->session()->forget('JSA');

        $statusJSA= $request->session()->get('JSA-Status');
        $statusJSA['id'] = $id;
        $statusJSA->save();
        $request->session()->forget('JSA-Status');

        $ADL = $request->session()->get('ADL');
        $ADL['id'] = $id;
        $ADL['no_adl'] = $id . '/' . 'ADL' . '/' .date('Y');
        $ADL->save();
        $request->session()->forget('ADL');

        $statusADL = $request->session()->get('ADL-Status');
        $statusADL['id'] = $id;
        $statusADL->save();
        $request->session()->forget('ADL-Status');


        $options = array(
          'cluster' => 'ap1',
          'encrypted' => true
        );

        $pusher = new Pusher(
          '9e51398a10f7aacc64d9',
          '34b3f273b25ac9ec2132',
          '583424',
          $options
        );

        $message = "";

        $pusher->trigger('notify', 'notify-event', $message);

        return redirect()->route('finish');
        }
    }


    public function IPB(Request $request) {

        $request->session()->get('IPB-Status');
        $request->session()->get('IPB');
        $jenis = $request->session()->get('catatan');

        $ipb = new ipb;
        $ipb->id_IKO = 0;


        if (false !== strpos($jenis->pekerjaan_berbahaya, 'Api')){
          $ipb->kebakaran = 'true';

        } else {
          $ipb->kebakaran = 'false';
        }

        if (false !== strpos($jenis->pekerjaan_berbahaya, 'Ketinggian')){
          $ipb->ketinggian = 'true';
        } else {
          $ipb->ketinggian = 'false';
        }

        if (false !== strpos($jenis->pekerjaan_berbahaya, 'Ruang Terbatas')){
          $ipb->ruang_terbatas = 'true';
        } else {
          $ipb->ruang_terbatas = 'false';
        }
        $request->session()->put('IPB', $ipb);

        $status = new IPB_Status;
        $status->status = 'Pending';
        $request->session()->put('IPB-Status', $status);

        if($ipb->ruang_terbatas == 'true') {
            return redirect('/IPB/ruang-terbatas');
        }
        elseif($ipb->kebakaran == 'true') {
            return redirect('/IPB/kebakaran');
        }
        elseif($ipb->ketinggian == 'true') {
            return redirect('/IPB/ketinggian');
        }

    }


    // View Ruangan Terbatas
    public function ruangTerbatas(Request $request) {
        $request->session()->get('ruang_terbatas');
        $request->session()->get('keterangan_ruang_terbatas');
        return view('IPB-RT');
    }

    // Store Form Ruangan Terbatas
    public function storeRuangTerbatas(Request $request) {

        $ruang_terbatas = new ruang_terbatas;
        $ruang_terbatas->id_ipb = 0;
        $ruang_terbatas->pemenuhan_1 = $request->input('pemenuhan_1');
        $ruang_terbatas->pemenuhan_2 = $request->input('pemenuhan_2');
        $ruang_terbatas->pemenuhan_3 = $request->input('pemenuhan_3');
        $ruang_terbatas->pemenuhan_4 = $request->input('pemenuhan_4');
        $ruang_terbatas->pemenuhan_5 = $request->input('pemenuhan_5');
        $ruang_terbatas->pemenuhan_6 = $request->input('pemenuhan_6');
        $ruang_terbatas->pemenuhan_7 = $request->input('pemenuhan_7');
        $ruang_terbatas->pemenuhan_8 = $request->input('pemenuhan_8');
        $ruang_terbatas->pemenuhan_9 = $request->input('pemenuhan_9');
        $ruang_terbatas->pemenuhan_10 = $request->input('pemenuhan_10');
        $ruang_terbatas->pemenuhan_11 = $request->input('pemenuhan_11');
        $ruang_terbatas->pemenuhan_12 = $request->input('pemenuhan_12');
        $ruang_terbatas->pemenuhan_13 = $request->input('pemenuhan_13');

        $request->session()->put('ruang_terbatas', $ruang_terbatas);

        $keterangan = new keterangan_ruang_terbatas;
        $keterangan->id = 0;
        $keterangan->keterangan_1 = $request->input('keterangan_1') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_2 = $request->input('keterangan_2') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_3 = $request->input('keterangan_3') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_4 = $request->input('keterangan_4') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_5 = $request->input('keterangan_5') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_6 = $request->input('keterangan_6') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_7 = $request->input('keterangan_7') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_8 = $request->input('keterangan_8') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_9 = $request->input('keterangan_9') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_10 = $request->input('keterangan_10') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_11 = $request->input('keterangan_11') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_12 = $request->input('keterangan_12') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_13 = $request->input('keterangan_13') ?? 'Tidak Ada Keterangan';

        $request->session()->put('keterangan_ruang_terbatas', $keterangan);

        $lampiran = $request->session()->get('IPB');

        if($lampiran->kebakaran == 'true') {
            return redirect('/IPB/kebakaran');
        }
        elseif($lampiran->kebakaran == 'false' && $lampiran->ketinggian == 'true') {
            return redirect('/IPB/ketinggian');
        }
        elseif($lampiran->kebakaran == 'false' && $lampiran->ketinggian == 'false') {
            $projectStatus = $request->session()->get('Project-Status');
            $projectStatus['no_ipb'] = ipb::count() + 1 . '/' . 'IPB' . '/' . date('Y');
            $projectStatus['ipb_status'] = 'Pending';
            $projectStatus->save();
            $request->session()->forget('Project-Status');

            $request->session()->get('pengantar')->save();
            $request->session()->forget('pengantar');
            $id = pengantar::orderBy('id', 'desc')->pluck('id')->first();

            $kontraktor = $request->session()->get('kontraktor');
            $kontraktor['id'] = $id;
            $kontraktor->save();
            $request->session()->forget('kontraktor');

            $nutrifood = $request->session()->get('nutrifood');
            $nutrifood['id'] = $id;
            $nutrifood->save();
            $request->session()->forget('nutrifood');

            $emergency = $request->session()->get('emergency');
            $emergency['id'] = $id;
            $emergency->save();
            $request->session()->forget('emergency');

            $deskripsi = $request->session()->get('deskripsi');
            $deskripsi['id'] = $id;
            $deskripsi->save();
            $request->session()->forget('deskripsi');

            $gmp = $request->session()->get('gmp');
            $gmp['id'] = $id;
            $gmp->save();
            $request->session()->forget('gmp');

            $catatan = $request->session()->get('catatan');
            $catatan['id'] = $id;
            $catatan->save();
            $request->session()->forget('catatan');

            $statusIKO = $request->session()->get('IKO-Status');
            $statusIKO['id'] = $id;
            $statusIKO->save();
            $request->session()->forget('IKO-Status');

            $JSA = $request->session()->get('JSA');
            $JSA['id'] = $id;
            $JSA['no_jsa'] = $id . '/' . 'JSA' . '/' . date('Y');
            $JSA->save();
            $request->session()->forget('JSA');

            $statusJSA = $request->session()->get('JSA-Status');
            $statusJSA['id'] = $id;
            $statusJSA->save();
            $request->session()->forget('JSA-Status');

            $ADL = $request->session()->get('ADL');
            $ADL['id'] = $id;
            $ADL['no_adl'] = $id . '/' . 'ADL' . '/' . date('Y');
            $ADL->save();
            $request->session()->forget('ADL');

            $statusADL = $request->session()->get('ADL-Status');
            $statusADL['id'] = $id;
            $statusADL->save();
            $request->session()->forget('ADL-Status');

            $IPB = $request->session()->get('IPB');
            $IPB['id_IKO'] = $id;
            $IPB->no_ipb = ipb::count() + 1 . '/' . 'IPB' . '/' . date('Y');
            $IPB->save();
            $idIPB = ipb::orderBy('id', 'desc')->pluck('id')->first();
            $request->session()->forget('IPB');

            $statusIPB = $request->session()->get('IPB-Status');
            $statusIPB['id'] = $idIPB;
            $statusIPB->save();
            $request->session()->forget('IPB-Status');

            $ruang_terbatas = $request->session()->get('ruang_terbatas');
            $ruang_terbatas['id_ipb'] = $idIPB;
            $ruang_terbatas->save();
            $id_ruang = DB::table('ruang_terbatas')->orderBy('id', 'desc')->pluck('id')->first();
            $request->session()->forget('ruang_terbatas');

            $keterangan_ruang_terbatas = $request->session()->get('keterangan_ruang_terbatas');
            $keterangan_ruang_terbatas['id'] = $id_ruang;
            $keterangan_ruang_terbatas->save();
            $request->session()->forget('keterangan_ruang_terbatas');


            $options = array(
              'cluster' => 'ap1',
              'encrypted' => true
            );

            $pusher = new Pusher(
              '9e51398a10f7aacc64d9',
              '34b3f273b25ac9ec2132',
              '583424',
              $options
            );

            $message = "";

            $pusher->trigger('notify', 'notify-event', $message);

            return redirect('/IKO');
        }
    }

    public function kebakaran(Request $request) {
        $request->session()->get('kebakaran');
        $request->session()->get('keterangan_kebakaran');
        return view('IPB-Kebakaran');
    }

    public function storeKebakaran(Request $request) {
        $kebakaran = new kebakaran;
        $kebakaran->id_ipb = 0;
        $kebakaran->pemenuhan_1 = $request->input('pemenuhan_1');
        $kebakaran->pemenuhan_2 = $request->input('pemenuhan_2');
        $kebakaran->pemenuhan_3 = $request->input('pemenuhan_3');
        $kebakaran->pemenuhan_4 = $request->input('pemenuhan_4');
        $kebakaran->pemenuhan_5 = $request->input('pemenuhan_5');
        $kebakaran->pemenuhan_6 = $request->input('pemenuhan_6');
        $kebakaran->pemenuhan_7 = $request->input('pemenuhan_7');
        $kebakaran->pemenuhan_8 = $request->input('pemenuhan_8');
        $kebakaran->pemenuhan_9 = $request->input('pemenuhan_9');
        $kebakaran->pemenuhan_10 = $request->input('pemenuhan_10');
        $kebakaran->pemenuhan_11 = $request->input('pemenuhan_11');
        $kebakaran->pemenuhan_12 = $request->input('pemenuhan_12');
        $kebakaran->pemenuhan_13 = $request->input('pemenuhan_13');

        $request->session()->put('kebakaran', $kebakaran);

        $keterangan = new keterangan_kebakaran;
        $keterangan->id = 0;
        $keterangan->keterangan_1 = $request->input('keterangan_1') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_2 = $request->input('keterangan_2') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_3 = $request->input('keterangan_3') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_4 = $request->input('keterangan_4') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_5 = $request->input('keterangan_5') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_6 = $request->input('keterangan_6') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_7 = $request->input('keterangan_7') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_8 = $request->input('keterangan_8') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_9 = $request->input('keterangan_9') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_10 = $request->input('keterangan_10') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_11 = $request->input('keterangan_11') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_12 = $request->input('keterangan_12') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_13 = $request->input('keterangan_13') ?? 'Tidak Ada Keterangan';

        $request->session()->put('keterangan_kebakaran', $keterangan);

        $lampiran = $request->session()->get('IPB');

        if($lampiran->ketinggian == 'true') {
            return redirect('/IPB/ketinggian');
          }
        elseif($lampiran->ketinggian == 'false') {
            $projectStatus = $request->session()->get('Project-Status');
            $projectStatus['no_ipb'] = ipb::count() + 1 . '/' . 'IPB' . '/' . date('Y');
            $projectStatus['ipb_status'] = 'Pending';
            $projectStatus->save();
            $request->session()->forget('Project-Status');

            $request->session()->get('pengantar')->save();
            $id = pengantar::orderBy('id', 'desc')->pluck('id')->first();
            $request->session()->forget('pengantar');

            $kontraktor = $request->session()->get('kontraktor');
            $kontraktor['id'] = $id;
            $kontraktor->save();
            $request->session()->forget('kontraktor');

            $nutrifood = $request->session()->get('nutrifood');
            $nutrifood['id'] = $id;
            $nutrifood->save();
            $request->session()->forget('nutrifood');

            $emergency = $request->session()->get('emergency');
            $emergency['id'] = $id;
            $emergency->save();
            $request->session()->forget('emergency');

            $deskripsi = $request->session()->get('deskripsi');
            $deskripsi['id'] = $id;
            $deskripsi->save();
            $request->session()->forget('deskripsi');

            $gmp = $request->session()->get('gmp');
            $gmp['id'] = $id;
            $gmp->save();
            $request->session()->forget('gmp');

            $catatan = $request->session()->get('catatan');
            $catatan['id'] = $id;
            $catatan->save();
            $request->session()->forget('catatan');

            $statusIKO = $request->session()->get('IKO-Status');
            $statusIKO['id'] = $id;
            $statusIKO->save();
            $request->session()->forget('IKO-Status');

            $JSA = $request->session()->get('JSA');
            $JSA['id'] = $id;
            $JSA['no_jsa'] = $id . '/' . 'JSA' . '/' . date('Y');
            $JSA->save();
            $request->session()->forget('JSA');

            $statusJSA = $request->session()->get('JSA-Status');
            $statusJSA['id'] = $id;
            $statusJSA->save();
            $request->session()->forget('JSA-Status');

            $ADL = $request->session()->get('ADL');
            $ADL['id'] = $id;
            $ADL['no_adl'] = $id . '/' . 'ADL' . '/' . date('Y');
            $ADL->save();
            $request->session()->forget('ADL');

            $statusADL = $request->session()->get('ADL-Status');
            $statusADL['id'] = $id;
            $statusADL->save();
            $request->session()->forget('ADL-Status');

            $IPB = $request->session()->get('IPB');
            $IPB['id_IKO'] = $id;
            $IPB->no_ipb = ipb::count() + 1 . '/' . 'IPB' . '/' . date('Y');
            $IPB->save();
            $idIPB = ipb::orderBy('id', 'desc')->pluck('id')->first();

            $statusIPB = $request->session()->get('IPB-Status');
            $statusIPB['id'] = $idIPB;
            $statusIPB->save();
            $request->session()->forget('IPB-Status');

            $kebakaran = $request->session()->get('kebakaran');
            $kebakaran['id_ipb'] = $idIPB;
            $kebakaran->save();
            $id_kebakaran = DB::table('kebakarans')->orderBy('id', 'desc')->pluck('id')->first();
            $request->session()->forget('kebakaran');

            $keterangan = $request->session()->get('keterangan_kebakaran');
            $keterangan['id'] = $id_kebakaran;
            $keterangan->save();
            $request->session()->forget('keterangan_kebakaran');

            if($lampiran->ruang_terbatas == 'true') {
                $ruang_terbatas = $request->session()->get('ruang_terbatas');
                $ruang_terbatas['id_ipb'] = $idIPB;
                $ruang_terbatas->save();
                $id_ruang = DB::table('ruang_terbatas')->orderBy('id', 'desc')->pluck('id')->first();
                $request->session()->forget('ruang_terbatas');

                $keterangan_ruang_terbatas = $request->session()->get('keterangan_ruang_terbatas');
                $keterangan_ruang_terbatas['id'] = $id_ruang;
                $request->session()->forget('keterangan_ruang_terbatas');


                $request->session()->forget('IPB');

                $options = array(
                  'cluster' => 'ap1',
                  'encrypted' => true
                );

                $pusher = new Pusher(
                  '9e51398a10f7aacc64d9',
                  '34b3f273b25ac9ec2132',
                  '583424',
                  $options
                );

                $message = "";

                $pusher->trigger('notify', 'notify-event', $message);

                return redirect('/IKO');
            } else {
              $request->session()->forget('IPB');


                $options = array(
                  'cluster' => 'ap1',
                  'encrypted' => true
                );

                $pusher = new Pusher(
                  '9e51398a10f7aacc64d9',
                  '34b3f273b25ac9ec2132',
                  '583424',
                  $options
                );

                $message = "";

                $pusher->trigger('notify', 'notify-event', $message);

                return redirect('/IKO');
            }
        }
    }

    public function ketinggian(Request $request) {
        $request->session()->get('ketinggian');
        $request->session()->get('keterangan_ketinggian');
        return view('IPB-Ketinggian');
    }

    public function storeKetinggian(Request $request) {


        $ketinggian = new ketinggian;
        $ketinggian->id = 0;
        $ketinggian->pemenuhan_1 = $request->input('pemenuhan_1');
        $ketinggian->pemenuhan_2 = $request->input('pemenuhan_2');
        $ketinggian->pemenuhan_3 = $request->input('pemenuhan_3');
        $ketinggian->pemenuhan_4 = $request->input('pemenuhan_4');

        $request->session()->put('ketinggian', $ketinggian);

        $keterangan = new keterangan_ketinggian;
        $keterangan->id = 0;
        $keterangan->keterangan_1 = $request->input('keterangan_1') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_2 = $request->input('keterangan_2') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_3 = $request->input('keterangan_3') ?? 'Tidak Ada Keterangan';
        $keterangan->keterangan_4 = $request->input('keterangan_4') ?? 'Tidak Ada Keterangan';

        $request->session()->put('keterangan_ketinggian', $keterangan);

        $projectStatus = $request->session()->get('Project-Status');
        $projectStatus['no_ipb'] = ipb::count() + 1 . '/' . 'IPB' . '/' . date('Y');
        $projectStatus['ipb_status'] = 'Pending';
        $projectStatus->save();
        $request->session()->forget('Project-Status');

        $request->session()->get('pengantar')->save();
        $request->session()->forget('pengantar');
        $id = pengantar::orderBy('id', 'desc')->pluck('id')->first();

        $kontraktor = $request->session()->get('kontraktor');
        $kontraktor['id'] = $id;
        $kontraktor->save();
        $request->session()->forget('kontraktor');

        $nutrifood = $request->session()->get('nutrifood');
        $nutrifood['id'] = $id;
        $nutrifood->save();
        $request->session()->forget('nutrifood');

        $emergency = $request->session()->get('emergency');
        $emergency['id'] = $id;
        $emergency->save();
        $request->session()->forget('emergency');

        $deskripsi = $request->session()->get('deskripsi');
        $deskripsi['id'] = $id;
        $deskripsi->save();
        $request->session()->forget('deskripsi');

        $gmp = $request->session()->get('gmp');
        $gmp['id'] = $id;
        $gmp->save();
        $request->session()->forget('gmp');

        $catatan = $request->session()->get('catatan');
        $catatan['id'] = $id;
        $catatan->save();
        $request->session()->forget('catatan');

        $statusIKO = $request->session()->get('IKO-Status');
        $statusIKO['id'] = $id;
        $statusIKO->save();
        $request->session()->forget('IKO-Status');

        $JSA = $request->session()->get('JSA');
        $JSA['id'] = $id;
        $JSA['no_jsa'] = $id . '/' . 'JSA' . '/' . date('Y');
        $JSA->save();
        $request->session()->forget('JSA');

        $statusJSA = $request->session()->get('JSA-Status');
        $statusJSA['id'] = $id;
        $statusJSA->save();
        $request->session()->forget('JSA-Status');

        $ADL = $request->session()->get('ADL');
        $ADL['id'] = $id;
        $ADL['no_adl'] = $id . '/' . 'ADL' . '/' . date('Y');
        $ADL->save();
        $request->session()->forget('ADL');

        $statusADL = $request->session()->get('ADL-Status');
        $statusADL['id'] = $id;
        $statusADL->save();
        $request->session()->forget('ADL-Status');

        $IPB = $request->session()->get('IPB');
        $IPB['id_IKO'] = $id;
        $IPB->no_ipb = ipb::count()+1 . '/' . 'IPB' . '/' . date('Y');
        $IPB->save();
        $idIPB = ipb::orderBy('id', 'desc')->pluck('id')->first();

        $statusIPB = $request->session()->get('IPB-Status');
        $statusIPB['id'] = $idIPB;
        $statusIPB->save();
        $request->session()->forget('IPB-Status');

        $ketinggian = $request->session()->get('ketinggian');
        $ketinggian['id_ipb'] = $idIPB;
        $ketinggian->save();
        $id_ketinggian = DB::table('ketinggians')->orderBy('id', 'desc')->pluck('id')->first();
        $request->session()->forget('ketinggian');

        $keterangan_ketinggian = $request->session()->get('keterangan_ketinggian');
        $keterangan_ketinggian['id'] = $id_ketinggian;
        $keterangan_ketinggian->save();
        $request->session()->forget('keterangan_ketinggian');

        $lampiran = $request->session()->get('IPB');

        if($lampiran->ruang_terbatas == 'true'){
            $ruang_terbatas = $request->session()->get('ruang_terbatas');
            $ruang_terbatas['id_ipb'] = $idIPB;
            $ruang_terbatas->save();
            $id_ruang = DB::table('ruang_terbatas')->orderBy('id', 'desc')->pluck('id')->first();
            $request->session()->forget('ruang_terbatas');

            $keterangan_ruang_terbatas = $request->session()->get('keterangan_ruang_terbatas');
            $keterangan_ruang_terbatas['id'] = $id_ruang;
            $keterangan_ruang_terbatas->save();
            $request->session()->forget('keterangan_ruang_terbatas');

            if($lampiran->kebakaran == 'true') {
                $kebakaran = $request->session()->get('kebakaran');
                $kebakaran['id_ipb'] = $idIPB;
                $kebakaran->save();
                $id_kebakaran = DB::table('kebakarans')->orderBy('id', 'desc')->pluck('id')->first();
                $request->session()->forget('kebakaran');

                $keterangan_kebakaran = $request->session()->get('keterangan_kebakaran');
                $keterangan_kebakaran['id'] = $id_kebakaran;
                $keterangan_kebakaran->save();
                $request->session()->forget('keterangan_kebakaran');


                $request->session()->forget('IPB');

                $options = array(
                  'cluster' => 'ap1',
                  'encrypted' => true
                );

                $pusher = new Pusher(
                  '9e51398a10f7aacc64d9',
                  '34b3f273b25ac9ec2132',
                  '583424',
                  $options
                );

                $message = "";

                $pusher->trigger('notify', 'notify-event', $message);

                return redirect('/IKO');
            } else {

                $request->session()->forget('IPB');

                $options = array(
                  'cluster' => 'ap1',
                  'encrypted' => true
                );

                $pusher = new Pusher(
                  '9e51398a10f7aacc64d9',
                  '34b3f273b25ac9ec2132',
                  '583424',
                  $options
                );

                $message = "";

                $pusher->trigger('notify', 'notify-event', $message);

                return redirect('/IKO');
            }
        } else {

        $request->session()->forget('IPB');

        $options = array(
          'cluster' => 'ap1',
          'encrypted' => true
        );

        $pusher = new Pusher(
          '9e51398a10f7aacc64d9',
          '34b3f273b25ac9ec2132',
          '583424',
          $options
        );

        $message = "";

        $pusher->trigger('notify', 'notify-event', $message);

        return redirect('/IKO');
        }
    }

}
