<?php

namespace App\Http\Controllers;

//Library
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

// Exports
use App\Exports\JSAExport;

//Model
use App\JSA;
use App\ADL;
use App\IPB;
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\profile;
use App\ProjectStatus;

class JSAController extends Controller
{

    public function export(){
      return Excel::download(new JSAExport, 'JSA.xlsx');
    }

    public function pending(Request $request) {
        if(Auth::guest()){
            return redirect()->route('404');
        }
        $request->user()->authorizeRoles(['Admin Developer', 'Admin Utama']);

        $iko = DB::table('jsa')
        ->join('pengantar', 'jsa.id', '=', 'pengantar.id')
        ->join('kontraktor', 'jsa.id', '=', 'kontraktor.id')
        ->join('emergency', 'jsa.id', '=', 'emergency.id')
        ->join('desc_project', 'jsa.id', '=', 'desc_project.id')
        ->join('jsa_status', 'jsa.id', '=', 'jsa_status.id')
        ->where('status', 'Pending')
        ->get();

        $aktivitasJSA = DB::table('jsa')
        ->join('pengantar', 'jsa.id', '=', 'pengantar.id')
        ->join('jsa_status', 'jsa.id', '=', 'jsa_status.id')
        ->where('status', '=', 'pending')
        ->get();

        // dd($aktivitasJSA);

        // sidebar
        $jumlahJSA   = JSA::count();
        $ADL_pending = ADL_Status::where('status', 'pending')->count();
        $JSA_pending = JSA_Status::where('status', 'pending')->count();
        $IKO_pending = IKO_Status::where('status', 'pending')->count();
        $IPB_pending = IPB_Status::where('status', 'pending')->count();

        $profile = profile::where('user_id', Auth::user()->id)->first();

        return view('admin/JSA/jsa-pending', [
            'profile' => $profile,
            'jumlahJSA'   => $jumlahJSA,
            'ADL_pending' => $ADL_pending,
            'JSA_pending' => $JSA_pending,
            'IKO_pending' => $IKO_pending,
            'IPB_pending' => $IPB_pending,
            'iko' => $iko,
            'aktivitasJSA' => $aktivitasJSA
        ]);
    }

    public function approved() {
        if(Auth::guest()){
            return redirect()->route('404');
        }
      $iko = DB::table('jsa')
      ->join('pengantar', 'jsa.id', '=', 'pengantar.id')
      ->join('kontraktor', 'jsa.id', '=', 'kontraktor.id')
      ->join('emergency', 'jsa.id', '=', 'emergency.id')
      ->join('desc_project', 'jsa.id', '=', 'desc_project.id')
      ->join('jsa_status', 'jsa.id', '=', 'jsa_status.id')
      ->where('status', 'Approved')
      ->get();

        $aktivitasJSA = DB::table('jsa')
        ->join('pengantar', 'jsa.id', '=', 'pengantar.id')
        ->join('jsa_status', 'jsa.id', '=', 'jsa_status.id')
        ->where('status', '=', 'approved')
        ->get();

        // sidebar
        $jumlah      = ADL::count();
        $ADL_pending = ADL_Status::where('status', 'pending')->count();
        $JSA_pending = JSA_Status::where('status', 'pending')->count();
        $IKO_pending = IKO_Status::where('status', 'pending')->count();
        $IPB_pending = IPB_Status::where('status', 'pending')->count();

        $JSA_approved = JSA_Status::where('status', 'approved')->count();

        $profile = profile::where('user_id', Auth::user()->id)->first();

        return view('admin/JSA/jsa-approved', [
            'profile' => $profile,
            'JSA_approved' => $JSA_approved,
            'jumlah'      => $jumlah,
            'ADL_pending' => $ADL_pending,
            'JSA_pending' => $JSA_pending,
            'IKO_pending' => $IKO_pending,
            'IPB_pending' => $IPB_pending,
            'iko' => $iko,
            'aktivitasJSA' => $aktivitasJSA
        ]);
    }

    public function update(Request $request, $id) {
    $jsa_status = JSA_Status::where('id' , '=', $id)->first();
    $jsa_status->status = 'Approved';
    $jsa_status->save();

    $no_jsa = JSA_Status::join('jsa', 'jsa_status.id', '=', 'jsa.id')
    ->where([
        ['jsa_status.id', $id],
        ['status', 'Approved']
    ])
    ->pluck('no_jsa')
    ->first();

    $projectStatus = ProjectStatus::where('no_jsa', $no_jsa)->first();
    $projectStatus->jsa_status = 'Approved';

    if(empty($projectStatus['no_ipb'])
    && $projectStatus['iko_status'] == 'Approved'
    && $projectStatus['jsa_status'] == 'Approved'
    && $projectStatus['jsa_status'] == 'Approved'
    ){
        $projectStatus->project_status = 'Approved';
        if($projectStatus['tanggal_mulai'] > now()->toDateString()){
            $projectStatus->status = 'Upcoming';
        } else if($projectStatus['tanggal_mulai'] <= now()->toDateString() && $projectStatus['tanggal_selesai'] >= now()->toDateString()){
            $projectStatus->status = 'Ongoing';
        } else if($projectStatus['tanggal_selesai'] < now()->toDateString()){
            $projectStatus->status = 'Finished';
        }
    }

    $projectStatus->save();

    return redirect()->back();

    }
}
