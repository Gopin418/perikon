<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Library
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

//Model
use App\pengantar;
use App\JSA;
use App\ADL;
use App\IPB;
use App\IKO_Status;
use App\JSA_Status;
use App\ADL_Status;
use App\IPB_Status;
use App\profile;

class ErrorController extends Controller
{
    public function notfound(){
      if(Auth::user()) {
        $profile = profile::where('user_id', Auth::user()->id)->first();

        // label count (sidebar)
        $jumlahADL = ADL::count();
        $jumlahJSA = JSA::count();
        $jumlahIPB = IPB::count();
        $jumlah = Pengantar::count();
        $ADL_pending = ADL_status::where('status', 'Pending')->count();
        $JSA_pending = JSA_Status::where('status', 'Pending')->count();
        $IKO_pending = IKO_Status::where('status', 'Pending')->count();
        $IPB_pending = IPB_Status::where('status', 'Pending')->count();
        return view('admin.errors.404', [
          'profile' => $profile,
          'jumlahADL' => $jumlahADL,
          'jumlahJSA' => $jumlahJSA,
          'jumlahADL' => $jumlahIPB,
          'jumlah' => $jumlah,
          'IKO_pending' => $IKO_pending,
          'JSA_pending' => $JSA_pending,
          'ADL_pending' => $ADL_pending,
          'IPB_pending' => $IPB_pending
        ]);
      } elseif(Auth::guest()) {
        return redirect()->route('guest-404');
      }
    }

    public function fatal() {
      if(Auth::user()){
        return view('admin.errors.500');
      } elseif(Auth::guest()) {
        return redirect()->route('guest-500');
      }
    }

    public function guestNotFound() {
      return view('errors.404');
    }

    public function guestFatal() {
      return view('errors.500');
    }
}
