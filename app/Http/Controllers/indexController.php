<?php

namespace App\Http\Controllers;

// Library
use Illuminate\Support\Str;
use Illuminate\Http\Request;

// Model
use App\email;
use App\Code;
use App\contractor;
use App\verified;

class indexController extends Controller
{
    public function index(Request $request) {
      $request->session()->get('contractor');
      return view('index');
    }

    public function start(Request $request){
      $contractor = new contractor;
      $contractor->uri_id           = Str::random(75);
      $contractor->nama_kontraktor  = $request->nama_kontraktor;
      $contractor->email            = $request->email;
      $contractor->nama_pekerjaan   = $request->nama_pekerjaan;
      $contractor->tanggal_mulai    = $request->tanggal_mulai;
      $contractor->tgl_mulai        = $request->tgl_mulai;
      $contractor->tanggal_selesai  = $request->tanggal_selesai;
      $contractor->tgl_selesai      = $request->tgl_selesai;
      $contractor->verified         = 'false';
      $contractor->status           = 'Pending';
      $request->session()->put('contractor', $contractor);
      $contractor->save();

      $contractorId = contractor::orderBy('created_at', 'desc')->first()->pluck('id');
      $code = new Code;
      $code->user_id  = $contractorId[0];
      $code->code     = Str::random(10);
      $code->uri_id   = Str::random(75);
      $code->verified = 'false';
      $code->save();

      $contractor_uri_id = contractor::orderBy('created_at', 'desc')->first()->pluck('uri_id');
      $code_uri_id = Code::orderBy('created_at', 'desc')->first()->pluck('uri_id');

      $verif = new verified;
      $verif->contractor_uri_id = $contractor_uri_id[0];
      $verif->code_uri_id       = $code_uri_id[0];
      $verif->status            = 'Pending';
      $verif->save();

      return redirect()->route('hold');
    }

    public function verif(Request $request, $id, $code_id){
      $request->session()->forget('verify');
      $verif = verified::where([
        ['contractor_uri_id', $id],
        ['code_uri_id', $code_id]
      ])->first();

      $compact = [
        'id' => $id,
        'code_id' => $code_id
      ];
      // dd($verif);

      if ($verif->status == 'Pending') {
        return view('status.verif', $compact);
      } elseif ($verif->status == 'Verified') {
        return redirect()->route('iko');
      }
    }

    public function verify(Request $request, $id, $code_id){
      $request->session()->get('verify');
      $verif = verified::where([
        ['contractor_uri_id', $id],
        ['code_uri_id', $code_id],
      ])->first();
      $code = Code::where([
        ['code', $request->code],
        ['uri_id', $code_id]
      ])->first();

      $verif->status  = 'Verified';
      $request->session()->put('verify', $verif);

      $verif->save();
      $code->verified = 'true';
      $code->save();


      if($verif->status == 'Verified' && $code->verified == 'true') {
        return redirect()->route('iko');
      } elseif ($verif->status == 'Pending' && $code->verified == 'false'){
        return redirect()->route('verif');
      }
    }
}
