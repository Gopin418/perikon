<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

use App\Mail\SendMailable;

class MailController extends Controller
{
    public function send(Request $request){
      $email = $request->session()->get('contractor');

      if(empty($email)){
        return redirect()->route('index');
      }

      Mail::to($email)->send(new SendMailable());

      return view('emails.name',[
        'email' => $email
      ]);
    }

    public function hold(Request $request){
      $email = $request->session()->get('contractor');

      return view('emails.hold', [
        'email' => $email
      ]);
    }
}
