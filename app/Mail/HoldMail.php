<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Code;
use App\verified;

class HoldMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
    * The Code instance
    *
    * @var Code Verify Code
    * @var verified URI code id
    */
    public $code;
    public $uri;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = Code::orderBy('created_at', 'desc')->where('verified', 'false')->first();
        $this->uri  = verified::orderBy('created_at', 'desc')->where('status', 'Pending')->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from('no-reply@perikon.lumba-studio.id')
                  ->subject('Kode Verifikasi Perikon')
                  ->view('emails.perikon')
                  ->with([
                    'code' => $this->code,
                    'uri' => $this->uri
                  ]);
    }
}
