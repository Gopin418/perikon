<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\AfterSheet;
use App\pengantar;
use App\User;

class DataExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
     * Data Collection to export
     *
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = pengantar::join('kontraktor','pengantar.id', '=', 'kontraktor.id')
        ->join('emergency', 'pengantar.id', '=', 'emergency.id')
        ->join('desc_project', 'pengantar.id', '=', 'desc_project.id')
        ->join('gmp', 'pengantar.id', '=', 'gmp.id')
        ->join('catatan', 'pengantar.id', '=', 'catatan.id')
        ->join('nutrifood', 'pengantar.id', '=', 'nutrifood.id')
        ->join('iko_status' ,'pengantar.id', '=', 'iko_status.id')
        ->where('status', '=', 'Approved')
        ->orderBy('pengantar.id')
        ->select(
            'pengantar.no_iko',
            'nama_kontraktor',
            'nama_pekerjaan',
            'jenis',
            'lokasi',
            'tanggal_mulai',
            'area',
            'tanggal_selesai'
        )
        ->get();
        return $data;
    }

    /**
     * Excel Table heading format
     *
     * @return array
     */
    public function headings(): array{
        return [
            'No IKO',
            'Nama Kontraktor',
            'Nama Pekerjaan',
            'Jenis Pekerjaan',
            'Lokasi Pekerjaan',
            'Area Pekerjaan',
            'Tanggal Mulai',
            'Tanggal Selesai'
        ];
    }

    /**
     * Excel heding styling
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
            },
        ];
    }
}
