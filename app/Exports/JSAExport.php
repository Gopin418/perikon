<?php

namespace App\Exports;

use App\JSA;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class JSAExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return JSA::all();
    }

    public function view(): View {
      return view('exports.JSA', [
        'JSA' => JSA::all()
      ]);
    }
}
